//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#include <fftw3.h>
#include <iomanip>
#include <iostream>
#include <complex>

template< int width = 26 >
void display( int m, int n, fftw_complex const * matrix )
{
   std::complex< double > const * aux_ptr = reinterpret_cast< std::complex< double > const * >( matrix );
   for ( int i = 0; i < m; ++i )
   {
      for ( int j = 0; j < n; ++j )
         std::cout << std::fixed
                   << std::setprecision( 4 )
                   << std::showpoint
                   << std::setw( width ) << aux_ptr[ j + i * n ];

      std::cout << '\n';
   }
}

template< int width = 13 >
void display_diff( int m, int n, fftw_complex const * matrix_a, fftw_complex const * matrix_b )
{
   std::complex< double > const * aux_ptr_a = reinterpret_cast< std::complex< double > const * >( matrix_a );
   std::complex< double > const * aux_ptr_b = reinterpret_cast< std::complex< double > const * >( matrix_b );
   for ( int i = 0; i < m; ++i )
   {
      for ( int j = 0; j < n; ++j )
         std::cout << std::fixed
         << std::setprecision( 4 )
         << std::showpoint
         << std::setw( width ) << std::abs( aux_ptr_a[ j + i * n ] - aux_ptr_b[ j + i * n ] );

      std::cout << '\n';
   }
}

template< int width = 26 >
void display_transpose( int m, int n, fftw_complex const * matrix )
{
   std::complex< double > const * aux_ptr = reinterpret_cast< std::complex< double > const * >( matrix );
   for ( int j = 0; j < n; ++j )
   {
      for ( int i = 0; i < m; ++i )
         std::cout << std::fixed
                   << std::setprecision( 4 )
                   << std::showpoint
                   << std::setw( width ) << aux_ptr[ j + i * n ];

      std::cout << '\n';
   }
}

template< int width = 13 >
void display( int m, int n, double const * matrix )
{
   for ( int i = 0; i < m; ++i )
   {
      for ( int j = 0; j < n; ++j )
         std::cout << std::fixed
         << std::setprecision( 4 )
         << std::showpoint
         << std::setw( width ) << matrix[ j + i * n ];

      std::cout << '\n';
   }
}

// #define PM_PROFILING_ON

#include "ppfft.h"
#include "dft.h"

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <chrono>
#include <algorithm>

// #define LARGE_TEST
// #define DISPLAY
#define RANDOM_SIZES
#define INPUT_COMPLEX
#define ACCURACY_TEST

#ifndef LARGE_TEST
#ifndef RANDOM_SIZES
#define TEST_SIZE_m 6
#define TEST_SIZE_n 5
#define TEST_SIZE_M 8
#define TEST_SIZE_N 14
#define TEST_SIZE_S 4
#endif
#else
// #define TEST_SIZE_m 2048
// #define TEST_SIZE_n 2048
// #define TEST_SIZE_M 2048
// #define TEST_SIZE_N 4096
// #define TEST_SIZE_S 0
#define TEST_SIZE_m 200
#define TEST_SIZE_n 200
#define TEST_SIZE_M 200
#define TEST_SIZE_N 400
#define TEST_SIZE_S 24
#endif

int main()
{
   int seed = std::time( 0 );
   std::cout << seed << '\n';
   std::srand( seed );

#ifndef LARGE_TEST
#ifdef RANDOM_SIZES
   unsigned TEST_SIZE_m = rand() % 150 + 1;
   unsigned TEST_SIZE_n = rand() % 150 + 1;
   unsigned TEST_SIZE_M = rand() % 150 + std::max( TEST_SIZE_m, TEST_SIZE_n );
   unsigned TEST_SIZE_N = rand() % 150 + std::max( TEST_SIZE_m, TEST_SIZE_n );
   TEST_SIZE_N = 2 * TEST_SIZE_N;
   unsigned TEST_SIZE_S = 4 * ( rand() % 38 );
   #endif
#endif
   double sigma_h = dft::PI / TEST_SIZE_n;
   double sigma_v = dft::PI / TEST_SIZE_m;

   std::cout << "Sizes: " << TEST_SIZE_m << '\t' << TEST_SIZE_n << '\t' << TEST_SIZE_M << '\t' << TEST_SIZE_N << '\t'
             << TEST_SIZE_S << '\n';

   std::cout << "Initializing transform...\n";
   std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
   //++++++++++++++++++++++++++++++++++++++++++
   ppfft pp_obj( TEST_SIZE_m, TEST_SIZE_n, TEST_SIZE_M, TEST_SIZE_N, TEST_SIZE_S, 0, FFTW_MEASURE, 0, sigma_h, sigma_v );
   //++++++++++++++++++++++++++++++++++++++++++
   std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
   std::cout << "Decorrido: " << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";
   std::cout << "Done!\n";

   std::cout << "Initializing test image...\n";
   //++++++++++++++++++++++++++++++++++++++++++
   fftw_complex * img = fftw_alloc_complex( TEST_SIZE_m * TEST_SIZE_n );
   fftw_complex * img_copy = fftw_alloc_complex( TEST_SIZE_m * TEST_SIZE_n );
   for( unsigned i = 0; i < TEST_SIZE_m; ++i )
      for( unsigned j = 0; j < TEST_SIZE_n; ++j )
      {
         img_copy[ j + i * TEST_SIZE_n ][ 0 ] = img[ j + i * TEST_SIZE_n ][ 0 ] = double( rand() ) / RAND_MAX;
         img_copy[ j + i * TEST_SIZE_n ][ 1 ] = img[ j + i * TEST_SIZE_n ][ 1 ] = double( rand() ) / RAND_MAX;
      }
   fftw_complex * adj_img = fftw_alloc_complex( TEST_SIZE_M * ( TEST_SIZE_N + TEST_SIZE_S ) );
   for( unsigned i = 0; i < TEST_SIZE_M; ++i )
      for( unsigned j = 0; j < TEST_SIZE_N + TEST_SIZE_S; ++j )
      {
         adj_img[ j + i * ( TEST_SIZE_N + TEST_SIZE_S ) ][ 0 ] = double( rand() ) / RAND_MAX;
         adj_img[ j + i * ( TEST_SIZE_N + TEST_SIZE_S ) ][ 1 ] = double( rand() ) / RAND_MAX;
      }

   fftw_complex * out = fftw_alloc_complex( TEST_SIZE_M * ( TEST_SIZE_N + TEST_SIZE_S ) );
   fftw_complex * adj_out = fftw_alloc_complex( TEST_SIZE_m * TEST_SIZE_n );
   //++++++++++++++++++++++++++++++++++++++++++
   std::cout << "Done!\n";

   std::cout << "Computing PPFFT...\n";
   begin = std::chrono::steady_clock::now();

   std::complex< double > sum_a = 0.0;
   std::complex< double > sum_b = 0.0;

   if ( rand() % 2  )
   {
      std::cout << "Order: adjoint, direct\n";
      pp_obj.adjoint( adj_img, adj_out );
      pp_obj.direct( img, out );
   }
   else
   {
      std::cout << "Order: direct, adjoint\n";
      pp_obj.direct( img, out );
      pp_obj.adjoint( adj_img, adj_out );
   }

   auto aux_ptr_adj_img = reinterpret_cast< std::complex< double > const * >( adj_img );
   auto aux_ptr_out = reinterpret_cast< std::complex< double > const * >( out );
   for( unsigned i = 0; i < TEST_SIZE_M; ++i )
      for( unsigned j = 0; j < TEST_SIZE_N + TEST_SIZE_S; ++j )
         sum_b += aux_ptr_adj_img[ j + i * ( TEST_SIZE_N + TEST_SIZE_S ) ] * std::conj( aux_ptr_out[ j + i * ( TEST_SIZE_N + TEST_SIZE_S ) ] );

   auto aux_ptr_img = reinterpret_cast< std::complex< double > const * >( img_copy );
   auto aux_ptr_adj_out = reinterpret_cast< std::complex< double > const * >( adj_out );
   for( unsigned i = 0; i < TEST_SIZE_m; ++i )
      for( unsigned j = 0; j < TEST_SIZE_n; ++j )
         sum_a += std::conj( aux_ptr_img[ j + i * TEST_SIZE_n ] ) * aux_ptr_adj_out[ j + i * TEST_SIZE_n ];

   std::cout << "Adjointeness: " << sum_a / sum_b << '\n';

   end = std::chrono::steady_clock::now();
   std::cout << "Decorrido: " << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";
   std::cout << "Done!\n";

#ifdef ACCURACY_TEST
   std::vector< double > xi( TEST_SIZE_M * ( TEST_SIZE_N + TEST_SIZE_S ) );
   std::vector< double > upsilon( TEST_SIZE_M * ( TEST_SIZE_N + TEST_SIZE_S ) );
   dft::pp_grid( TEST_SIZE_M, TEST_SIZE_N, TEST_SIZE_S, &(xi[ 0 ]), &(upsilon[ 0 ]), sigma_h, sigma_v );
//    auto angles = dft::equal_slope_angles( TEST_SIZE_N, TEST_SIZE_S );
//    dft::any_angle_pp_grid( TEST_SIZE_M, angles, &(xi[ 0 ]), &(upsilon[ 0 ]), sigma_h, sigma_v );

   std::vector< std::complex< double > > bf_out( TEST_SIZE_M * ( TEST_SIZE_N + TEST_SIZE_S ) );
   dft::dft( TEST_SIZE_m, TEST_SIZE_n, aux_ptr_img, TEST_SIZE_M, TEST_SIZE_N + TEST_SIZE_S, &(xi[ 0 ]), &(upsilon[ 0 ]), &(bf_out[ 0 ]) );
   double max_err = 0.0;
   for ( unsigned i = 0; i < TEST_SIZE_M; ++i )
      for ( unsigned j = 0; j < TEST_SIZE_N + TEST_SIZE_S; ++j )
      {
         double curr_err = std::abs( aux_ptr_out[ j + i * ( TEST_SIZE_N + TEST_SIZE_S ) ] - bf_out[ j + i * ( TEST_SIZE_N + TEST_SIZE_S ) ] ) / std::abs( bf_out[ j + i * ( TEST_SIZE_N + TEST_SIZE_S ) ] );
         if ( curr_err > max_err )
            max_err = curr_err;
      }
   std::cout << "Maximum relative error: " << max_err << '\n';
#endif

#ifdef DISPLAY
#ifndef LARGE_TEST
   display( TEST_SIZE_M, TEST_SIZE_N + TEST_SIZE_S, out );
   std::cout << "\n\n";
#ifdef ACCURACY_TEST
   display( TEST_SIZE_M, TEST_SIZE_N + TEST_SIZE_S, reinterpret_cast< fftw_complex const * >( &(bf_out[ 0 ]) ) );
   std::cout << "\n\n";
   display_diff( TEST_SIZE_M, TEST_SIZE_N + TEST_SIZE_S, out, reinterpret_cast< fftw_complex const * >( &(bf_out[ 0 ]) ) );
   std::cout << "\n\n";
   display( TEST_SIZE_M, TEST_SIZE_N + TEST_SIZE_S, &(xi[0]) );
   std::cout << "\n\n";
   display( TEST_SIZE_M, TEST_SIZE_N + TEST_SIZE_S, &(upsilon[0]) );
   #endif
//    display( 2 * TEST_SIZE, 2 * TEST_SIZE, adj_img );
//    display( TEST_SIZE, TEST_SIZE, adj_out );
#else
   std:: cout << std::setprecision( 16 )
              << std::scientific
              << reinterpret_cast< std::complex< double > * > ( out )[ 35 + 47 * ( TEST_SIZE_N + TEST_SIZE_S ) ] << '\n';
#endif
#endif

   return 0;
}
