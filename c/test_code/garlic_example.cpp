//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#include "garlic.h"
#include <iostream>
#include <cstdlib>

// #define NON_COMPLEX_INPUT
// #define DFT_COMPARE

int main()
{
#ifndef DFT_COMPARE
//    int m = 400;
//    int n = 400;
//    int M = 400;
//    int N_g = 100;
//    int S = 8;
//    int N = 1024 - ( 4 * S );
   int m = 35;
   int n = 35;
   int M = 100;
   int N_g = 90;
   int S = 6;
   int N = 128 - ( 4 * S );
   #else
   // If comparison is required we use smaller size to avoid wasting time since
   // brute-force DFT takes too long to compute.
   int m = 40;
   int n = 40;
   int M = 40;
   int N_g = 20;
   int S = 8;
   int N = 128 - ( 4 * S );
#endif

   // We will create a GARLiC plan here. Notice that parameter N is chosen so that
   // N + 4 * S is the next power of two larger than 2 * max{ m, n }. It does note need
   // to be so, it only needs to be divisible by 4 and larger than 2 * max{ m, n }, however,
   // It is always a good idea to round N + 4 * S to the next power of two because we will gain
   // precision with low extra computational cost.
   garlic_plan plan = garlic_create( m, n, M, N_g, N, S );

   // Output the angles where the transforms are computed. Notice that the angles are always
   // in the range ( 45, 235 ] and that 180 is always in the set of angles:
   std::vector< double > angles = garlic_angles( plan );
   for ( int i = 0; i < N_g; ++i )
      std::cout << angles[ i ] * 180 / 3.141592653589793238462643383279502884 << '\t';
   std::cout << '\n';

   // Create some random input complex image. We will create using c++ complex type, but double is also accepted.
   // Image is supposed to be in C ordering (each row in sequence, as opposed to FORTRAN collumn ordering), otherwise
   // the meaning of the DFT has columns/rows reversed. If the image you have is not in c++ complex type, but in C
   // complex type, a reinterpret_cast can be used so that no copying is required.
#ifndef NON_COMPLEX_INPUT
   std::complex< double > * x = new std::complex< double >[ m * n ];
   for ( int i = 0; i < m * n; ++i )
//       x[ i ] = std::complex< double >( double( std::rand() ) / RAND_MAX, double( std::rand() ) / RAND_MAX );
      x[ i ] = std::complex< double >( 1.0, 0.5 );
#else
   double * x = new double[ m * n ];
   for ( int i = 0; i < m * n; ++i )
      x[ i ] = double( std::rand() ) / RAND_MAX;
#endif
   // Create space for output:
   std::complex< double > * Ax = new std::complex< double >[ M * N_g ];
   // Compute GARLiC DFT:
   garlic_execute( plan, x, Ax );

   // Create random input complex image for adjoint.
   std::complex< double > * y = new std::complex< double >[ M * N_g ];
   for ( int i = 0; i < M * N_g; ++i )
//       y[ i ] = std::complex< double >( double( std::rand() ) / RAND_MAX, double( std::rand() ) / RAND_MAX );
      y[ i ] = std::complex< double >( 1.0 );
   // Create space for adjoint output:
   std::complex< double > * ATy = new std::complex< double >[ m * n ];
   // Compute GARLiC DFT:
   garlic_execute_adjoint( plan, y, ATy );

   // Check for "adjointeness":
   std::complex< double > yTAx( 0.0 );
   for ( int i = 0; i < M * N_g; ++i )
      yTAx += std::conj( y[ i ] ) * Ax[ i ];

   std::complex< double > xTATy( 0.0 );
   for ( int i = 0; i < m * n; ++i )
      xTATy += std::conj( x[ i ] ) * ATy[ i ];

   // Result should be 1:
   std::cout << xTATy << '\t' << yTAx << '\n';
   std::cout << std::conj( xTATy ) / yTAx << '\n';

#ifdef DFT_COMPARE
   std::complex< double > * bf_Ax = new std::complex< double >[ M * N_g ];

   garlic_execute_dft( plan, x, bf_Ax );

   double max_err = 0.0;
   for ( int i = 0; i < M * N_g; ++i )
   {
      double curr_err = std::abs( Ax[ i ] - bf_Ax[ i ] ) / std::abs( bf_Ax[ i ] );
      if ( curr_err > max_err )
         max_err = curr_err;
   }

   std::cout << "Maximum relative error: " << max_err << '\n';

   delete[] bf_Ax;
#endif

   delete[] ATy;
   delete[] y;
   delete[] Ax;
   delete[] x;

   // Clean up plan:
   garlic_destroy( plan );

   return 0;
}

// Instructions for compilation under Linux.

// 0) Install fftw3 library:
// sudo apt-get install libfftw3-dev

// 1) First compile the GARLiC library:
// g++ -O3 -Wall -c -o garlic.o garlic.cpp

// 2) Compile and link the program:
// g++ -O3 -Wall -o garlic_example garlic_example.cpp garlic.o -lfftw3 -lfftw3_threads -lpthread -lm

// 3) Run the resulting executable:
// ./garlic_example

// Everything in one line:
// g++ -O3 -Wall -c -o garlic.o garlic.cpp && g++ -O3 -Wall -o garlic_example garlic_example.cpp garlic.o -lfftw3 -lfftw3_threads -lpthread -lm && ./garlic_example

// 4) If separate compilation is complicated, replace
// #include "garlic.h"
// by
// #include "garlic.cpp"
// and compile with
// g++ -O3 -Wall -o garlic_example garlic_example.cpp -lfftw3 -lfftw3_threads -lpthread -lm

// Some ideas about how to create a Matlab function from these functions:
// 1) you should have four routines, one for plan creation, one for direct computation, one for
//    adjoint computation and finally one for plan destruction.
//
// 2) The pointer returned by the creation function should be cast as a 64 bits integer, see these examples:
// https://www.mathworks.com/matlabcentral/answers/75524-returning-and-passing-void-pointers-to-mex-functions?requestedDomain=true
//
// 3) I believe there is no need to have Matlab bindings for the garlic_execute_dft function, it is for tessting purposes only. Undocument line 6 of the present file to have an accuracy comparison between the fast and the brute-force DFT computations at the golden-angle linogram samples.
