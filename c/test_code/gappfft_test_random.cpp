//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

//#define GPFFT_ALWAYS_ODD_SUMMANDS
#include "gppfft.h"
#include "dft.h"
#include "test_utils.h"

#include <iostream>
#include <chrono>
#include <nfft3.h>

int HK = 5;
int OS = 0.0;

int NHK = 4;
int NOS = 1;

int m = 512;
int n = 512;
int N = 400;
// int m = 50;
// int n = 50;
// int N = 40;

double sigma_h = dft::PI / std::max( m, n );
double sigma_v = dft::PI / std::max( m, n );
// double sigma_h = 0.0;
// double sigma_v = 0.0;

// #define DISPLAY
#define ACCURACY_TEST
#define REL_ERROR
// #define DISPLAY_ANGLES
// #define CONST_ARRAY
// #define SQUARE
#define NFFT_COMPARE
#define SAVE_DATA

int nthreads = 8;

int main()
{
   std::vector< double > angles = dft::golden_angles( N );

   std::cout << "Initializing GPPFFT transform...\n";
   std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
   //++++++++++++++++++++++++++++++++++++++++++
   gppfft<> gpp_obj( m, n, &( angles[ 0 ] ), N, 0, HK, 1.0, 2.5 * std::max( m, n ), ( HK + 1 ) * 4, 0, FFTW_MEASURE, 0 );
   std::cout << gpp_obj.OS_ << '\t' << gpp_obj.ppfft_object_.N_ + gpp_obj.ppfft_object_.S_ << '\n';
   //++++++++++++++++++++++++++++++++++++++++++
   std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
   std::cout << "Elapsed: " << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";
   std::cout << "Done!\n";

#ifdef NFFT_COMPARE
   std::cout << "Initializing NFFT transform...\n";
   double * xi = new double[ gpp_obj.ppfft_object_.M_ * N ];
   double * upsilon = new double[ gpp_obj.ppfft_object_.M_ * N ];
   dft::any_angle_pp_grid( gpp_obj.ppfft_object_.M_, angles, xi, upsilon, sigma_h, sigma_v );
   int NN[ 2 ] = { m , n };
   int nn[ 2 ];
   nn[ 0 ] = m * gppfft<>::OS_compute( m, m, -1, NOS );
   nn[ 1 ] = n * gppfft<>::OS_compute( n, n, -1, NOS );
   std::cout << nn[ 0 ] << '\t' << nn[ 1 ] << '\n';
   begin = std::chrono::steady_clock::now();
   //++++++++++++++++++++++++++++++++++++++++++
   nfft_plan p;
   nfft_init_guru( &p, 2, NN, gpp_obj.ppfft_object_.M_ * N, nn, NHK, PRE_PHI_HUT | PRE_PSI | MALLOC_X | MALLOC_F | MALLOC_F_HAT | FFTW_INIT | FFT_OUT_OF_PLACE, FFTW_MEASURE );
   std::cout << gpp_obj.ppfft_object_.M_ * N << '\t' << p.M_total << '\t' << m * n << '\t' << p.N_total << '\n';
   for ( int i = 0; i < gpp_obj.ppfft_object_.M_ * N; ++i )
   {
      p.x[ i * 2 + 1 ] = xi[ i ] / ( 2.0 * dft::PI );
      p.x[ i * 2 ] = upsilon[ i ] / ( 2.0 * dft::PI );
   }
   nfft_precompute_one_psi( &p );
   if( p.flags & PRE_LIN_PSI )
      nfft_precompute_lin_psi( &p );
   if( p.flags & PRE_PSI )
      nfft_precompute_psi( &p );
   if( p.flags & PRE_FULL_PSI )
      nfft_precompute_full_psi( &p );
   end = std::chrono::steady_clock::now();
   std::cout << "Elapsed: " << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";
   std::cout << "Done!\n";
#endif

   #ifdef DISPLAY_ANGLES
   std::vector< double > pp_angles = dft::equal_slope_angles( gpp_obj.ppfft_object_.N_, gpp_obj.ppfft_object_.S_ );

   for ( int j = 0; j < N; ++ j )
   {
      std::cout << angles[ j ] * 180 / dft::PI << '\n';
      int K = gpp_obj.combination_indices_[ j ].size();
      for ( int k = 0; k < K; ++k )
         std::cout << pp_angles[ gpp_obj.combination_indices_[ j ][ k ] ] * 180.0 / dft::PI << '\t';
      std::cout << '\n';
      std::cout << '\n';
   }
#endif

   std::cout << "Initializing test images...\n";
   //++++++++++++++++++++++++++++++++++++++++++
#ifndef CONST_ARRAY
   std::complex< double > * img = test_utils::random_array( m, n );
#else
#ifdef SQUARE
   std::complex< double > * img = test_utils::square( m, n );
#else
   std::complex< double > * img = test_utils::const_array( m, n );
#endif
#endif
   std::complex< double > * adj_img = test_utils::random_array( gpp_obj.ppfft_object_.M_, N );

   std::complex< double > * out = new std::complex< double >[ gpp_obj.ppfft_object_.M_ * N ];
   std::complex< double > * adj_out = new std::complex< double >[  m * n ];
   //++++++++++++++++++++++++++++++++++++++++++
   std::cout << "Done!\n";

   std::cout << "Computing GPPFFT...\n";
   begin = std::chrono::steady_clock::now();
   //++++++++++++++++++++++++++++++++++++++++++
   gpp_obj.direct( img, out );
   //++++++++++++++++++++++++++++++++++++++++++
   end = std::chrono::steady_clock::now();
   std::cout << "Elapsed: " << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";
   std::cout << "Done!\n";
   std::cout << "Computing adjoint GPPFFT...\n";
   begin = std::chrono::steady_clock::now();
   //++++++++++++++++++++++++++++++++++++++++++
   gpp_obj.adjoint( adj_img, adj_out );
   //++++++++++++++++++++++++++++++++++++++++++
   end = std::chrono::steady_clock::now();
   std::cout << "Elapsed: " << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";
   std::cout << "Done!\n";

   std::complex< double > sum_a = test_utils::inner_prod( m, n, img, adj_out );
   std::complex< double > sum_b = test_utils::inner_prod( gpp_obj.ppfft_object_.M_, N, out, adj_img );
   std::cout << sum_a << '\t' << sum_b << '\t' << sum_a / sum_b << '\n';

#ifdef NFFT_COMPARE
   std::complex< double > * nfft_autx_ptr = reinterpret_cast< std::complex< double > * >( p.f_hat );
   for ( int i = 0; i < m * n; ++i )
      nfft_autx_ptr[ i ] = img[ i ];

   std::cout << "Computing NFFT...\n";
   begin = std::chrono::steady_clock::now();
   //++++++++++++++++++++++++++++++++++++++++++
   nfft_trafo( &p );
   //++++++++++++++++++++++++++++++++++++++++++
   end = std::chrono::steady_clock::now();
   std::cout << "Elapsed: " << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";
   std::cout << "Done!\n";
   nfft_autx_ptr = reinterpret_cast< std::complex< double > * >( p.f );
   for ( int i = 0; i < p.M_total; ++i )
      nfft_autx_ptr[ i ] *= std::exp( std::complex< double >( 0.0, -2 * dft::PI * ( p.x[ 2 * i ] * ( m /  2 ) + p.x[ 2 * i + 1 ] * ( n /  2 ) ) ) );
#endif

#ifdef ACCURACY_TEST
   std::complex< double > * bf_out = new std::complex< double >[ gpp_obj.ppfft_object_.M_ * N ];
   std::cout << "Computing DFT...\n";
   begin = std::chrono::steady_clock::now();
   //++++++++++++++++++++++++++++++++++++++++++
#ifndef NFFT_COMPARE
   double * xi = new double[ gpp_obj.ppfft_object_.M_ * N ];
   double * upsilon = new double[ gpp_obj.ppfft_object_.M_ * N ];
   dft::any_angle_pp_grid( gpp_obj.ppfft_object_.M_, angles, xi, upsilon );
#endif
   dft::dft( m, n, img,
             gpp_obj.ppfft_object_.M_, N, xi, upsilon, bf_out
           );

   //++++++++++++++++++++++++++++++++++++++++++
   end = std::chrono::steady_clock::now();
   std::cout << "Elapsed: " << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";
   std::cout << "Done!\n";

   int i, j;
#ifndef REL_ERROR
   double err = test_utils::max_diff( gpp_obj.ppfft_object_.M_, N, out, bf_out, i, j );
   std::cout << "GPFFT absolute errors:\n";
   std::cout << err << '\t' << i << '\t' << j  << '\t' << angles[ j ] * 180.0 / dft::PI << '\n';
   err = test_utils::mean_diff( gpp_obj.ppfft_object_.M_, N, out, bf_out );
   std::cout << err << '\n';
   std::cout << "+++++++++++++++++++++++++++++++\n";
#ifdef NFFT_COMPARE
   err = test_utils::max_diff( gpp_obj.ppfft_object_.M_, N, p.f, bf_out, i, j );
   std::cout << "NFFT absolute errors:\n";
   std::cout << err << '\t' << i << '\t' << j  << '\t' << angles[ j ] * 180.0 / dft::PI << '\n';
   err = test_utils::mean_diff( gpp_obj.ppfft_object_.M_, N, p.f, bf_out );
   std::cout << err << '\n';
   std::cout << "+++++++++++++++++++++++++++++++\n";
#endif
#else
   double err = test_utils::max_rel_diff( gpp_obj.ppfft_object_.M_, N, out, bf_out, i, j );
   std::cout << "GPFFT relative errors:\n";
   std::cout << err << '\t' << i << '\t' << j  << '\t' << angles[ j ] * 180.0 / dft::PI << '\n';
   err = test_utils::mean_rel_diff( gpp_obj.ppfft_object_.M_, N, out, bf_out );
   std::cout << err << '\n';
   std::cout << "+++++++++++++++++++++++++++++++\n";
#ifdef NFFT_COMPARE
   err = test_utils::max_rel_diff( gpp_obj.ppfft_object_.M_, N, p.f, bf_out, i, j );
   std::cout << "NFFT relative errors:\n";
   std::cout << err << '\t' << i << '\t' << j  << '\t' << angles[ j ] * 180.0 / dft::PI << '\n';
   err = test_utils::mean_rel_diff( gpp_obj.ppfft_object_.M_, N, p.f, bf_out );
   std::cout << err << '\n';
   std::cout << "+++++++++++++++++++++++++++++++\n";
#endif
#endif
#endif

#ifdef DISPLAY
   test_utils::display( gpp_obj.ppfft_object_.M_, N, bf_out );
   std::cout << '\n';
   test_utils::display( gpp_obj.ppfft_object_.M_, N, p.f );
   std::cout << '\n';
   test_utils::display( gpp_obj.ppfft_object_.M_, N, out );
#endif

#ifdef SAVE_DATA
   test_utils::save_binary( gpp_obj.ppfft_object_.M_, N, out, "gppfft.bin" );
#ifdef ACCURACY_TEST
   test_utils::save_binary( gpp_obj.ppfft_object_.M_, N, bf_out, "dft.bin" );
#endif
#ifdef NFFT_COMPARE
   test_utils::save_binary( gpp_obj.ppfft_object_.M_, N, p.f, "nfft.bin" );
#endif
#endif

// Testing "transposition time:"
begin = std::chrono::steady_clock::now();
for ( int i = 0; i < gpp_obj.ppfft_object_.M_; ++i )
   for ( int j = 0; j < N; ++j )
      bf_out[ i * N + j ] = out[ j * gpp_obj.ppfft_object_.M_+ i ];
end = std::chrono::steady_clock::now();
std::cout << "Elapsed: " << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";


#ifdef ACCURACY_TEST
   delete[] bf_out;
#endif
#if defined (ACCURACY_TEST) || defined (NFFT_COMPARE)
   delete[] upsilon;
   delete[] xi;
#endif
   delete[] adj_out;
   delete[] out;
   delete[] adj_img;
   delete[] img;

   return 0;
}

// Everything in one line:
// g++ -O3 -Wall -o gappfft_test_random gappfft_test_random.cpp -lnfft3 -lfftw3 -lfftw3_threads -lpthread -lm && ./gappfft_test_random
