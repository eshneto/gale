//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#include "kaiser_bessel.h"
#include <iostream>
#include <complex>

double const alpha = 6.28;
double const tau = 25.0;
int n_vals = 500;

std::complex< double > fourier_estimate( double omega, double tau, kb::kb<> & kb_obj, double epsilon = 1e-6 )
{
   std::complex< double > retval = 0.0;

   std::complex< double > neg_iomega( 0.0, -omega );

   for ( double x = -tau; x <= tau; x += epsilon )
      retval += kb_obj( x ) * std::exp( neg_iomega * x );

   return retval * epsilon;
}

int main()
{
   kb::kb<> kb_obj( alpha, tau );

   for ( int i = -n_vals / 2; i < n_vals / 2; ++i  )
   {
      double omega = 2.0 * 3.14 * i / n_vals;
      std::cout << omega << '\t' << kb_obj.fourier( omega ) / fourier_estimate( omega, tau, kb_obj, 2.0 * tau / 1e7 ) << '\n';
   }

   return 0;
}
