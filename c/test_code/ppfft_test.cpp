//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#include <fftw3.h>
#include <iomanip>
#include <iostream>
#include <complex>
template< int width = 26 >
void display( int m, int n, fftw_complex const * matrix )
{
   std::complex< double > const * aux_ptr = reinterpret_cast< std::complex< double > const * >( matrix );
   for ( int i = 0; i < m; ++i )
   {
      for ( int j = 0; j < n; ++j )
         std::cout << std::fixed
                   << std::setprecision( 4 )
                   << std::showpoint
                   << std::setw( width ) << aux_ptr[ j + i * n ];

      std::cout << '\n';
   }
}

template< int width = 26 >
void display_transpose( int m, int n, fftw_complex const * matrix )
{
   std::complex< double > const * aux_ptr = reinterpret_cast< std::complex< double > const * >( matrix );
   for ( int j = 0; j < n; ++j )
   {
      for ( int i = 0; i < m; ++i )
         std::cout << std::fixed
                   << std::setprecision( 4 )
                   << std::showpoint
                   << std::setw( width ) << aux_ptr[ j + i * n ];

      std::cout << '\n';
   }
}

// #define PM_PROFILING_ON

#include "ppfft.h"

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <chrono>

// #define LARGE_TEST
#define INPUT_COMPLEX

#ifndef LARGE_TEST
#define TEST_SIZE_m 8
#define TEST_SIZE_n 8
#define TEST_SIZE_M 8
#define TEST_SIZE_N 16
#else
#define TEST_SIZE_m 2048
#define TEST_SIZE_n 2048
#define TEST_SIZE_M 4096
#define TEST_SIZE_N 4096
#endif

int main()
{
   std::cerr << "Initializing transform...\n";
   std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
   //++++++++++++++++++++++++++++++++++++++++++
   ppfft pp_obj( TEST_SIZE_m, TEST_SIZE_n, TEST_SIZE_M, TEST_SIZE_N );
   //++++++++++++++++++++++++++++++++++++++++++
   std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
   std::cout << "Decorrido: " << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";
   std::cerr << "Done!\n";

   std::cerr << "Initializing test image...\n";
   //++++++++++++++++++++++++++++++++++++++++++
#ifndef INPUT_COMPLEX
   double * img = static_cast< double* >( std::malloc( TEST_SIZE_m * TEST_SIZE_n * sizeof( double ) ) );
   double * img_copy = static_cast< double* >( std::malloc( TEST_SIZE_m * TEST_SIZE_n * sizeof( double ) ) );
   for( unsigned i = 0; i < TEST_SIZE_m; ++i )
      for( unsigned j = 0; j < TEST_SIZE_n; ++j )
         img_copy[ j + i * TEST_SIZE_n ] = img[ j + i * TEST_SIZE_n ] = i + ( j * j );
   double * adj_img = static_cast< double* >( std::malloc( TEST_SIZE_M * TEST_SIZE_N * sizeof( double ) ) );
   for( unsigned i = 0; i < TEST_SIZE_M; ++i )
      for( unsigned j = 0; j < TEST_SIZE_N; ++j )
         adj_img[ j + i * TEST_SIZE_N ] = i + ( j * j );
#else
   fftw_complex * img = fftw_alloc_complex( TEST_SIZE_m * TEST_SIZE_n );
   fftw_complex * img_copy = fftw_alloc_complex( TEST_SIZE_m * TEST_SIZE_n );
   for( unsigned i = 0; i < TEST_SIZE_m; ++i )
      for( unsigned j = 0; j < TEST_SIZE_n; ++j )
      {
         img_copy[ j + i * TEST_SIZE_n ][ 0 ] = img[ j + i * TEST_SIZE_n ][ 0 ] = i + ( j * j );
         img_copy[ j + i * TEST_SIZE_n ][ 1 ] = img[ j + i * TEST_SIZE_n ][ 1 ] = 0.0;
      }
   fftw_complex * adj_img = fftw_alloc_complex( TEST_SIZE_M * TEST_SIZE_N );
   for( unsigned i = 0; i < TEST_SIZE_M; ++i )
      for( unsigned j = 0; j < TEST_SIZE_N; ++j )
      {
         adj_img[ j + i * TEST_SIZE_N ][ 0 ] = i + ( j * j );
         adj_img[ j + i * TEST_SIZE_N ][ 1 ] = 0.0;
      }
#endif
   fftw_complex * out = fftw_alloc_complex( TEST_SIZE_M * TEST_SIZE_N );
   fftw_complex * adj_out = fftw_alloc_complex( TEST_SIZE_m * TEST_SIZE_n );
   //++++++++++++++++++++++++++++++++++++++++++
   std::cerr << "Done!\n";

   std::cerr << "Computing PPFFT...\n";
   begin = std::chrono::steady_clock::now();

   std::complex< double > sum_a = 0.0;
   std::complex< double > sum_b = 0.0;

   pp_obj.direct( img, out );
   auto aux_ptr_adj_img = reinterpret_cast< std::complex< double > const * >( adj_img );
   auto aux_ptr_out = reinterpret_cast< std::complex< double > const * >( out );
   for( unsigned i = 0; i < TEST_SIZE_M; ++i )
      for( unsigned j = 0; j < TEST_SIZE_N; ++j )
         sum_b += aux_ptr_adj_img[ j + i * TEST_SIZE_N ] * std::conj( aux_ptr_out[ j + i * TEST_SIZE_N ] );

   pp_obj.adjoint( adj_img, adj_out );
   auto aux_ptr_img = reinterpret_cast< std::complex< double > const * >( img_copy );
   auto aux_ptr_adj_out = reinterpret_cast< std::complex< double > const * >( adj_out );
   for( unsigned i = 0; i < TEST_SIZE_m; ++i )
      for( unsigned j = 0; j < TEST_SIZE_n; ++j )
         sum_a += std::conj( aux_ptr_img[ j + i * TEST_SIZE_n ] ) * aux_ptr_adj_out[ j + i * TEST_SIZE_n ];

   std::cout << sum_a << '\t' << sum_b << '\t' << sum_a / sum_b << '\n';


#ifndef LARGE_TEST
   display( TEST_SIZE_M, TEST_SIZE_N, out );
//    display( 2 * TEST_SIZE, 2 * TEST_SIZE, adj_img );
//    display( TEST_SIZE, TEST_SIZE, adj_out );
   #else
   std:: cout << std::setprecision( 16 )
              << std::scientific
              << reinterpret_cast< std::complex< double > * > ( out )[ 35 + 47 * TEST_SIZE_N ] << '\n';
#endif

   end = std::chrono::steady_clock::now();
   std::cout << "Decorrido: " << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";
   std::cerr << "Done!\n";

   return 0;
}
