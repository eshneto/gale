//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#ifndef TEST_UTILS_H
#define TEST_UTILS_H

#include <fftw3.h>
#include <complex>
#include <iostream>
#include <iomanip>
#include <fstream>

namespace test_utils{

   template< class data_t >
   struct type_provider{
   };
   template<>
   struct type_provider< double >{
      typedef double * pointer_t; ///< Pointer type for real data
      typedef double const * const_pointer_t; ///< Pointer type for const real data
   };
   template<>
   struct type_provider< fftw_complex >{
      typedef std::complex< double > * pointer_t; ///< Pointer type for complex data
      typedef std::complex< double > const * const_pointer_t; ///< Pointer type for const complex data
   };
   template<>
   struct type_provider< std::complex< double > >{
      typedef std::complex< double > * pointer_t; ///< Pointer type for complex data
      typedef std::complex< double > const * const_pointer_t; ///< Pointer type for const complex data
   };

   template< class data_t_a, class data_t_b, int width = 12 >
   void display_rel_diff( int m, int n, data_t_a const * matrix_a, data_t_b const * matrix_b )
   {
      typedef typename type_provider< data_t_a >::const_pointer_t type_a;
      typedef typename type_provider< data_t_b >::const_pointer_t type_b;
      auto aux_ptr_a = reinterpret_cast< type_a >( matrix_a );
      auto aux_ptr_b = reinterpret_cast< type_b >( matrix_b );
      for ( int i = 0; i < m; ++i )
      {
         for ( int j = 0; j < n; ++j )
            std::cout << std::fixed
                      << std::setprecision( 4 )
                      << std::showpoint
                      << std::setw( width ) << std::abs( aux_ptr_a[ j + i * n ] - aux_ptr_b[ j + i * n ] ) / std::abs( aux_ptr_b[ j + i * n ] );
         std::cout << '\n';
      }
   }

   template< int width = 12, class data_t_a, class data_t_b >
   void display_diff( int m, int n, data_t_a const * matrix_a, data_t_b const * matrix_b )
   {
      typedef typename type_provider< data_t_a >::const_pointer_t type_a;
      typedef typename type_provider< data_t_b >::const_pointer_t type_b;
      auto aux_ptr_a = reinterpret_cast< type_a >( matrix_a );
      auto aux_ptr_b = reinterpret_cast< type_b >( matrix_b );
      for ( int i = 0; i < m; ++i )
      {
         for ( int j = 0; j < n; ++j )
            std::cout << std::fixed
                      << std::setprecision( 4 )
                      << std::showpoint
                      << std::setw( width ) << std::abs( aux_ptr_a[ j + i * n ] - aux_ptr_b[ j + i * n ] );
         std::cout << '\n';
      }
   }

   template< int width = 23, class data_t >
   void display( int m, int n, data_t const * matrix )
   {
      typedef typename type_provider< data_t >::const_pointer_t type;
      auto aux_ptr = reinterpret_cast< type >( matrix );
      for ( int i = 0; i < m; ++i )
      {
         for ( int j = 0; j < n; ++j )
            std::cout << std::fixed
                      << std::setprecision( 4 )
                      << std::showpoint
                      << std::setw( width ) << aux_ptr[ j + i * n ];
         std::cout << '\n';
      }
   }

   template< int width = 12, class data_t >
   void display_abs( int m, int n, data_t const * matrix )
   {
      typedef typename type_provider< data_t >::const_pointer_t type;
      auto aux_ptr = reinterpret_cast< type >( matrix );
      for ( int i = 0; i < m; ++i )
      {
         for ( int j = 0; j < n; ++j )
            std::cout << std::fixed
                      << std::setprecision( 4 )
                      << std::showpoint
                      << std::setw( width ) << std::abs( aux_ptr[ j + i * n ] );
         std::cout << '\n';
      }
   }

   template< class data_t_a, class data_t_b >
   double max_rel_diff( int m, int n, data_t_a const * matrix_a, data_t_b const * matrix_b )
   {
      typedef typename type_provider< data_t_a >::const_pointer_t type_a;
      typedef typename type_provider< data_t_b >::const_pointer_t type_b;

      auto aux_ptr_a = reinterpret_cast< type_a >( matrix_a );
      auto aux_ptr_b = reinterpret_cast< type_b >( matrix_b );

      double retval = 0.0;

      for ( int i = 0; i < m; ++i )
         for ( int j = 0; j < n; ++j )
         {
            double curr_err = std::abs( aux_ptr_a[ j + i * n ] - aux_ptr_b[ j + i * n ] ) / std::abs( aux_ptr_b[ j + i * n ] );
            if ( curr_err > retval )
               retval = curr_err;
         }

      return retval;
   }

   template< class data_t_a, class data_t_b >
   double max_rel_diff( int m, int n, data_t_a const * matrix_a, data_t_b const * matrix_b, int & i_max, int & j_max )
   {
      typedef typename type_provider< data_t_a >::const_pointer_t type_a;
      typedef typename type_provider< data_t_b >::const_pointer_t type_b;

      auto aux_ptr_a = reinterpret_cast< type_a >( matrix_a );
      auto aux_ptr_b = reinterpret_cast< type_b >( matrix_b );

      double retval = 0.0;
      i_max = j_max = 0;

      for ( int i = 0; i < m; ++i )
         for ( int j = 0; j < n; ++j )
         {
            double curr_err = std::abs( aux_ptr_a[ j + i * n ] - aux_ptr_b[ j + i * n ] ) / std::abs( aux_ptr_b[ j + i * n ] );
            if ( curr_err > retval )
            {
               retval = curr_err;
               i_max = i;
               j_max = j;
            }
         }

      return retval;
   }

   template< class data_t_a, class data_t_b >
   double max_diff( int m, int n, data_t_a const * matrix_a, data_t_b const * matrix_b )
   {
      typedef typename type_provider< data_t_a >::const_pointer_t type_a;
      typedef typename type_provider< data_t_b >::const_pointer_t type_b;

      auto aux_ptr_a = reinterpret_cast< type_a >( matrix_a );
      auto aux_ptr_b = reinterpret_cast< type_b >( matrix_b );

      double retval = 0.0;

      for ( int i = 0; i < m; ++i )
         for ( int j = 0; j < n; ++j )
         {
            double curr_err = std::abs( aux_ptr_a[ j + i * n ] - aux_ptr_b[ j + i * n ] );
            if ( curr_err > retval )
               retval = curr_err;
         }

      return retval;
   }

   template< class data_t_a, class data_t_b >
   double max_diff( int m, int n, data_t_a const * matrix_a, data_t_b const * matrix_b, int & i_max, int & j_max )
   {
      typedef typename type_provider< data_t_a >::const_pointer_t type_a;
      typedef typename type_provider< data_t_b >::const_pointer_t type_b;

      auto aux_ptr_a = reinterpret_cast< type_a >( matrix_a );
      auto aux_ptr_b = reinterpret_cast< type_b >( matrix_b );

      double retval = 0.0;
      i_max = j_max = 0;

      for ( int i = 0; i < m; ++i )
         for ( int j = 0; j < n; ++j )
         {
            double curr_err = std::abs( aux_ptr_a[ j + i * n ] - aux_ptr_b[ j + i * n ] );
            if ( curr_err > retval )
            {
               retval = curr_err;
               i_max = i;
               j_max = j;
            }
         }

      return retval;
   }

   template< class data_t_a, class data_t_b >
   double mean_rel_diff( int m, int n, data_t_a const * matrix_a, data_t_b const * matrix_b )
   {
      typedef typename type_provider< data_t_a >::const_pointer_t type_a;
      typedef typename type_provider< data_t_b >::const_pointer_t type_b;

      auto aux_ptr_a = reinterpret_cast< type_a >( matrix_a );
      auto aux_ptr_b = reinterpret_cast< type_b >( matrix_b );

      double retval = 0.0;

      for ( int i = 0; i < m; ++i )
         for ( int j = 0; j < n; ++j )
            retval += std::abs( aux_ptr_a[ j + i * n ] - aux_ptr_b[ j + i * n ] ) / std::abs( aux_ptr_b[ j + i * n ] );

      return retval / ( m * n );
   }

   template< class data_t_a, class data_t_b >
   double mean_diff( int m, int n, data_t_a const * matrix_a, data_t_b const * matrix_b )
   {
      typedef typename type_provider< data_t_a >::const_pointer_t type_a;
      typedef typename type_provider< data_t_b >::const_pointer_t type_b;

      auto aux_ptr_a = reinterpret_cast< type_a >( matrix_a );
      auto aux_ptr_b = reinterpret_cast< type_b >( matrix_b );

      double retval = 0.0, sum = 0.0;

      for ( int i = 0; i < m; ++i )
         for ( int j = 0; j < n; ++j )
         {
            retval += std::abs( aux_ptr_a[ j + i * n ] - aux_ptr_b[ j + i * n ] );
            sum += std::abs( aux_ptr_b[ j + i * n ] );
         }

      return retval / sum;
   }

   template< class data_t_a, class data_t_b >
   std::complex< double > inner_prod( int m, int n, data_t_a const * matrix_a, data_t_b const * matrix_b )
   {
      typedef typename type_provider< data_t_a >::const_pointer_t type_a;
      typedef typename type_provider< data_t_b >::const_pointer_t type_b;

      auto aux_ptr_a = reinterpret_cast< type_a >( matrix_a );
      auto aux_ptr_b = reinterpret_cast< type_b >( matrix_b );

      std::complex< double > retval = 0.0;

      for ( int i = 0; i < m; ++i )
         for ( int j = 0; j < n; ++j )
            retval += std::conj( aux_ptr_a[ j + i * n ] ) * aux_ptr_b[ j + i * n ];

      return retval;
   }

   template < class T >
   T random_element();
   template <>
   std::complex< double > random_element< std::complex< double > >()
   {
      return std::complex< double >( double( std::rand() ) / RAND_MAX, double( std::rand() ) / RAND_MAX );
   }
   template <>
   double random_element< double >()
   {
      return double( std::rand() ) / RAND_MAX;
   }

   template< class data_t = std::complex< double > >
   data_t * random_array( int m, int n )
   {
      data_t * retval = new data_t[ m * n ];

      for ( int i = 0; i < m; ++i )
         for ( int j = 0; j < n; ++j )
            retval[ j + i * n ] = random_element< data_t >();

      return retval;
   }

   template< class data_t = std::complex< double > >
   data_t * square( int m, int n )
   {
      data_t * retval = new data_t[ m * n ];

      for ( int i = 0; i < m / 4; ++i )
         for ( int j = 0; j < n; ++j )
            retval[ j + i * n ] = 0.0;
      for ( int i = m / 4; i < 3 * m / 2; ++i )
      {
         for ( int j = 0; j < n / 2; ++j )
            retval[ j + i * n ] = 0.0;
         for ( int j = n / 2; j < 3 * n / 2; ++j )
            retval[ j + i * n ] = 1.0;
         for ( int j = 3 * n / 2; j < n; ++j )
            retval[ j + i * n ] = 0.0;
      }
      for ( int i = 3 * m / 2; i < m; ++i )
         for ( int j = 0; j < n; ++j )
            retval[ j + i * n ] = 0.0;

      return retval;
   }

   template< class data_t = std::complex< double > >
   data_t * const_array( int m, int n, double val = 1.0 )
   {
      data_t * retval = new data_t[ m * n ];

      for ( int i = 0; i < m; ++i )
         for ( int j = 0; j < n; ++j )
            retval[ j + i * n ] = val;

      return retval;
   }

   template< class data_t >
   void save_binary( int m, int n, data_t const * arr, char const * filename )
   {
      std::ofstream out_file( filename, std::ios::binary );

      out_file.write( reinterpret_cast< char const * >( &m ), sizeof( int ) );
      out_file.write( reinterpret_cast< char const * >( &n ), sizeof( int ) );

      out_file.write( reinterpret_cast< char const * >( arr ), m * n * sizeof( data_t ) );
   }

   template< class data_t >
   void load_binary( int & m, int & n, data_t * & arr, char const * filename )
   {
      std::ifstream in_file( filename, std::ios::binary );

      in_file.read( reinterpret_cast< char * >( &m ), sizeof( int ) );
      in_file.read( reinterpret_cast< char * >( &n ), sizeof( int ) );

      arr = new data_t[ m * n ];
      in_file.read( reinterpret_cast< char * >( arr ), m * n * sizeof( data_t ) );
   }
} //namespace test_utils

#endif // #ifndef TEST_UTILS_H
