//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#ifndef UTILS_H
#define UTILS_H

#include <fstream>
#include <vector>
#include <thread>
#include <complex>

void save_image_complex( char const * filename, int m, int n, fftw_complex * img )
{
   std::ofstream file( filename, std::ios::binary );
   file.write( reinterpret_cast< char* >( &m ), sizeof( int ) );
   file.write( reinterpret_cast< char* >( &n ), sizeof( int ) );

   file.write( reinterpret_cast< char* >( img ), m * n * sizeof( fftw_complex ) );
}

fftw_complex * read_image_complex( char const * filename, int & m, int & n )
{
   std::ifstream file( filename, std::ios::binary );
   if ( !file.is_open() )
      return 0;

   file.read( reinterpret_cast< char* >( &m ), sizeof( int ) );
   file.read( reinterpret_cast< char* >( &n ), sizeof( int ) );

   fftw_complex * ret_val = fftw_alloc_complex( m  * n );

   file.read( reinterpret_cast< char* >( ret_val ), m * n * sizeof( fftw_complex ) );

   return ret_val;
}

fftw_complex * read_image( char const * filename, int & m, int & n )
{
   std::ifstream file( filename, std::ios::binary );
   if ( !file.is_open() )
      return 0;

   file.read( reinterpret_cast< char* >( &m ), sizeof( int ) );
   file.read( reinterpret_cast< char* >( &n ), sizeof( int ) );

   double * buffer = new double[ m * n ];
   fftw_complex * ret_val = fftw_alloc_complex( m  * n );

   file.read( reinterpret_cast< char* >( buffer ), m * n * sizeof( fftw_complex ) );
   for ( int i = 0; i < m; ++i )
      for ( int j = 0; j < n; ++j )
      {
         ret_val[ j + i * n ][ 0 ] = buffer[ j + i * n ];
         ret_val[ j + i * n ][ 1 ] = 0.0;
      }
   delete[] buffer;

   return ret_val;
}

void sum_imgs_real( int m, int n, fftw_complex * imga, fftw_complex const * imgb, double step )
{
   for ( int i = 0; i < m; ++i )
      for ( int j = 0; j < n; ++j )
      {
         imga[ j + i * n ][ 0 ] += step * imgb[ j + i * n ][ 0 ];
      }
}

void sum_imgs_complex( int m, int n, fftw_complex * imga, fftw_complex const * imgb, double step )
{
   for ( int i = 0; i < m; ++i )
      for ( int j = 0; j < n; ++j )
      {
         imga[ j + i * n ][ 0 ] += step * imgb[ j + i * n ][ 0 ];
         imga[ j + i * n ][ 1 ] += step * imgb[ j + i * n ][ 1 ];
      }
}

void sum_imgs_complex_worker( int m, int n, fftw_complex * imga, fftw_complex const * imgb, double step, int thread_id, int n_threads )
{
   for ( int i = thread_id; i < m; i += n_threads )
      for ( int j = 0; j < n; ++j )
      {
         imga[ j + i * n ][ 0 ] += step * imgb[ j + i * n ][ 0 ];
         imga[ j + i * n ][ 1 ] += step * imgb[ j + i * n ][ 1 ];
      }
}

void sum_imgs_complex( int m, int n, fftw_complex * imga, fftw_complex const * imgb, double step, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( sum_imgs_complex_worker, m, n, imga, imgb, step, i, n_threads ) );

   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

void sum_imgs_complex_complex_worker( int m, int n, fftw_complex * imga, fftw_complex const * imgb, std::complex< double > step, int thread_id, int n_threads )
{
   auto ptr_a = reinterpret_cast< std::complex< double > * >( imga );
   auto ptr_b = reinterpret_cast< std::complex< double > const * >( imgb );

   for ( int i = thread_id; i < m; i += n_threads )
      for ( int j = 0; j < n; ++j )
         ptr_a[ j + i * n ] += step * ptr_b[ j + i * n ];
}

void sum_imgs_complex( int m, int n, fftw_complex * imga, fftw_complex const * imgb, std::complex< double > step, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( sum_imgs_complex_complex_worker, m, n, imga, imgb, step, i, n_threads ) );

   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

void  diff_two_norm_worker( int m, int n, fftw_complex const * imga, fftw_complex const * imgb, int thread_id, int n_threads, double * retval )
{
   *retval = 0.0;
   for ( int i = thread_id; i < m; i += n_threads )
      for ( int j = 0; j < n; ++j )
      {
         double del = imga[ j + i * n ][ 0 ] - imgb[ j + i * n ][ 0 ];
         *retval += del * del;
         del = imga[ j + i * n ][ 1 ] - imgb[ j + i * n ][ 1 ];
         *retval += del * del;
      }
}

double diff_two_norm( int m, int n, fftw_complex const * imga, fftw_complex const * imgb, int n_threads )
{
   double retval = 0.0;
   std::vector< double > retvals( n_threads );

   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( diff_two_norm_worker, m, n, imga, imgb, i, n_threads, &(retvals[ i ]) ) );

   for ( int i = 0; i < n_threads; ++i )
   {
      v_threads[ i ].join();
      retval += retvals[ i ];
   }

   return std::sqrt( retval );
}

void  inner_prod_worker( int m, int n, fftw_complex const * imga, fftw_complex const * imgb, int thread_id, int n_threads, std::complex< double > * retval )
{
   auto ptr_a = reinterpret_cast< std::complex< double > const * >( imga );
   auto ptr_b = reinterpret_cast< std::complex< double > const * >( imgb );

   *retval = 0.0;

   for ( int i = thread_id; i < m; i += n_threads )
      for ( int j = 0; j < n; ++j )
      {
         *retval += std::conj( ptr_a[ j + i * n ] ) * ptr_b[ j + i * n ];
      }
}

std::complex< double > inner_prod( int m, int n, fftw_complex const * imga, fftw_complex const * imgb, int n_threads )
{
   std::complex< double > retval = 0.0;
   std::vector< std::complex< double > > retvals( n_threads );

   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( inner_prod_worker, m, n, imga, imgb, i, n_threads, &(retvals[ i ]) ) );

   for ( int i = 0; i < n_threads; ++i )
   {
      v_threads[ i ].join();
      retval += retvals[ i ];
   }

   return retval;
}

void sub_mult_complex_worker( int m, int n, fftw_complex * imga, fftw_complex const * imgb, std::complex< double > step, int thread_id, int n_threads )
{
   auto ptr_a = reinterpret_cast< std::complex< double > * >( imga );
   auto ptr_b = reinterpret_cast< std::complex< double > const * >( imgb );

   for ( int i = thread_id; i < m; i += n_threads )
      for ( int j = 0; j < n; ++j )
         ptr_a[ j + i * n ] = step * ptr_a[ j + i * n ] - ptr_b[ j + i * n ];
}

void sub_mult_complex( int m, int n, fftw_complex * imga, fftw_complex const * imgb, std::complex< double > step, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( sub_mult_complex_worker, m, n, imga, imgb, step, i, n_threads ) );

   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

#endif //#ifndef UTILS_H
