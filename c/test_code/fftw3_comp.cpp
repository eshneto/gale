//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#include <fftw3.h>
#include <cstdlib>
#include <chrono>
#include <iostream>
#include <thread>
#include <complex>
#include <vector>

int m = 512;
int n = 2048 * 4;

int nthreads_many = 4;
int nthreads_single = 8;

void fft_execute_worker( int thread_id, int nthreads, int how_many, int size, fftw_plan plan, fftw_complex * in, fftw_complex * out  )
{
   for ( int i = thread_id; i < how_many; i += nthreads )
      fftw_execute_dft( plan, in + i * size, out + i * size );
}

void fft_execute( int nthreads, int how_many, int size, fftw_plan plan, fftw_complex * in, fftw_complex * out  )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( nthreads );
   for ( int i = 0; i < nthreads; ++i )
      v_threads.push_back( std::thread( fft_execute_worker, i, nthreads, how_many, size, plan, in, out ) );
   for ( int i = 0; i < nthreads; ++i )
      v_threads[ i ].join();
}

int main()
{
   fftw_complex * data = fftw_alloc_complex( m * n );
   for ( int i = 0; i < m * n; ++i )
   {
      data[ i ][ 0 ] = std::rand() / double( RAND_MAX );
      data[ i ][ 1 ] = std::rand() / double( RAND_MAX );
   }

   fftw_complex * out_1 = fftw_alloc_complex( m * n );

   fftw_init_threads();
   fftw_plan_with_nthreads( nthreads_many );

   std::chrono::steady_clock::time_point begin;
   std::chrono::steady_clock::time_point end;

   fftw_plan plan_1 = fftw_plan_many_dft(
     1,     // rank
     &n,    // dimensions
     m,     // how many
     data,  // input
     0,     // inembed
     1,     // istride
     n,     // idist
     out_1,   // output
     0,     // onembed
     1,     // ostride
     n,     // odist
     FFTW_FORWARD,
     FFTW_ESTIMATE
   );

   begin = std::chrono::steady_clock::now();
   fftw_execute( plan_1 );
   end = std::chrono::steady_clock::now();
   std::cout << "Elapsed: " << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";

   fftw_complex * out_2 = fftw_alloc_complex( m * n );

   fftw_plan_with_nthreads( 1 );
   fftw_plan plan_2 = fftw_plan_dft_1d(
     n,     // dimension
     data,  // input
     out_2, // output
     FFTW_FORWARD,
     FFTW_ESTIMATE
   );

   begin = std::chrono::steady_clock::now();
   for ( int i = 0; i < m; ++i )
      fftw_execute_dft( plan_2, data + i * n, out_2 + i * n );
//       fft_execute( nthreads_single, m, n, plan_2, data, out_2 );
   end = std::chrono::steady_clock::now();
   std::cout << "Elapsed: " << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";

   std::complex< double > * aux_out_1 = reinterpret_cast< std::complex< double > * >( out_1 );
   std::complex< double > * aux_out_2 = reinterpret_cast< std::complex< double > * >( out_2 );
   double mx = 0.0;
   for ( int i = 0; i < m * n; ++i )
      if ( mx < std::abs( aux_out_1[ i ] - aux_out_2[ i ] ) )
         mx = std::abs( aux_out_1[ i ] - aux_out_2[ i ] );
   std::cout << mx << '\n';


   return 0;
}
