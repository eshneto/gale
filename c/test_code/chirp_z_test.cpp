//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#include <fftw3.h>
#include <iomanip>
#include <iostream>
#include <complex>
template< int width = 26 >
void display( int m, int n, fftw_complex const * matrix )
{
   std::complex< double > const * aux_ptr = reinterpret_cast< std::complex< double > const * >( matrix );
   for ( int i = 0; i < m; ++i )
   {
      for ( int j = 0; j < n; ++j )
         std::cout << std::fixed
                   << std::setprecision( 4 )
                   << std::showpoint
                   << std::setw( width ) << aux_ptr[ j + i * n ];

      std::cout << '\n';
   }
}

template< int width = 26 >
void display_transpose( int m, int n, fftw_complex const * matrix )
{
   std::complex< double > const * aux_ptr = reinterpret_cast< std::complex< double > const * >( matrix );
   for ( int j = 0; j < n; ++j )
   {
      for ( int i = 0; i < m; ++i )
         std::cout << std::fixed
                   << std::setprecision( 4 )
                   << std::showpoint
                   << std::setw( width ) << aux_ptr[ j + i * n ];

      std::cout << '\n';
   }
}

// #define PM_PROFILING_ON

#include "chirp_z.h"

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <chrono>

#define NTHREADS 4

#define LARGE_TEST

#ifndef LARGE_TEST
#define TEST_SIZE 16
#else
#define TEST_SIZE_N 4096
#define TEST_SIZE_M 3072
#define TEST_SIZE_S 50
// #define TEST_SIZE_N 100
// #define TEST_SIZE_M 120
// #define TEST_SIZE_S 10
#endif

#include <cstdlib>
#include <ctime>

int main()
{
   std::srand( std::time( 0 ) );

   fftw_complex * img = fftw_alloc_complex( 2 * TEST_SIZE_N * ( TEST_SIZE_M + TEST_SIZE_S ) );
   fftw_complex * img_copy = fftw_alloc_complex( 2 * TEST_SIZE_N * ( TEST_SIZE_M + TEST_SIZE_S ) );
   fftw_complex * adj_img = fftw_alloc_complex( 2 * TEST_SIZE_N * ( TEST_SIZE_M + TEST_SIZE_S ) );
   fftw_complex * buffer = fftw_alloc_complex( 2 * TEST_SIZE_N * ( TEST_SIZE_M + TEST_SIZE_S ) );
   fftw_complex * out_array = fftw_alloc_complex( 2 * TEST_SIZE_N * ( TEST_SIZE_M + TEST_SIZE_S ) );

   fftw_init_threads();
   fftw_plan_with_nthreads( NTHREADS );
   fftw_import_wisdom_from_filename( "chirp_z_wisdom.txt" );

   std::cerr << "Initializing transform...\n";
   std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
   //++++++++++++++++++++++++++++++++++++++++++
   chirp_z cz_obj( TEST_SIZE_M, TEST_SIZE_S, TEST_SIZE_N, TEST_SIZE_M / 2, TEST_SIZE_N / 2, img, adj_img, buffer, out_array, NTHREADS, FFTW_MEASURE, 0.0 );
   //++++++++++++++++++++++++++++++++++++++++++
   std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
   std::cout << "Elapsed: " << std::chrono::duration_cast< std::chrono::microseconds >( end - begin ).count() / 1e6 << "s\n";
   std::cerr << "Done!\n";

   std::cerr << "Initializing test images...\n";
   //++++++++++++++++++++++++++++++++++++++++++
   for( unsigned i = 0; i < TEST_SIZE_M; ++i )
      for( unsigned j = 0; j < TEST_SIZE_N; ++j )
      {
         img_copy[ j + i * TEST_SIZE_N ][ 0 ] = img[ j + i * TEST_SIZE_N ][ 0 ] = double( rand() ) / RAND_MAX;
         img_copy[ j + i * TEST_SIZE_N ][ 1 ] = img[ j + i * TEST_SIZE_N ][ 1 ] = double( rand() ) / RAND_MAX;
      }
   for( unsigned i = TEST_SIZE_M; i < 2 * ( TEST_SIZE_M + TEST_SIZE_S ); ++i )
      for( unsigned j = 0; j < TEST_SIZE_N; ++j )
      {
         img_copy[ j + i * TEST_SIZE_N ][ 0 ] = img[ j + i * TEST_SIZE_N ][ 0 ] = 0.0;
         img_copy[ j + i * TEST_SIZE_N ][ 1 ] = img[ j + i * TEST_SIZE_N ][ 1 ] = 0.0;
      }

   for( unsigned i = 0; i < TEST_SIZE_N; ++i )
      for( unsigned j = 0; j < ( TEST_SIZE_M + TEST_SIZE_S ); ++j )
      {
         adj_img[ j + i * 2 * ( TEST_SIZE_M + TEST_SIZE_S ) ][ 0 ] = double( rand() ) / RAND_MAX;
         adj_img[ j + i * 2 * ( TEST_SIZE_M + TEST_SIZE_S ) ][ 1 ] = double( rand() ) / RAND_MAX;
      }
   for( unsigned i = 0; i < TEST_SIZE_N; ++i )
      for( unsigned j = ( TEST_SIZE_M + TEST_SIZE_S ); j < 2 * ( TEST_SIZE_M + TEST_SIZE_S ); ++j )
      {
         adj_img[ j + i * 2 * ( TEST_SIZE_M + TEST_SIZE_S ) ][ 0 ] = 0.0;
         adj_img[ j + i * 2 * ( TEST_SIZE_M + TEST_SIZE_S ) ][ 1 ] = 0.0;
      }
   //++++++++++++++++++++++++++++++++++++++++++
   std::cerr << "Done!\n";

   cz_obj.direct();

   std::complex< double > sum = 0.0;
   std::complex< double > const * out = reinterpret_cast< std::complex< double > const * >( cz_obj.out_ );
   std::complex< double > const * adj_in = reinterpret_cast< std::complex< double > const * >( adj_img );
   for( unsigned i = 0; i < TEST_SIZE_N; ++i )
      for( unsigned j = 0; j < ( TEST_SIZE_M + TEST_SIZE_S ); ++j )
         sum += std::conj( out[ j + i * 2 * ( TEST_SIZE_M + TEST_SIZE_S ) ] ) * adj_in[ j + i * 2 * ( TEST_SIZE_M + TEST_SIZE_S ) ];

   std::cout << sum << '\n';

   cz_obj.adjoint();

   std::complex< double > sum_transp = 0.0;
   std::complex< double > const * in = reinterpret_cast< std::complex< double > const * >( img_copy );
   for( unsigned i = 0; i < TEST_SIZE_N; ++i )
      for( unsigned j = 0; j < TEST_SIZE_M; ++j )
         sum_transp += out[ j + i * 2 * ( TEST_SIZE_M + TEST_SIZE_S ) ] * std::conj( in[ i + j * TEST_SIZE_N ] );

   std::cout << sum_transp << '\n';
   std::cout << sum_transp / sum << '\n';

   fftw_export_wisdom_to_filename( "chirp_z_wisdom.txt" );

   fftw_free( out_array );
   fftw_free( buffer );
   fftw_free( adj_img );
   fftw_free( img_copy );
   fftw_free( img );

   return 0;
}
