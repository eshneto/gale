//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#include "dft.h"
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iomanip>

template< int width = 13 >
void display_diff( int m, int n, double const * matrix_a, double const * matrix_b )
{
   for ( int i = 0; i < m; ++i )
   {
      for ( int j = 0; j < n; ++j )
         std::cout << std::fixed
         << std::setprecision( 4 )
         << std::showpoint
         << std::setw( width ) << std::abs( matrix_a[ j + i * n ] - matrix_b[ j + i * n ] );

      std::cout << '\n';
   }
}

// #define DISPLAY

int main()
{
   for ( int k = 1; k <= 100; ++k )
   {
      int M = k;
      int N = k + ( k % 2 );

      double * xi_a = new double[ M * N ];
      double * upsilon_a = new double[ M * N ];
      double * xi_b = new double[ M * N ];
      double * upsilon_b = new double[ M * N ];

      dft::pp_grid( M, N, xi_a, upsilon_a );
      std::vector< double > angles = dft::equal_slope_angles( N );
      dft::any_angle_pp_grid( M, angles, xi_b, upsilon_b );

      double max_err = 0.0;
      for ( int i = 0; i < M; ++i )
      {
         for ( int j = 0; j < N; ++j )
         {
            double curr_err = std::abs( xi_a[ j + i * N ] - xi_b[ j + i * N ] );
            if ( curr_err > max_err )
               max_err = curr_err;
            curr_err = std::abs( upsilon_a[ j + i * N ] - upsilon_b[ j + i * N ] );
            if ( curr_err > max_err )
               max_err = curr_err;
         }
      }

//       std::cout << angles[ N - 1 ] * 180.0 / dft::PI << '\n';
//       std::cout << M << '\t' << N << '\n';
      std::cout << max_err << '\n';
//       std::cout << '\n';

#ifdef DISPLAY
      display_diff( M, N, xi_a, xi_b );
      std::cout << '\n';
      display_diff( M, N, upsilon_a, upsilon_b );
      std::cout << '\n';
#endif

      delete[] xi_a;
      delete[] upsilon_a;
      delete[] xi_b;
      delete[] upsilon_b;
   }

   return 0;
}
