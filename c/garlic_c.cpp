//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#include "garlic.cpp" // Source file for c++ garlic routines.
#include "garlic_c.h" // Header file for routines defined here.

garlic_plan C_garlic_create( int m, int n, int M, int N_g, int N, int S )
{
   return garlic_create( m, n, M, N_g, N, S );
}

garlic_plan C_garlic_create_general( int m, int n, int M, int N_g, int N, int S, double * angles )
{
   return garlic_create_general( m, n, M, N_g, N, S, angles );
}

void C_garlic_execute_cplx( garlic_plan plan, complex const * in, complex * out )
{
   garlic_execute( plan,
                   reinterpret_cast< std::complex< double > const * >( in ),
                   reinterpret_cast< std::complex< double > * >( out )
                 );
}

void C_garlic_execute( garlic_plan plan, double const * in, complex * out )
{
   garlic_execute( plan,
                   in,
                   reinterpret_cast< std::complex< double > * >( out )
                 );
}

void C_garlic_execute_dft_cplx( garlic_plan plan, complex const * in, complex * out )
{
   garlic_execute_dft( plan,
                       reinterpret_cast< std::complex< double > const * >( in ),
                       reinterpret_cast< std::complex< double > * >( out )
                     );
}

void C_garlic_execute_dft( garlic_plan plan, double const * in, complex * out )
{
   garlic_execute_dft( plan,
                       in,
                       reinterpret_cast< std::complex< double > * >( out )
                     );
}

void C_garlic_execute_adjoint( garlic_plan plan, complex const * in, complex * out )
{
   garlic_execute_adjoint( plan,
                           reinterpret_cast< std::complex< double > const * >( in ),
                           reinterpret_cast< std::complex< double > * >( out )
                         );
}

void C_garlic_destroy( garlic_plan plan )
{
   garlic_destroy( plan );
}
