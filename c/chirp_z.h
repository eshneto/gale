//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#ifndef CHIRP_Z_H
#define CHIRP_Z_H

#include <fftw3.h>

#include <thread>
#include <complex>
#include <cstring>
#include <vector>

#include "pm_prof.h"

/**
 * This class provides routines for computation of the chirp-z transform of several sequences with some specific "chirp factors". The value of these factors are useful for the pseudo-polar fast Fourier transform.
 */
struct chirp_z {
   static const double PI; ///< \f$\pi\f$.

   // Parallelism:
   int n_threads_; ///< Number of threads for computation.

   // Transform dimensions:
   int m_; ///< Transforms lengths.
   int s_; ///< Extra elements on each transform.
   int n_; ///< Number of transforms.
   int m_shift_; ///< Shift in ordering of output sequence.
   int n_shift_; ///< Shift in sequences' elements.

   // Array that will contain intermediate result:
   fftw_complex * buffer_; ///< Stores DFT of the input sequence times scaling factors.

   // Array that will contain the result:
   fftw_complex * out_; ///< Stores output of the transform

   // Array that will contain the factors:
   fftw_complex * factors_; ///< Scaling factors for multiplication of input sequence.
   fftw_complex * external_factors_; ///< Scaling factors for multiplication of output sequence.
   // Transpose of the array that will contain the factors:
   fftw_complex * transpose_factors_; ///< Transpose of the input factors for transposed sequence.
   // Array that will contain the DFT of the factors:
   fftw_complex * dft_factors_; ///< DFT of the convolved sequence.

   // Pointer to input data (managed outside chirp Z object):
   fftw_complex * in_; ///< Pointer to input data.
   // Pointer to adjoint's input data (managed outside chirp Z object):
   fftw_complex * adjoint_in_; ///< Point to adjoint of input data.

   // Plan for intermediate transform:
   fftw_plan interm_plan_; ///< Forward FFT of input columns (output lines).
   // Plan for adjoint's intermediate transform:
   fftw_plan adjoint_interm_plan_; ///< Forward FFT of input lines (output lines).
   // Plan for final transform:
   fftw_plan direct_plan_; ///< Backward FFT of input lines (output lines), can be used to adjoint as well.

   // Componentwise multiply a matrix by some coefficients:
   static void multiply( int m, int n, fftw_complex * in, fftw_complex const * coefs, int in_stride, int n_threads );
   static void multiply_worker( int m, int n, fftw_complex * in, fftw_complex const * coefs, int in_stride, int thread_id, int n_threads );
   static void conjugate_multiply( int m, int n, fftw_complex * in, fftw_complex const * coefs, int in_stride, int n_threads );
   static void conjugate_multiply_worker( int m, int n, fftw_complex * in, fftw_complex const * coefs, int in_stride, int thread_id, int n_threads );
   // Normalize FFTW output;
   static void normalize( int m, int n, fftw_complex * out, int n_threads );
   static void normalize_worker( int m, int n, fftw_complex * out, int thread_id, int n_threads );

   // Initializes transform:
    chirp_z( int m, int s, int n, int n_shift, int m_shift, fftw_complex * in, fftw_complex * adjoint_in, fftw_complex * buffer, fftw_complex * out, int n_threads, unsigned flags, double sigma = 0.0 );
   // Clean up:
   ~chirp_z();

   // Compute direct chirp_z transform:
   void direct();
   // Compute adjoint chirp_z transform:
   void adjoint();
};

const double chirp_z::PI = 3.1415926535897932384;

/**
 * \brief Componentwise multiply two arrays
 *
 * \param m Number of lines of each array
 * \param n Number of columns of each array
 * \param in Array to me multiplied
 * \param coefs Array with multiplication coefficients
 * \param in_stride Phisical length of in array's lines
 * \param n_threads Number of computational threads of execution
 *
 * This function multiplies each component of array in by the coefficients of array coefs using \p n_threads parallel threads of computation. This is done by calling \p n_threads instances of multiply_worker().
 *
 */
void chirp_z::multiply( int m, int n, fftw_complex * in, fftw_complex const * coefs, int in_stride, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( chirp_z::multiply_worker, m, n, in, coefs, in_stride, i, n_threads ) );
   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

/**
 * \brief Componentwise multiply select lines of two arrays
 *
 * \param m Number of lines of each array
 * \param n Number of columns of each array
 * \param in Array to me multiplied
 * \param coefs Array with multiplication coefficients
 * \param in_stride Phisical length of in array's lines
 * \param thread_id Number of current thread of execution (should be a number in 0, 1, ..., \p n_threads - 1)
 * \param n_threads Number of computational threads of execution
 *
 * This function multiplies certain lines of array \p in by the coefficients of array \p coefs. Which lines are multiplied depend on \p thread_id and \p n_threads. All components should be multiplied only once if this function is run with every \p thread_id ranging from 0, 1, ..., \p n_threads - 1.
 */
void chirp_z::multiply_worker( int m, int n, fftw_complex * in, fftw_complex const * coefs, int in_stride, int thread_id, int n_threads )
{
   auto aux_ptr_coefs = reinterpret_cast< std::complex< double > const * >( coefs );
   auto aux_ptr_in = reinterpret_cast< std::complex< double >* >( in );
   for ( int i = thread_id; i < m; i += n_threads )
      for ( int j = 0; j < n; ++j )
         aux_ptr_in[ j + i * in_stride ] *= aux_ptr_coefs[ j + i * n ];
}

/**
 * \brief Componentwise multiply one array by the conjugate of the other.
 *
 * \param m Number of lines of each array
 * \param n Number of columns of each array
 * \param in Array to me multiplied
 * \param coefs Array with multiplication coefficients
 * \param in_stride Phisical length of in array's lines
 * \param n_threads Number of computational threads of execution
 *
 * This function multiplies each component of array \p in by the conjugate coefficients of array \p coefs using \p n_threads parallel threads of computation. This is done by calling \p n_threads instances of conjugate_multiply_worker().
 *
 */
void chirp_z::conjugate_multiply( int m, int n, fftw_complex * in, fftw_complex const * coefs, int in_stride, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( chirp_z::conjugate_multiply_worker, m, n, in, coefs, in_stride, i, n_threads ) );
   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

/**
 * \brief Componentwise multiply select lines of one array by the conjugate of the other.
 *
 * \param m Number of lines of each array
 * \param n Number of columns of each array
 * \param in Array to me multiplied
 * \param coefs Array with multiplication coefficients
 * \param in_stride Phisical length of in array's lines
 * \param thread_id Number of current thread of execution (should be a number in 0, 1, ..., \p n_threads - 1)
 * \param n_threads Number of computational threads of execution
 *
 * This function multiplies certain lines of array \p in by the conjugate coefficients of array \p coefs. Which lines are multiplied depend on \p thread_id and \p n_threads. All components should be multiplied only once if this function is run with every \p thread_id ranging from 0, 1, ..., \p n_threads - 1.
 */
void chirp_z::conjugate_multiply_worker( int m, int n, fftw_complex * in, fftw_complex const * coefs, int in_stride, int thread_id, int n_threads )
{
   auto aux_ptr_coefs = reinterpret_cast< std::complex< double > const * >( coefs );
   auto aux_ptr_in = reinterpret_cast< std::complex< double >* >( in );
   for ( int i = thread_id; i < m; i += n_threads )
      for ( int j = 0; j < n; ++j )
         aux_ptr_in[ j + i * in_stride ] *= std::conj( aux_ptr_coefs[ j + i * n ] );
}

/**
 *
 * \brief Constructor
 *
 * \param m The length of each of the transforms.
 * \param s The number of extra elements on each transform.
 * \param n The number of transforms.
 * \param n_shift The shift performed in the order of sequences (not on each sequence), of which depends the sequence of chirp factors.
 * \param m_shift The shift performed in each sequence.
 * \param in An array of size 2(m+s)n (2(m+s) lines and n columns) which must contain the zero-padded input sequences when the transform is be called. Each column will contain a sequence. This array will have its contents destroyed by this function and it must have been properly allocated before calling the constructor!
 * \param adjoint_in An array of size 2(m+s)n (2(m+s) columns and n lines) which must contain the zero-padded input sequences when the transform adjoint is be called. Each line will be a sequence. This array will have its contents destroyed by this function and it must have been properly allocated before calling the constructor!
 * \param buffer An array of size 2(m+s)n elements to be used for intermediate computations storage.
 * \param out An array of size 2(m+s)n elements to be used for computations output.
 * \param n_threads Number of concurrent threads to be used in the execution of the transform and its adjoint. The constructor itself uses only one thread, but the direct and adjoint transforms will use as many threads as indicated by this parameter. Exception is made for the routines form the FFTW3 library, in order to set the number of working threads for theses routines, you should call the appropriate FFTW3 function prior to calling this constructor: first fftw_init_threads(), then fftw_plan_with_nthreads()
 * \param flags Flags for the FFTW3 planner functions
 * \param sigma Shift in the fractional factor
 *
 * This is the constructor of the chirp_z transform. It initializes the object by computing all of the factors that will be used in the transform calculation that do not depend on the input data.
 *
 */
chirp_z::chirp_z( int m, int s, int n, int n_shift, int m_shift, fftw_complex * in, fftw_complex * adjoint_in, fftw_complex * buffer, fftw_complex * out, int n_threads, unsigned flags, double sigma )
   : n_threads_( n_threads ),
     m_( m ), s_( s ), n_( n ),
     m_shift_( m_shift ), n_shift_( n_shift ),
     buffer_( buffer ),
     out_( out ),
     factors_( fftw_alloc_complex( m * n ) ),
     external_factors_( fftw_alloc_complex( ( m + s ) * n ) ),
     transpose_factors_( fftw_alloc_complex( m * n ) ),
     dft_factors_( fftw_alloc_complex( 2 * ( m + s ) * n ) ),
     in_( in ),
     adjoint_in_( adjoint_in )
{
   // FFT lengths are twice the sequence length
   // because of circular effects:
   const int length = 2 * ( m + s );

   // Plan for n forward FFTs of length 2*(m+s) with
   // column input and row output:
   interm_plan_ = fftw_plan_many_dft( 1, // Rank
                                      &length, // Dimensions
                                      n, // How many
                                      in, // Input data
                                      0, // Inembed
                                      n, // Istride
                                      1, // Idist
                                      buffer_, // Output data
                                      0, // Onembed
                                      1, // Ostride
                                      2 * ( m + s ), // Odist
                                      FFTW_FORWARD,
                                      flags
                                    );
   // Plan for n backwards FFTs of length 2*(m+s) with
   // row input and row output:
   direct_plan_ = fftw_plan_many_dft( 1, // Rank
                                      &length, // Dimensions
                                      n, // How many
                                      buffer_, // Input data
                                      0, // Inembed
                                      1, // Istride
                                      2 * ( m + s ), // Idist
                                      out_, // Output data
                                      0, // Onembed
                                      1, // Ostride
                                      2 * ( m + s ), // Odist
                                      FFTW_BACKWARD,
                                      flags
                                    );
   // Plan for n forward FFTs of length 2*(m+s) with
   // row input and row output:
   adjoint_interm_plan_ = fftw_plan_many_dft( 1, // Rank
                                              &length, // Dimensions
                                              n, // How many
                                              adjoint_in, // Input data
                                              0, // Inembed
                                              1, // Istride
                                              2 * ( m + s ), // Idist
                                              buffer_, // Output data
                                              0, // Onembed
                                              1, // Ostride
                                              2 * ( m + s ), // Odist
                                              FFTW_FORWARD,
                                              flags
                                            );

   // At this point m = N / 2 and M = n:
   double M = n;
   double N = 2.0 * m;
   // Total number of elements:
   int mps = m + s;

   // Compute factors to be DFTed:
   // exp( \sqrt{-1} * alpha_I * PI * j^2 / N ) with I = -M/2, -M/2 + 1, ..., M/2 - 1 and
   // alpha_I = 4 * I / M - 2 * sigma / pi
   std::complex< double > * aux_ptr = reinterpret_cast< std::complex< double >* >( adjoint_in_ );
   // Each line performs the chirp_z with a different fractional factor:
   for ( int i = 0; i < n; ++i )
   {
      // We are taking the desired ordering into consideration here,
      // which is -M/2, -M/2 + 1, ..., M/2 - 1:
      double alpha_I = 4.0 * ( i - n_shift ) / M - 2.0 * sigma / PI;
      std::complex< double > W( 0.0, PI * alpha_I / N );
      // Compute each element of the sequence to be DFT'ed for the
      // corresponding input line (the r_j in the text):
      for ( int j = 0; j < mps; ++j )
      {
         // First half of sequence:
         double j_sq = j; j_sq *= j_sq;
         aux_ptr[ j + i * length ] = std::exp( W * j_sq );
         // Second half of sequence:
         j_sq = mps - j; j_sq *= j_sq;
         aux_ptr[ j + mps + i * length ] = std::exp( W * j_sq );
      }
   }

   // Compute factors' DFTs:
   fftw_execute( adjoint_interm_plan_ );

   // Copy factors' DFTs:
   aux_ptr = reinterpret_cast< std::complex< double >* >( buffer_ );
   std::complex< double > * dft_factors_ptr = reinterpret_cast< std::complex< double >* >( dft_factors_ );
   for ( int i = 0; i < n; ++i )
      for ( int j = 0; j < length; ++j )
         dft_factors_ptr[ j + i * length ] = aux_ptr[ j + i * length ];

   // Compute multiplying factors:
   // exp( -\sqrt{-1} * alpha_I * PI * j^2 / N ) with I = -M/2, -M/2 + 1, ..., M/2 - 1 and
   // alpha_I = 4 * I / M - 2 * sigma / pi
   aux_ptr = reinterpret_cast< std::complex< double > * >( factors_ );
   std::complex< double > * aux_ptr_ext = reinterpret_cast< std::complex< double > * >( external_factors_ );
   std::complex< double > * aux_ptr_transp = reinterpret_cast< std::complex< double > * >( transpose_factors_ );
   // Each line performs the chirp_z with a different fractional factor:
   for ( int i = 0; i < n; ++i )
   {
      // We are taking the desired ordering into consideration here,
      // which is -n_shift, -n_shift + 1, ..., M + n_shift - 1:
      double alpha_I = 4.0 * ( i - n_shift ) / M - 2.0 * sigma / PI;
      std::complex< double > W( 0.0, -PI * alpha_I / N );
      // Compute each element of the factor sequence:
      for ( int j = 0; j < mps; ++j )
      {
         double j_sq = j; j_sq *= j_sq;
         aux_ptr_ext[ j + i * mps ] = std::exp( W * j_sq );
         if ( j < m )
         {
            // Multiply by the in-sequence shifting factors:
            // exp( -\sqrt{-1} * 2 * PI * j * m_shift * alpha_I / N )
            aux_ptr_transp[ i + j * n ] = std::exp( W * ( -2.0 * m_shift * j + j_sq ) );
            aux_ptr[ j + i * m ] = aux_ptr_transp[ i + j * n ];
         }
      }
   }
}

/**
 * \brief Normalize lines
 *
 * \param m Number of lines of the array
 * \param n Number of columns of the array
 * \param out Array to me normalized
 * \param n_threads Number of computational threads of execution
 *
 * Since FFTW3 backward FFT routines do not normalize the result, this function performs this operation. This is done by calling \p n_threads instances of normalize_worker().
 *
 */
void chirp_z::normalize( int m, int n, fftw_complex * out, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( chirp_z::normalize_worker, m, n, out, i, n_threads ) );
   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

/**
 * \brief Normalize select lines
 *
 * \param m Number of lines of the array
 * \param n Number of columns of the array
 * \param out Array to me normalized
 * \param thread_id Number of current thread of execution (should be a number in 0, 1, ..., \p n_threads - 1)
 * \param n_threads Number of computational threads of execution - 1
 *
 * Since FFTW3 backward FFT routines do not normalize the result, this function performs this operation in select lines of the array. Normalization here is division by 2m of each element. All components should be multiplied only once if this function is run with every \p thread_id ranging from 0, 1, ..., \p n_threads - 1.
 *
 */
void chirp_z::normalize_worker( int m, int n, fftw_complex * out, int thread_id, int n_threads )
{
   int length = 2 * m;
   double ilength_d = 1.0 / length;
   std::complex< double > * aux_ptr = reinterpret_cast< std::complex< double >* >( out );
   for ( int i = thread_id; i < n; i += n_threads )
      for ( int j = 0; j < m; ++j )
         aux_ptr[ j + i * length ] *= ilength_d;
}

/**
 * \brief Destructor
 *
 * Releases the memory allocated for chirp_z::dft_factors_, chirp_z::transpose_factors_, chirp_z::factors_, chirp_z::out_, and chirp_z::buffer_. Memory pointed to by chirp_z::in_ and chirp_z::adjoint_in_ must be allocated and deallocated by the user. This function also releases resources allocated for chirp_z::adjoint_interm_plan_, chirp_z::direct_plan_, chirp_z::interm_plan_.
 *
 */
chirp_z::~chirp_z()
{
   // Clean up memory resources:
   fftw_free( dft_factors_ );
   fftw_free( transpose_factors_ );
   fftw_free( external_factors_ );
   fftw_free( factors_ );

   // Clean up FFTW plans:
   fftw_destroy_plan( adjoint_interm_plan_ );
   fftw_destroy_plan( direct_plan_ );
   fftw_destroy_plan( interm_plan_ );
}

/**
 * \brief Compute the chirp-z transforms.
 *
 * This function computes the following mathematical operation:
 * \f[
 *     X_{I, J} = \sum_{i = 0}^{M - 1} x_{i, J}e^{-\imath i\frac{2\pi I}{M}\alpha_J},
 * \f]
 * where \f$\imath = \sqrt{-1}\f$ and \f$\alpha_J = \frac{4J}{N} - \frac{2\sigma}{\pi}\f$.
 *
 * Each column of the array chirp_z::in_ is considered an input sequence and determines \f$J\f$ following the sequence \f$\{ -S_N, -S_N + 1, \dots, N - S_N - 1 \}\f$, where \f$N = {}\f$ chirp_z::n_ is the number of chirp-z transforms to compute and \f$S_N = {}\f$ chirp_z::n_shift_. These are determined by the parameters \p n and \p n_shift of chirp_z::chirp_z(). The length of each input sequence is assumed to be \f$M = {}\f$ chirp_z::m_, wich is determined by the parameter \p m of chirp_z::chirp_z(). Notice that chirp_z::in_ must be zero padded to an array of size \f$2( S + M )\f$ lines and \f$N\f$ columns, where \f$S = {}\f$ chirp_z::s_ which is determined by the parameter \p s of chirp_z::chirp_z().
 *
 * After the function returns, the result will be stored on chirp_z::out_, where it is organized by lines. That is, each sequence \f$X_{I, J}\f$ for a fixed \f$J\f$ will be stored in a different line of the ouptut. \f$I\f$ follows the sequence \f$\{ -S_M, -S_M + 1, \dots, M + S - S_M - 1\}\f$, where \f$S_M = {}\f$ chirp_z::m_shift_, which is determined by parameter \p m_shift of chirp_z::chirp_z(). Notice that although the sequence is of length \f$M + S\f$, physically, the array chirp_z::out_ is organized in \f$2(M + S)\f$ columns per line when returning from this function.
 */
void chirp_z::direct()
{
   pm_prof prof( "chirp_z::direct()" );

   // Multiply input by factors:
   prof.start( "Chirp Z First multiplication" );
   chirp_z::multiply( m_, n_, in_, transpose_factors_, n_, n_threads_ );
   prof.stop();

   // Compute FFT of multiplied input:
   prof.start( "Second FFT" );
   fftw_execute( interm_plan_ );
   prof.stop();

   // Multiply intermediate result by DFT factors:
   prof.start( "Chirp Z second multiplication" );
   chirp_z::multiply( n_, 2 * ( m_ + s_ ), buffer_, dft_factors_, 2 * ( m_ + s_ ), n_threads_ );
   prof.stop();

   // Compute inverse DFT:
   prof.start( "Third FFT" );
   fftw_execute( direct_plan_ );
   prof.stop();

   // Multiply by factors:
   prof.start( "Chirp final factors" );
   chirp_z::multiply( n_, m_ + s_, out_, external_factors_, 2 * ( m_ + s_ ), n_threads_ );
   prof.stop();

   // Normalize:
   prof.start( "Normalize" );
   chirp_z::normalize( m_ + s_, n_, out_, n_threads_ );
   prof.stop();
}

/**
 * \brief Compute the adjointt chirp-z transform
 *
 * This function computes the adjoint operation of chirp_z::direct(). The input must be stored by lines on chirp_z::adjoint_in_, which is assume to be zero-padded to a size of chirp_z::n_ lines and 2 * ( chirp_z::m_ + chirp_z::s_ ) columns. There is a caveat: the result will be stored by lines instead of by columns, which is a different organization than the organization of the input of chirp_z::direct().
 *
 * After the function returns, the result will be stored on chirp_z::out_. Notice that although only chirp_z::m_ columns and chirp_z::n_ lines of the output are meaningful, the array chirp_z::out_ will be physically organized as having 2 * ( chirp_z::m_ + chirp_z::s_ ) columns per line after returning from this function.
 *
 * For an explanation of the members mentioned here, see the documentation of chirp_z::direct().
 */
void chirp_z::adjoint()
{
   pm_prof prof( "chirp_z::adjoint()" );

   // Normalize:
   prof.start( "Normalize" );
   chirp_z::normalize( m_ + s_, n_, adjoint_in_, n_threads_ );
   prof.stop();

   // Multiply input by factors:
   prof.start( "First multiplication" );
   chirp_z::conjugate_multiply( n_, m_ + s_, adjoint_in_, external_factors_, 2 * ( m_ + s_ ), n_threads_ );
   prof.stop();

   // Compute FFT of multiplied input:
   prof.start( "First FFT" );
   fftw_execute( adjoint_interm_plan_ );
   prof.stop();

   // Multiply intermediate result by DFT factors:
   prof.start( "Second multiplication" );
   chirp_z::conjugate_multiply( n_, 2 * ( m_ + s_ ), buffer_, dft_factors_, 2 * ( m_ + s_ ), n_threads_ );
   prof.stop();

   // Compute inverse DFT:
   prof.start( "Second FFT" );
   fftw_execute( direct_plan_ );
   prof.stop();

   // Multiply by factors:
   prof.start( "Chirp final factors" );
   chirp_z::conjugate_multiply( n_, m_, out_, factors_, 2 * ( m_ + s_ ), n_threads_ );
   prof.stop();
}

#endif // #ifndef CHIRP_Z_H
