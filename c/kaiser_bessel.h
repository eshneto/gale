//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#ifndef KAISER_BESSEL_H
#define KAISER_BESSEL_H

#include <boost/math/special_functions.hpp>
#include <cmath>

/**
 * \file kaiser_bessel.h
 *
 * This file brings functions related to the Kaiser Bessel window function.
 */

/**
 * \brief Namespace for Kaiser-Bessel window functions.
 */
namespace kb {

   /**
    * \brief Kaiser-Bessel object.
    *
    * Makes the computation of the Kaiser-Bessel window function more efficient by storing some constants.
    */
   template < class d_type = double >
   struct kb {
      static d_type const zero_; ///< Constant of the correct type equal to zero.
      static d_type const one_; ///< Constant of the correct type equal to one.
      static d_type const two_; ///< Constant of the correct type equal to two.
      d_type alpha_; ///< \f$\alpha\f$.
      d_type I_0_alpha_; ///< \f$I_0( \alpha )\f$.
      d_type tau_; ///< \f$\tau\f$.
      d_type factor_; ///< \f$2\tau/I_0(\alpha)\f$.
      d_type tau_alpha_; ///< \f$\tau / \alpha\f$.

      /**
       * \brief Constructor.
       *
       * \param alpha Main lobe halfwidth parameter. In the frequency domain, it determines the trade-off between main-lobe width and side lobe level.
       * \param tau Support parameter.
       *
       * Initializes constants.
       */
      kb( d_type alpha, d_type tau )
         : alpha_( alpha ),
           I_0_alpha_( boost::math::cyl_bessel_i( kb< d_type >::zero_, alpha ) ),
           tau_( tau ),
           factor_( kb< d_type >::two_ * tau / I_0_alpha_ ),
           tau_alpha_( tau / alpha )
      {}

      /**
       * \brief Kaiser-Bessel window function.
       *
       * \param t Argument.
       *
       * The Kaiser-Bessel window function \f$K_{\alpha, \tau}\f$ has a good compromise between small support and fast Fourier decay, and is given by
       * \f[
       *    K_{\alpha, \tau}( t ) := \begin{cases}
       *                                \displaystyle\frac{I_0( \alpha\sqrt{1 - (t / \tau)^2} )}{I_0( \alpha )} & | t | \leq \tau\\
       *                                0                                                          & | t | > \tau,
       *                             \end{cases}
       * \f]
       * where \f$I_0\f$ is the zero-order modified cylindrical Bessel function, which can be evaluated calling boost::math::cyl_bessel_i(), \f$\alpha = {}\f$ kb<>::alpha_, \f$\tau = {}\f$ kb<>::tau_, and \f$t = {}\f$ \p t.
       */
      d_type operator()( d_type t )
      {
         if ( std::abs( t ) > std::abs( tau_ ) )
            return  kb< d_type >::zero_;
         else
         {
            d_type tmp = t / tau_;
            tmp *= tmp;
            return boost::math::cyl_bessel_i( kb< d_type >::zero_, alpha_ * std::sqrt( kb< d_type >::one_ - tmp ) ) / I_0_alpha_;
         }
      }

      /**
       * \brief Fourier Transform of Kaiser-Bessel window function.
       *
       * \param omega Argument.
       *
       * Fourier Transform \f$\hat K_{\alpha, \tau}\f$ of the Kaiser-Bessel window function \f$K_{\alpha, \tau}\f$ (see kb<>::operator()()), given by:
       * \f[
       *    \hat K_{\alpha, \tau}( \omega ) = \frac{2\tau}{I_0( \alpha )}\frac{\sinh(\alpha\sqrt{ 1 - (\omega\tau / \alpha )^2 })}{\alpha\sqrt{ 1 - (\omega\tau / \alpha )^2 }}
       * \f]
       * where \f$I_0\f$ is the zero-order modified cylindrical Bessel function, which can be evaluated calling boost::math::cyl_bessel_i(), \f$\alpha = {}\f$ kb<>:alpha_, \f$\tau = {}\f$ kb<>::tau, and \f$\omega = {}\f$ \p omega.
       */
      d_type fourier( d_type omega )
      {
         d_type tmp = omega * tau_alpha_;
         tmp *= tmp;
         d_type arg = kb< d_type >::one_ - tmp;

         if ( arg < kb< d_type >::zero_ )
         {
            arg = alpha_ * std::sqrt( -arg );
            return factor_ * ( std::sin( arg ) / arg );
         }
         else if ( arg > kb< d_type >::zero_ )
         {
            arg = alpha_ * std::sqrt( arg );
            return factor_ * ( std::sinh( arg ) / arg );
         }
         else
            return factor_;
      }
   };

   template < class d_type >
   d_type const kb< d_type >::zero_ =  0.0;
   template < class d_type >
   d_type const kb< d_type >::one_ =  1.0;
   template < class d_type >
   d_type const kb< d_type >::two_ =  2.0;


   /**
    * \brief Kaiser-Bessel window function.
    *
    * \tparam d_type Floating point type.
    * \param alpha Main lobe halfwidth parameter. In the frequency domain, it determines the trade-off between main-lobe width and side lobe level.
    * \param tau Support parameter.
    * \param t Argument.
    *
    * The Kaiser-Bessel window function \f$K_{\alpha, \tau}\f$ has a good compromise between small support and fast Fourier decay, and is given by
    * \f[
    *    K_{\alpha, \tau}( t ) := \begin{cases}
    *                                \displaystyle\frac{I_0( \alpha\sqrt{1 - (t / \tau)^2} )}{I_0( \alpha )} & | t | \leq | \tau |\\
    *                                0                                                          & | t | > | \tau |,
    *                             \end{cases}
    * \f]
    * where \f$I_0\f$ is the zero-order modified cylindrical Bessel function, which can be evaluated calling boost::math::cyl_bessel_i(), \f$\alpha = {}\f$ \p alpha, \f$\tau = {}\f$ \p tau, and \f$t = {}\f$ \p t.
    */
   template < class d_type = double >
   d_type kaiser_bessel( d_type alpha, d_type tau, d_type t )
   {
      if ( std::abs( t ) > std::abs( tau ) )
         return  kb< d_type >::zero_;
      else
      {
         d_type tmp = t / tau;
         tmp *= tmp;
         return boost::math::cyl_bessel_i( kb< d_type >::zero_, alpha * std::sqrt( kb< d_type >::one_ - tmp ) ) / boost::math::cyl_bessel_i( kb< d_type >::zero_, alpha );
      }
   }

   /**
    * \brief Fourier Transform of Kaiser-Bessel window function.
    *
    * \tparam d_type Floating point type.
    * \param alpha Main lobe halfwidth parameter. In the frequency domain, it determines the trade-off between main-lobe width and side lobe level.
    * \param tau Support parameter.
    * \param omega Argument.
    *
    * Fourier Transform \f$\hat K_{\alpha, \tau}\f$ of the Kaiser-Bessel window function \f$K_{\alpha, \tau}\f$ (see kb::kaiser_bessel()), given by:
    * \f[
    *    \hat K_{\alpha, \tau}( t ) = \frac{2\tau}{I_0( \alpha )}\frac{\sinh(\alpha\sqrt{ 1 - (\omega\tau / \alpha )^2 })}{\alpha\sqrt{ 1 - (\omega\tau / \alpha )^2 }}
    * \f]
    * where \f$I_0\f$ is the zero-order modified cylindrical Bessel function, which can be evaluated calling boost::math::cyl_bessel_i(), \f$\alpha = {}\f$ \p alpha, \f$\tau = {}\f$ \p tau, and \f$\omega = {}\f$ \p omega.
    */
   template < class d_type = double >
   d_type fourier_kaiser_bessel( d_type alpha, d_type tau, d_type omega )
   {
      d_type const factor = ( kb< d_type >::two_ * tau ) / boost::math::cyl_bessel_i( kb< d_type >::zero_, alpha );

      d_type tmp = ( omega * tau ) / alpha;
      tmp *= tmp;
      d_type arg = kb< d_type >::one_ - tmp;

      if ( arg < 0 )
      {
         arg = alpha * std::sqrt( -arg );
         return factor * ( std::sin( arg ) / arg );
      }
      else if ( arg > 0 )
      {
         arg = alpha * std::sqrt( arg );
         return factor * ( std::sinh( arg ) / arg );
      }
      else
         return factor;
   }

} // namespace kb

#endif // #ifndef KAISER_BESSEL_H
