//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#ifndef GPPFFT_H
#define GPPFFT_H

#include "ppfft.h"
#include "kaiser_bessel.h"
#include "dft.h"
#include <fftw3.h>
#include <vector>
#include <thread>

//TODO: The number of extra rays in the PPFFT can be computed based on which are the transform angles. In some cases no extra angle will be necessary.

/**
 * \brief Generalized linogram Fast Fourier Transform
 *
 * \tparam weight_t Kind of weighting funcion.
 *
 * This class template contains functions for the computation of samples of the DFT lying on equally spaced concentric squares but not necessarily along the equally slope-spaced rays.
 */
#ifndef GPPFFT_NO_DEFAULT
template < class weight_t = kb::kb< double > >
#else
template < class weight_t >
#endif
struct gppfft {

   static double alpha( int HK, double tau )
   {
      return HK * tau;
   }

   static double epsilon_;

   double OS_; ///< Oversampling factor.
   ppfft ppfft_object_; ///< This object will perform the heavy-lifiting of computing the PPFFT.
   int HK_; ///< Half of the number of parcels in the approximation.
   int N_; ///< Number of angles in the GPPFFT.
   fftw_complex * ppfft_out_; ///< Output array for ppfft.
   std::vector< std::complex< double > > linear_coefficients_; ///< Coefficients for linear combinations to obtain approximations.
   std::vector< std::vector< int > > combination_indices_; ///< Indices of values to be used in linear combinations.

   /**
    * \brief Compute next even number.
    *
    * \param n Integer number.
    *
    * \return Smallest even integer equal to or larger than \p n.
    */
   static int make_even( int n )
   {
      return n + ( n % 2 );
   }

   /**
    * \brief Compute oversampling factor.
    *
    * \param m Number of lines in input image (\f$m\f$).
    * \param n Number of columns in input image (\f$n\f$).
    * \param HK Number of lateral parcels in truncated approximation  (\f$K\f$). Total number of parcels will be \f$2K + 1\f$.
    * \param up_sample How many times to increase power of two in number of samples (\f$u\f$).
    *
    * This function computes the oversampling factor
    * \f[
    *    O = \frac{2^{\lceil \log_2( \max\{ m, n \} + 2( K + 1 ) ) + u \rceil} - 2( K + 1 )}{\max\{ m, n \}}.
    * \f]
    *
    * This value is such that, for \f$u \geq 0\f$, thet value \f$O\max\{m, n\} + 2(K + 1)\f$ is always a power of two and is never smaller than \f$\max\{m, n\} + 2(K + 1)\f$. The power of two is larger as \f$u\f$ is increased.
    *
    * \return \f$O\f$.
    */
   static double OS_compute( int m, int n, int HK, int up_sample = 0 )
   {
      return ( std::pow( 2, std::ceil( std::log2( std::max( m, n ) + 2.0 * ( HK + 1 ) ) + up_sample ) ) + 0.1 - 2.0 * ( HK + 1 ) ) / std::max( m, n );
   }

   // Computation of the weights.
   static void create_weight_factors( int M, int N, int M_shift,
                                      fftw_complex * out, fftw_complex * transpose_out,
                                      int HK, int n, double sigma
                                    );

   // Computation of linear coefficients.
   static void create_linear_combinations( int M, int N, int S, int HK, int m, int n,
                                           double const * angles, int n_angles,
                                           int BH_N_shift, int BV_N_shift,
                                           int BH_M_shift, int BV_M_shift,
                                           std::vector< std::complex< double > > & linear_coefficients,
                                           std::vector< std::vector< int > > & combination_indices,
                                           double sigma_h, double sigma_v
                                         );

   // Constructor.
   gppfft( int m, int n,
           double * angles, int n_angles, int M = 0,
           int HK = 6, double OS = 0.0,
           int N = 0, int S = 0,
           int fftw_n_threads = 0, unsigned fftw_flags = FFTW_MEASURE, int n_threads = 0,
           double sigma_h = 0.0 / 0.0, double sigma_v = 0.0 / 0.0
         );

   ~gppfft();

   // Direct transform.
   template< class data_t >
   void direct( data_t const * in, fftw_complex * out );
   template< class data_t >
   void direct( data_t const * in, std::complex< double > * out )
   {
      direct( in, reinterpret_cast< fftw_complex * >( out ) );
   }
   void direct( std::complex< double > const * in, std::complex< double > * out )
   {
      direct( reinterpret_cast< fftw_complex const * >( in ), out );
   }
   // Direct transform.
   void adjoint( fftw_complex const * in, fftw_complex * out );
   template < class data_t_in, class data_t_out >
   void adjoint( data_t_in const * in, data_t_out * out )
   {
      adjoint( reinterpret_cast< fftw_complex const * >( in ),
               reinterpret_cast< fftw_complex * >( out )
             );
   }

   // Compute linear combination.
   static void combine( fftw_complex * out, fftw_complex const * data, int M,
                        int data_N, int data_S,
                        std::vector< std::complex< double > > const & linear_coefficients,
                        std::vector< std::vector< int > > const & combination_indices,
                        int n_threads
                      );
   static void combine_worker( fftw_complex * out, fftw_complex const * data, int M,
                               int data_N, int data_S,
                               std::vector< std::complex< double > > const & linear_coefficients,
                               std::vector< std::vector< int > > const & combination_indices,
                               int thread_id, int n_threads
                             );
   static void spread( fftw_complex const * in, fftw_complex * data, int M,
                       int data_N, int data_S,
                       std::vector< std::complex< double > > const & linear_coefficients,
                       std::vector< std::vector< int > > const & combination_indices,
                       int n_threads
                     );
   static void spread_worker( fftw_complex const * in, fftw_complex * data, int M,
                              int data_N, int data_S,
                              std::vector< std::complex< double > > const & linear_coefficients,
                              std::vector< std::vector< int > > const & combination_indices,
                              int thread_id, int n_threads
                            );

};

template < class weight_t >
double gppfft< weight_t >::epsilon_ = 1.0 - 1e-4;

/**
 * \brief Constructor.
 *
 * \tparam weight_t Kind of weighting funcion.
 *
 * \param m Number \f$m\f$ of lines in the image.
 * \param n Number \f$n\f$ of columns in the image.
 * \param HK Half of the number of parcels in the approximation.
 * \param angles Angles where to compute the DFT.
 * \param n_angles Number of angles where to compute the DFT.
 * \param M Number \f$M\f$ of samples on each angle. If a non-positive number is given, this parameter's value will be selected according to the image dimensions. It must be larger than or equal the maximum between \p m and \p n.
 * \param N Number of angles \f$N\f$ in the intermediate PPFFT. If a non-positive number is given, this parameter's value will be selected according to the image dimensions. It must be even and it must be larger than or equal 2 times the maximum between \p m and \p n.
 * \param S Number of extra angles \f$S\f$ in the intermediate PPFFT. If a non-positive number is given, this parameter's value will be selected according to the number of parcels in the exponential approximation. It must be divisible by 4.
 * \param fftw_n_threads Number of FFTW execution threads.
 * \param fftw_flags Flags for FFTW3's planning routines.
 * \param n_threads Number of execution threads.
 */
template < class weight_t >
gppfft< weight_t >::gppfft( int m, int n,
                            double * angles, int n_angles, int M,
                            int HK, double OS,
                            int N, int S,
                            int fftw_n_threads, unsigned fftw_flags, int n_threads,
                            double sigma_h, double sigma_v
                          )
   : OS_( OS <= 0.0 ? OS_compute( m, n, HK, -OS ) : OS ),
     ppfft_object_( m, n,
                    M > 0 ? M : std::max( m, n ),
                    N > 0 ? N : make_even( 2 * OS_ * std::max( m, n ) ),
                    S > 0 ? S : ( HK + 1 ) * 4,
                    fftw_n_threads, fftw_flags, n_threads,
                    std::isnan( sigma_h ) ? dft::PI / ( M > 0 ? M : std::max( m, n ) ) : sigma_h,
                    std::isnan( sigma_v ) ? dft::PI / ( M > 0 ? M : std::max( m, n ) ) : sigma_v
                  ),
     HK_( HK ),
     N_( n_angles ),
     ppfft_out_( fftw_alloc_complex( ppfft_object_.M_ * ( ppfft_object_.N_ + ppfft_object_.S_ ) ) ),
     linear_coefficients_( n_angles * ( 2 * HK + 1 ) * ppfft_object_.M_ )
{
   sigma_h = std::isnan( sigma_h ) ? dft::PI / ( M > 0 ? M : std::max( m, n ) ) : sigma_h;
   sigma_v = std::isnan( sigma_v ) ? dft::PI / ( M > 0 ? M : std::max( m, n ) ) : sigma_v;
   create_weight_factors( ppfft_object_.M_, ppfft_object_.N_, ppfft_object_.M_ / 2,
                          ppfft_object_.chirp_z_bh_obj_.factors_,
                          ppfft_object_.chirp_z_bh_obj_.transpose_factors_,
                          HK, m, -sigma_h
                        );
   create_weight_factors( ppfft_object_.M_, ppfft_object_.N_,
                          ( ppfft_object_.M_ % 2 ) ? ppfft_object_.M_ / 2 : ppfft_object_.M_ / 2 - 1,
                          ppfft_object_.chirp_z_bv_obj_.factors_,
                          ppfft_object_.chirp_z_bv_obj_.transpose_factors_,
                          HK, n, sigma_v
                        );

   create_linear_combinations( ppfft_object_.M_, ppfft_object_.N_, ppfft_object_.S_, HK, m, n,
                               angles, n_angles,
                               ppfft_object_.N_ % 4 ? ppfft_object_.N_ / 4 : ppfft_object_.N_ / 4 - 1,
                               ppfft_object_.N_ / 4,
                               ppfft_object_.M_ / 2,
                               ( ppfft_object_.M_ % 2 ) ? ppfft_object_.M_ / 2 : ppfft_object_.M_ / 2 - 1,
                               linear_coefficients_, combination_indices_,
                               sigma_h, sigma_v
                             );
}

/**
 * \brief Destructor.
 *
 * \tparam weight_t Kind of weighting funcion.
 *
 */
template < class weight_t >
gppfft< weight_t >::~gppfft()
{
   fftw_free( ppfft_out_ );
}

/**
 * \brief Create weight factors.
 *
 * \tparam weight_t Kind of weighting funcion.
 *
 * \param M Number \f$M\f$ of samples on each pseudo-polar ray.
 * \param N Number \f$M\f$ of pseudo-polar rays.
 * \param M_shift FFT M-shifting factor \f$S_M\f$.
 * \param out Array where to store weighting factors.
 * \param transpose_out Array where to store weighting factors in transpose order.
 * \param HK Linear combination truncation parameter.
 * \param n Corresponding image dimension \f$n\f$ (number of columns for BV and number of lines for BH).
 *
 * This function computes the weighting factors which will multiply the input to chirp_z::direct() so that the sequences. The weights are:
 *
 * \f[
 *     \frac1{W( t^I_j - \varpi^I )},
 * \f]
 * where \f$t^I_j = \frac{8\pi Ij}{MN}\f$ and \f$\varpi^I = \frac{4\pi I( n - 1)}{MN}\f$. The values of \f$I\f$ are \f$-S_M, -S_M + 1, \dots, -S_M + M / 2 - 1\f$.
 *
 * The factors are stored in transpose order in \p transpose_out for better efficiency when multiplying the output of chirp_z::adjoint() when computing the adjoint transform.
 */
template < class weight_t >
void gppfft< weight_t >::create_weight_factors( int M, int N, int M_shift,
                                                fftw_complex * out, fftw_complex * transpose_out,
                                                int HK, int n, double sigma
                                              )
{
   int hN = N / 2;

   auto aux_ptr = reinterpret_cast< std::complex< double >* >( out );
   auto aux_ptr_transpose = reinterpret_cast< std::complex< double >* >( transpose_out );
   // Compute weighting factors $1 / W( t^I_j - \varpi )$ where $t^I_j = \frac{8\pi Ij}{MN}$.
   for ( int i = 0; i < M; ++i )
   {
      double tI = 4.0 / N * ( 2 * dft::PI * ( i - M_shift ) / M - sigma );
      double varpi_I = 2.0 * ( n - 1 ) / N * ( 2.0 * dft::PI * ( i - M_shift ) / M - sigma );
      double tau_I = dft::PI + epsilon_ * ( dft::PI - std::abs( varpi_I ) );
      weight_t W_object_I( alpha( HK, tau_I ), tau_I );
#ifdef GPPFFT_DONT_IGNORE_DC
      if ( tI )
#endif
         for ( int j = 0; j < n; ++j )
         {
            double W = W_object_I( tI * j - varpi_I );
            // Factors positioned for the direct transform:
            aux_ptr[ j + i * hN ] /= W;
            // Factors positioned for the adjoint transform:
            aux_ptr_transpose[ i + j * M ] /= W;
         }
   }
}

/**
 * \brief Create linear combination parameters.
 *
 * \tparam weight_t Kind of weighting funcion.
 *
 * \param M Number \f$M\f$ of samples on each ray.
 * \param N Number \f$M\f$ of rays.
 * \param HK Number \f$H\f$ of indices past thhe closest sample in truncated summation.
 * \param m Number \f$m\f$ of lines in image.
 * \param n Number \f$n\f$ of columns in image.
 * \param angles Angles where the desired approximations will be computed.
 * \param n_angles Number of angles.
 * \param BH_N_shift BH shift in rays positioning.
 * \param BV_N_shift BV shift in rays positioning.
 * \param BH_M_shift BH shift in samples positioning on each ray.
 * \param BV_M_shift BV shift in samples positioning on each ray.
 * \param linear_coefficients Vector where to store the coefficients.
 * \param combination_indices Indices of the Fourier samples that will be combined.
 *
 * This function computes the weights and indices of the values that will go into each approximation of the Fourier samples. See documentation of function gppfft::direc() for more details.
 */
template < class weight_t >
void gppfft< weight_t >::create_linear_combinations( int M, int N, int S, int HK, int m, int n,
                                                     double const * angles, int n_angles,
                                                     int BH_N_shift, int BV_N_shift,
                                                     int BH_M_shift, int BV_M_shift,
                                                     std::vector< std::complex< double > > & linear_coefficients,
                                                     std::vector< std::vector< int > > & combination_indices,
                                                     double sigma_h, double sigma_v
                                                   )
{
   combination_indices.reserve( n_angles );
   int coef_skip = 2 * HK + 1;
   int N_coefs = coef_skip * n_angles;

#ifdef GPFFT_ALWAYS_ODD_SUMMANDS
   for ( unsigned i = 0; i < linear_coefficients.size(); ++i )
      linear_coefficients[ i ] = 0.0;

   for ( int j = 0; j < n_angles; ++j )
   {
      double c = std::cos( angles[ j ] );
      double s = std::sin( angles[ j ] );

      // BV angles:
      if ( ( std::abs( s ) > std::abs( c ) ) && ( angles[ j ] <= dft::TQPI ) )
      {
         double eta = ( c / s ) * N / 4.0;
         int del = ( S / 4 ) + ( N / 2 ) - 1 - BV_N_shift;

         int J_eta = std::round( eta );

         std::vector< int > curr_indices;
         curr_indices.reserve( 2 * HK + 1 );

         for ( int i = 0; i < M; ++i )
         {
            double varpi = 2.0 * ( n - 1 ) / N * ( 2.0 * dft::PI * ( M - 1 - BV_M_shift - i ) / M - sigma_v );
            double tau_I = dft::PI + epsilon_ * ( dft::PI - std::abs( varpi ) );
            weight_t W_object_I( alpha( HK, tau_I ), tau_I );
            int I = M - 1 - i - BV_M_shift;

            std::complex< double > exp_factor;

            for ( int k = HK; k > 0; --k )
            {
               exp_factor = std::exp( std::complex< double >( 0.0, -( eta - ( J_eta + k ) ) * varpi ) );
               if ( I )
                  linear_coefficients[ coef_skip * j + i * N_coefs + HK - k ] = exp_factor * W_object_I.fourier( eta - ( J_eta + k ) ) / dft::DPI;
               if ( !i )
                  curr_indices.push_back( del - ( J_eta + k ) );
            }

            exp_factor = std::exp( std::complex< double >( 0.0, -( eta - J_eta ) * varpi ) );
            linear_coefficients[ coef_skip * j + i * N_coefs + HK ] = I ? exp_factor * W_object_I.fourier( eta - J_eta  ) / dft::DPI : 1.0;
            if ( !i )
               curr_indices.push_back( del - J_eta );

            for ( int k = 1; k <= HK; ++k )
            {
               exp_factor = std::exp( std::complex< double >( 0.0, -( eta - ( J_eta - k ) ) * varpi ) );
               if ( I )
                  linear_coefficients[ coef_skip * j + i * N_coefs + HK + k ] = exp_factor * W_object_I.fourier( eta - ( J_eta - k ) ) / dft::DPI;
               if ( !i )
                  curr_indices.push_back( del - ( J_eta - k ) );
            }
         }
         combination_indices.push_back( curr_indices );
      }
      // BH angles:
      else
      {
         double eta = ( s / c ) * N / 4.0;
         int del = ( S + N ) / 2 + ( S / 4 ) + BH_N_shift;

         int J_eta = std::round( eta );

         std::vector< int > curr_indices;
         curr_indices.reserve( 2 * HK + 1 );

         for ( int i = 0; i < M; ++i )
         {
            double varpi = 2.0 * ( m - 1 ) / N * ( 2.0 * dft::PI * ( i - BH_M_shift ) / M + sigma_h );
            double tau_I = dft::PI + epsilon_ * ( dft::PI - std::abs( varpi ) );
            weight_t W_object_I( alpha( HK, tau_I ), tau_I );
            int I = i - BH_M_shift;

            std::complex< double > exp_factor;

            for ( int k = HK; k > 0; --k )
            {
               exp_factor = std::exp( std::complex< double >( 0.0, -( eta - ( J_eta - k ) ) * varpi ) );
               if ( I )
                  linear_coefficients[ coef_skip * j + i * N_coefs + HK - k ] = exp_factor * W_object_I.fourier( eta - ( J_eta - k ) ) / dft::DPI;
               if ( !i )
                  curr_indices.push_back( del + ( J_eta - k ) );
            }

            exp_factor = std::exp( std::complex< double >( 0.0, -( eta - J_eta ) * varpi ) );
            linear_coefficients[ coef_skip * j + i * N_coefs + HK ] = I ? exp_factor * W_object_I.fourier( eta - J_eta  ) / dft::DPI : 1.0;
            if ( !i )
               curr_indices.push_back( del + J_eta );

            for ( int k = 1; k <= HK; ++k )
            {
               exp_factor = std::exp( std::complex< double >( 0.0, -( eta - ( J_eta + k ) ) * varpi ) );
               if ( I )
                  linear_coefficients[ coef_skip * j + i * N_coefs + HK + k ] = exp_factor * W_object_I.fourier( eta - ( J_eta + k ) ) / dft::DPI;
               if ( !i )
                  curr_indices.push_back( del + ( J_eta + k ) );
            }
         }
         combination_indices.push_back( curr_indices );
      }
   }
#else
   for ( unsigned i = 0; i < linear_coefficients.size(); ++i )
      linear_coefficients[ i ] = 0.0;

   for ( int j = 0; j < n_angles; ++j )
   {
      double c = std::cos( angles[ j ] );
      double s = std::sin( angles[ j ] );

      // BV angles:
      if ( ( std::abs( s ) > std::abs( c ) ) && ( angles[ j ] <= dft::TQPI ) )
      {
         double eta = ( c / s ) * N / 4.0;
         int del = ( S / 4 ) + ( N / 2 ) - 1 - BV_N_shift;

         int J_eta = std::ceil( eta );

         std::vector< int > curr_indices;
         curr_indices.reserve( 2 * HK + 1 );

         for ( int i = 0; i < M; ++i )
         {
            double varpi = 2.0 * ( n - 1 ) / N * ( 2.0 * dft::PI * ( M - 1 - BV_M_shift - i ) / M - sigma_v );
            double tau_I = dft::PI + epsilon_ * ( dft::PI - std::abs( varpi ) );
            weight_t W_object_I( alpha( HK, tau_I ), tau_I );
#ifdef GPPFFT_DONT_IGNORE_DC
            int I = M - 1 - i - BV_M_shift;
#endif

            std::complex< double > exp_factor;

            int is_diff = ( J_eta != eta );
            if ( is_diff && !i )
            {
               curr_indices.push_back( del - ( J_eta + HK ) );
            }

            for ( int k = HK - is_diff; k > 0; --k )
            {
               exp_factor = std::exp( std::complex< double >( 0.0, -( eta - ( J_eta + k ) ) * varpi ) );
#ifdef GPPFFT_DONT_IGNORE_DC
               if ( I )
#endif
                  linear_coefficients[ coef_skip * j + i * N_coefs + HK - k ] = exp_factor * W_object_I.fourier( eta - ( J_eta + k ) ) / dft::DPI;
               if ( !i )
                  curr_indices.push_back( del - ( J_eta + k ) );
            }

            exp_factor = std::exp( std::complex< double >( 0.0, -( eta - J_eta ) * varpi ) );
#ifdef GPPFFT_DONT_IGNORE_DC
            linear_coefficients[ coef_skip * j + i * N_coefs + HK ] = I ? exp_factor * W_object_I.fourier( eta - J_eta  ) / dft::DPI : 1.0;
#else
            linear_coefficients[ coef_skip * j + i * N_coefs + HK ] = exp_factor * W_object_I.fourier( eta - J_eta  ) / dft::DPI;
#endif
            if ( !i )
               curr_indices.push_back( del - J_eta );

            for ( int k = 1; k <= HK; ++k )
            {
               exp_factor = std::exp( std::complex< double >( 0.0, -( eta - ( J_eta - k ) ) * varpi ) );
#ifdef GPPFFT_DONT_IGNORE_DC
               if ( I )
#endif
                  linear_coefficients[ coef_skip * j + i * N_coefs + HK + k ] = exp_factor * W_object_I.fourier( eta - ( J_eta - k ) ) / dft::DPI;
               if ( !i )
                  curr_indices.push_back( del - ( J_eta - k ) );
            }
         }
         combination_indices.push_back( curr_indices );
      }
      // BH angles:
      else
      {
         double eta = ( s / c ) * N / 4.0;
         int del = ( S + N ) / 2 + ( S / 4 ) + BH_N_shift;

         int J_eta = std::floor( eta );

         std::vector< int > curr_indices;
         curr_indices.reserve( 2 * HK + 1 );

         for ( int i = 0; i < M; ++i )
         {
            double varpi = 2.0 * ( m - 1 ) / N * ( 2.0 * dft::PI * ( i - BH_M_shift ) / M + sigma_h );
            double tau_I = dft::PI + epsilon_ * ( dft::PI - std::abs( varpi ) );
            weight_t W_object_I( alpha( HK, tau_I ), tau_I );
#ifdef GPPFFT_DONT_IGNORE_DC
            int I = i - BH_M_shift;
#endif

            std::complex< double > exp_factor;

            int is_diff = ( J_eta != eta );
            if ( is_diff && !i )
            {
               curr_indices.push_back( del + J_eta - HK );
            }

            for ( int k = HK - is_diff; k > 0; --k )
            {
               exp_factor = std::exp( std::complex< double >( 0.0, -( eta - ( J_eta - k ) ) * varpi ) );
#ifdef GPPFFT_DONT_IGNORE_DC
               if ( I )
#endif
                  linear_coefficients[ coef_skip * j + i * N_coefs + HK - k ] = exp_factor * W_object_I.fourier( eta - ( J_eta - k ) ) / dft::DPI;
               if ( !i )
                  curr_indices.push_back( del + ( J_eta - k ) );
            }

            exp_factor = std::exp( std::complex< double >( 0.0, -( eta - J_eta ) * varpi ) );
#ifdef GPPFFT_DONT_IGNORE_DC
            linear_coefficients[ coef_skip * j + i * N_coefs + HK ] = I ? exp_factor * W_object_I.fourier( eta - J_eta  ) / dft::DPI : 1.0;
#else
            linear_coefficients[ coef_skip * j + i * N_coefs + HK ] = exp_factor * W_object_I.fourier( eta - J_eta  ) / dft::DPI;
#endif
            if ( !i )
               curr_indices.push_back( del + J_eta );

            for ( int k = 1; k <= HK; ++k )
            {
               exp_factor = std::exp( std::complex< double >( 0.0, -( eta - ( J_eta + k ) ) * varpi ) );
#ifdef GPPFFT_DONT_IGNORE_DC
               if ( I )
#endif
                  linear_coefficients[ coef_skip * j + i * N_coefs + HK + k ] = exp_factor * W_object_I.fourier( eta - ( J_eta + k ) ) / dft::DPI;
               if ( !i )
                  curr_indices.push_back( del + ( J_eta + k ) );
            }
         }
         combination_indices.push_back( curr_indices );
      }
   }
#endif
}

/**
 * \brief Discrete Fourier Transform approximation over the generalized linogram space.
 *
 * \param in Input image. Should be of size gppfft::ppfft_object_.m_ lines and gppfft::ppfft_object_.n_ columns. Can be of type double or fftw_complex.
 * \param out Output result. Should be of size gppfft::ppfft_object_.M_ lines and gppfft::combination_indices_.size() columns.
 *
 * Let us define the Discrete Fourier Transform (DFT) of a discrete image \f$\boldsymbol x \in \mathbb R^{m \times n}\f$ as:
 * \f[
 *    \mathcal D[\boldsymbol x]( \xi, \upsilon ) := \sum_{i = 0}^{m - 1}\sum_{j = 0}^{n - 1}x_{i, j}e^{-\imath( j\xi + i\upsilon )}.
 * \f]
 *
 * Then, this function computes approximations for
 * \f[
 *    \mathcal D[ \boldsymbol x ]\left( \frac{2\pi I}M\cot \theta, \frac{2\pi I}M \right)
 * \f]
 * with \f$I \in \{-M/2 + 1, -M/2 + 2, \dots, M/2\}\f$ when \f$|\cos( \theta )| \leq |\sin\theta|\f$ and for
 * \f[
 *    \mathcal D[ \boldsymbol x ]\left( \frac{2\pi I}M, \frac{2\pi I}M\tan \theta \right)
 * \f]
 * with \f$I \in \{-M/2, -M/2 + 1, \dots, M/2 - 1\}\f$ when \f$|\cos( \theta )| > |\sin\theta|\f$.
 *
 * The results for each angle \f$\theta\f$ are stored in a column of the array \p out. The angles, and dimensions are determined by the parameters passed to the Constructor ppfft::ppfft().
 */
template < class weight_t >
template < class data_t >
void gppfft< weight_t >::direct( data_t const * in, fftw_complex * out )
{
   ppfft_object_.direct( in, ppfft_out_ );
   combine( out, ppfft_out_, ppfft_object_.M_,
            ppfft_object_.N_, ppfft_object_.S_,
            linear_coefficients_, combination_indices_,
            ppfft_object_.n_threads_
          );
}

template < class weight_t >
void gppfft< weight_t >::adjoint( fftw_complex const * in, fftw_complex * out )
{
   ppfft::line_set_zero( ppfft_object_.N_ + ppfft_object_.S_, 0, ppfft_object_.M_, ppfft_out_, ppfft_object_.n_threads_ );
   spread( in, ppfft_out_, ppfft_object_.M_,
           ppfft_object_.N_, ppfft_object_.S_,
           linear_coefficients_, combination_indices_,
           ppfft_object_.n_threads_
         );
   ppfft_object_.adjoint( ppfft_out_, out );
}

template < class weight_t >
void gppfft< weight_t >::combine( fftw_complex * out, fftw_complex const * data, int M,
                                  int data_N, int data_S,
                                  std::vector< std::complex< double > > const & linear_coefficients,
                                  std::vector< std::vector< int > > const & combination_indices,
                                  int n_threads
                                )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( gppfft< weight_t >::combine_worker,
                                        out, data, M, data_N, data_S,
                                        std::ref( linear_coefficients ), std::ref( combination_indices ),
                                        i, n_threads
                                      )
                         );
   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

template < class weight_t >
void gppfft< weight_t >::combine_worker( fftw_complex * out, fftw_complex const * data, int M,
                                         int data_N, int data_S,
                                         std::vector< std::complex< double > > const & linear_coefficients,
                                         std::vector< std::vector< int > > const & combination_indices,
                                         int thread_id, int n_threads
                                       )
{
   int N = combination_indices.size();
   int coef_skip = combination_indices[ 0 ].size();
   int N_coefs = coef_skip * N;
   int NpS = data_N + data_S;

   std::complex< double > * out_ptr = reinterpret_cast< std::complex< double > * >( out );
   std::complex< double > const * data_ptr = reinterpret_cast< std::complex< double > const * >( data );

   for ( int i = thread_id; i < M ; i += n_threads )
   {
      int iNpS = NpS * i;
      for ( int j = 0; j < N; ++j )
      {
         std::complex< double > sum = 0.0;

         std::complex< double > const * coefs = &( linear_coefficients[ j * coef_skip + i * N_coefs ] );
         int const * indices = &( combination_indices[ j ][ 0 ] );
         for ( int k = 0; k < coef_skip; ++k )
            sum += coefs[ k ] * data_ptr[ indices[ k ] + iNpS ];

         out_ptr[ j + i * N ] = sum;
      }
   }
}

template < class weight_t >
void gppfft< weight_t >::spread( fftw_complex const * in, fftw_complex * data, int M,
                                 int data_N, int data_S,
                                 std::vector< std::complex< double > > const & linear_coefficients,
                                 std::vector< std::vector< int > > const & combination_indices,
                                 int n_threads
                               )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( gppfft< weight_t >::spread_worker,
                                        in, data, M, data_N, data_S,
                                        std::ref( linear_coefficients ), std::ref( combination_indices ),
                                        i, n_threads
                                      )
                         );
   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

template < class weight_t >
void gppfft< weight_t >::spread_worker( fftw_complex const * in, fftw_complex * data, int M,
                                        int data_N, int data_S,
                                        std::vector< std::complex< double > > const & linear_coefficients,
                                        std::vector< std::vector< int > > const & combination_indices,
                                        int thread_id, int n_threads
                                      )
{
   int N = combination_indices.size();
   int coef_skip = combination_indices[ 0 ].size();
   int N_coefs = coef_skip * N;
   int NpS = data_N + data_S;

   std::complex< double > const * in_ptr = reinterpret_cast< std::complex< double > const * >( in );
   std::complex< double > * data_ptr = reinterpret_cast< std::complex< double > * >( data );

   for ( int i = thread_id; i < M ; i += n_threads )
   {
      int iNpS = NpS * i;
      for ( int j = 0; j < N; ++j )
      {
         std::complex< double > val = in_ptr[ j + i * N ];

         std::complex< double > const * coefs = &( linear_coefficients[ j * coef_skip + i * N_coefs ] );
         int const * indices = &( combination_indices[ j ][ 0 ] );
         for ( int k = 0; k < coef_skip; ++k )
            data_ptr[ indices[ k ] + iNpS ] += std::conj( coefs[ k ] ) * val;
      }
   }
}

#endif // #ifndef GPPFFT_H
