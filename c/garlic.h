//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#ifndef GARLIC_H
#define GARLIC_H

#include <complex>
#include <vector>
#include "kaiser_bessel.h"

/**
 * \file garlic.h
 *
 * This file provides simplified functions to work with GARLiC routines.
 */

template < class weight_t = kb::kb< double > >
struct gppfft;
typedef gppfft<> * garlic_plan;

/**
   * \brief Create a GARLiC plan.
   *
   * \param m Number of lines in input image.
   * \param n Number of columns in input image.
   * \param M Number of samples on each ray. Must be larger than or equal \f$\max\{ m, n \}\f$.
   * \param N_g Number of rays in output DFT.
   * \param N Number of angles in intermediate linogram DFT. This value must be larger than or equal \f$2 \max\{ m, n \}\f$ and should be a multiple of 4.
   * \param S Half of the number of the terms in the truncated series. Ideally, \f$N + 4S\f$ should be a power of two for more effective internal FFT computations.
   *
   * \return GARLiC plan.
   */
garlic_plan garlic_create( int m, int n, int M, int N_g, int N, int S );

garlic_plan garlic_create_general( int m, int n, int M, int N_g, int N, int S, double * angles );


/**
   * \brief Execute a GARLiC plan.
   *
   * \param plan GARLiC plan.
   * \param in Input image. Must be of size \f$mn\f$, according to parameters passed to planner function.
   * \param out Output DFT. Should be of size \f$MN_g\f$, according to parameters passed to planner function.
   *
   */
void garlic_execute( garlic_plan plan, std::complex< double > const * in, std::complex< double > * out );

/**
   * \brief Execute a GARLiC plan.
   *
   * \param plan GARLiC plan.
   * \param in Input image. Must be of size \f$mn\f$, according to parameters passed to planner function.
   * \param out Output DFT. Should be of size \f$MN_g\f$, according to parameters passed to planner function.
   *
   */
void garlic_execute( garlic_plan plan, double const * in, std::complex< double > * out );

/**
   * \brief Execute a GARLiC plan via brute force DFT.
   *
   * \param plan GARLiC plan.
   * \param in Input image. Must be of size \f$mn\f$, according to parameters passed to planner function.
   * \param out Output DFT. Should be of size \f$MN_g\f$, according to parameters passed to planner function.
   *
   */
void garlic_execute_dft( garlic_plan plan, std::complex< double > const * in, std::complex< double > * out );

/**
   * \brief Execute a GARLiC plan via brute force DFT.
   *
   * \param plan GARLiC plan.
   * \param in Input image. Must be of size \f$mn\f$, according to parameters passed to planner function.
   * \param out Output DFT. Should be of size \f$MN_g\f$, according to parameters passed to planner function.
   *
   */
void garlic_execute_dft( garlic_plan plan, double const * in, std::complex< double > * out );

/**
   * \brief Execute adjoint of a GARLiC plan.
   *
   * \param plan GARLiC plan.
   * \param in Input DFT. Must be of size \f$MN_g\f$, according to parameters passed to planner function.
   * \param out Output image. Should be of size \f$mn\f$, according to parameters passed to planner function.
   *
   */
void garlic_execute_adjoint( garlic_plan plan, std::complex< double > const * in, std::complex< double > * out );

/**
 * \brief Clean up resources allocated by planner function.
 *
 * \param plan Plan to be destroyed.
 */
void garlic_destroy( garlic_plan plan );

/**
 * \brief Small golden angles.
 *
 * \param plan GARLiC plan.
 *
 * \return Vector containing the angles where the golden-angle linogram DFTs are computed for the plan.
 */
std::vector< double > garlic_angles( garlic_plan plan );

#endif //#ifndef GARLIC_H
