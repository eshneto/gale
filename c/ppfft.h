//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#ifndef PPFFT_H
#define PPFFT_H

#include "chirp_z.h"
#include <fftw3.h>

#include <thread>
#include <stdexcept>
#include <cstring>
#include <vector>

//TODO: Verify whether or not FFTW3 is doing a good threading job. I suspect it does not split the many FFTs accross threads, instead, it seems to parallelize only each individual FFT.

/**
 * This class provide routines for computation of the pseudo-polar fast Fourier transform and its adjoint.
 */
struct ppfft {
   // Concurrency level:
   int n_threads_; ///< Number of execution threads.
   // FFTW concurrency level:
   int fftw_n_threads_; ///< Number of FFTW execution threads.

   // Transform dimensions:
   int m_; ///< Number of lines at the image.
   int n_; ///< Number of columns at the image.
   int M_; ///< Number of samples on each pseudo polar ray.
   int N_; ///< Number of pseudo polar rays.
   int S_; ///< Number of extra pseudo polar rays.

   // Array that will contain FFT input data:
   fftw_complex * fftw_in_; ///< Array that will contain FFT input data.

   // Array that will contain FFT output data:
   fftw_complex * fftw_out_; ///< Array that will contain FFT output data.

   // FFT's plans:
   fftw_plan direct_plan_bv_; ///< Plan for BV part of transform.
   fftw_plan direct_plan_bh_; ///< Plan for BH part of transform.
   fftw_plan adjoint_plan_bv_; ///< Plan for BV part of adjoint transform.
   fftw_plan adjoint_plan_bh_; ///< Plan for BH part of adjoint transform.

   // Chirp z transform objects:
   chirp_z chirp_z_bh_obj_; ///< Object that will perform the BH chirp-z transforms.
   chirp_z chirp_z_bv_obj_; ///< Object that will perform the BV chirp-z transforms.

   // Arrays that will contain the FFT shifting factors:
   fftw_complex * bh_M_shift_; ///< Array that will contain the BH FFT shifting factors.
   fftw_complex * bv_M_shift_; ///< Array that will contain the BV FFT shifting factors.

   // Create shift factors for FFT:
   static void create_M_shift_factors( int M, int shift, fftw_complex * out, double sigma );

   // Set lines to zero
   static void line_set_zero( int n, int start_line, int end_line, fftw_complex * array, int n_threads );
   static void line_set_zero_worker( int n, int start_line, int end_line, fftw_complex * array, int thread_id, int n_threads );

   // Copy result to output array:
   static void copy_bv_result( int M, int N, fftw_complex * dest, fftw_complex const * res, int n_threads );
   static void copy_bv_result_worker( int M, int N, fftw_complex * dest, fftw_complex const * res, int thread_id, int n_threads );
   static void copy_bh_result( int M, int N, fftw_complex * dest, fftw_complex const * res, int n_threads );
   static void copy_bh_result_worker( int M, int N, fftw_complex * dest, fftw_complex const * res, int thread_id, int n_threads );

   // Copy data alternating signals to shift FFT result:
   template< class data_t >
   static void copy_shifted_data_bh( int m, int n, int M, fftw_complex * dest, data_t const * src, fftw_complex const * factors, int n_threads = 1 );
   template< class data_t >
   static void copy_shifted_data_bh_worker( int m, int n, int M, fftw_complex * dest, data_t const * src, fftw_complex const * factors, int thread_id, int n_threads );
   template< class data_t >
   static void copy_shifted_data_bv( int m, int n, int M, fftw_complex * dest, data_t const * src, fftw_complex const * factors, int n_threads = 1 );
   template< class data_t >
   static void copy_shifted_data_bv_worker( int m, int n, int M, fftw_complex * dest, data_t const * src, fftw_complex const * factors, int thread_id, int n_threads );

   // Copy adjoint result to output array:
   static void copy_shifted_result_bh_adjoint( int m, int n, int M, fftw_complex * dest, fftw_complex const * src, fftw_complex const * factors, int n_threads = 1 );
   static void copy_shifted_result_bh_adjoint_worker( int m, int n, int M, fftw_complex * dest, fftw_complex const * src, fftw_complex const * factors, int thread_id, int n_threads );
   static void sum_shifted_result_bv_adjoint( int m, int n, int M, fftw_complex * dest, fftw_complex const * src, fftw_complex const * factors, int n_threads = 1 );
   static void sum_shifted_result_bv_adjoint_worker( int m, int n, int M, fftw_complex * dest, fftw_complex const * src, fftw_complex const * factors, int thread_id, int n_threads );

   // Copy data from input for adjoint:
   template< class data_t >
   static void copy_data_adjoint_bv( int M, int N, fftw_complex * dest, data_t const * in, int n_threads );
   template< class data_t >
   static void copy_data_adjoint_bv_worker( int M, int N, fftw_complex * dest, data_t const * in, int thread_id, int n_threads );
   template< class data_t >
   static void copy_data_adjoint_bh( int M, int N, fftw_complex * dest, data_t const * in, int n_threads );
   template< class data_t >
   static void copy_data_adjoint_bh_worker( int M, int N, fftw_complex * dest, data_t const * in, int thread_id, int n_threads );

   // Returns the number of used threads and initializes FFTW concurrency:
   static int thread_num( int n_threads );
   static int fftw_thread_num( int n_threads );

   // Initialize transform.
   ppfft( int m, int n, int M, int N, int S = 0, unsigned int fftw_n_threads = 0, unsigned int flags = FFTW_MEASURE, int n_threads = 0, double sigma_h = 0.0, double sigma_v = 0.0 );
   // Clean up transform:
   ~ppfft();

   // Compute half direct transform:
   void half_direct( fftw_plan * plan, chirp_z * chirp_z_obj );
   template< class data_t >
   void direct( data_t const * in, fftw_complex * out );

   // Compute half adjoint transform:
   void half_adjoint( fftw_plan * plan, chirp_z * chirp_z_obj );
   template< class data_t >
   void adjoint( data_t const * in, fftw_complex * out );

   /**
    * This is a template for providing the correct types to be used when performing copying operations on the input data for both the transpose and the adjoint PPFFT.
    */
   template< class data_t >
   struct type_provider{
      typedef data_t * pointer_t; ///< Pointer type for mutable data
      typedef data_t const * const_pointer_t; ///< Pointer type for const data
   };
};
/**
 * This specializaton gives the correct pointer types to be used when performing copying operations on fftw_complex input data.
 */
template<>
struct ppfft::type_provider< fftw_complex >{
   typedef std::complex< double > * pointer_t; ///< Pointer type for complex data
   typedef std::complex< double > const * const_pointer_t; ///< Pointer type for const complex data
};

/////////////////////// Implementations //////////////////////////

/**
 * \brief Number of threads.
 *
 * \param n_threads Number of desired threads, or 0 if the function is to find it out.
 * \return This function returns \p n_threads if this value is not 0, otherwise tries to call std::thread::hardware_concurrency() to find out how many threads it should use. If this call returns 0, it raises an std::runtime_error exception.
 */
int ppfft::thread_num( int n_threads )
{
#ifndef PFFT_NOTHREADS
   // In case the parallelization has not been user-defined:
   if( !n_threads )
      // Check out for hardware supported parallelization:
      n_threads = std::thread::hardware_concurrency();
   // This should not happen:
   if( !n_threads )
      throw std::runtime_error( "Could not automatically determine hardware concurrency! "
                                "Manually provide a suitable number of threads." );

   return n_threads;
#else
   return 1;
#endif
}

/**
 * \brief Number of threads for fftw.
 *
 * \param n_threads Number of desired threads for FFTW3 routines, or 0 if the function is to find it out.
 * \return This function returns \p n_threads if this value is not 0, otherwise tries to call std::thread::hardware_concurrency() to find out how many threads it should use. If this call returns 0, it raises an std::runtime_error exception.
 *
 * If PFFT_NOTHREADS is not defined, this function calls fftw_init_threads() and raises std::runtime_error if an error is found during this call. It also calls fftw_plan_with_nthreads() for the desired number of threads.
 *
 * In any case, the function calls fftw_import_wisdom_from_filename( "ppfft_wisdom.txt" ).
 */
int ppfft::fftw_thread_num( int n_threads )
{
   #ifndef PFFT_NOTHREADS
   // In case the parallelization has not been user-defined:
   if( !n_threads )
      // Check out for hardware supported parallelization:
      n_threads = std::thread::hardware_concurrency();
   // This should not happen:
   if( !n_threads )
      throw std::runtime_error( "Could not automatically determine hardware concurrency! "
                                "Manually provide a suitable number of threads." );

   // Initialize FFTW parallelization:
   int errcode = fftw_init_threads();
   // This should not happen:
   if( !errcode )
      throw std::runtime_error( "FFTW3 parallel initialization failed! "
                                "Try using only one thread." );

   // Set the parallelization level:
   fftw_plan_with_nthreads( n_threads );

   // Import wisdom:
   fftw_import_wisdom_from_filename( "ppfft_wisdom.txt" );

   return n_threads;
   #else
   // Import wisdom:
   fftw_import_wisdom_from_filename( "ppfft_wisdom.txt" );

   return 1;
   #endif
}

/**
 * \brief Create shifting factors.
 *
 * \param M Number of samples on each pseudo-polar ray.
 * \param shift Shifting factor.
 * \param out Array where to store shifting factors.
 *
 * This function creates shifting factors for multiplying the input to the FFT so that the transforms are computed with the frequencies following the sequence \f$\{ -S, -S + 1, \dots, M - S - 1\}\f$ (where \f$S ={}\f$ \p shift) instead of \f$\{ 0, 1, \dots, M - 1\}\f$. This is done as follows:
 *
 * \f[
 *     X_I = \sum_{i = 0}^{M - 1} x_{i}e^{-\imath i\frac{2\pi ( I - S )}{M}} =  \sum_{i = 0}^{M - 1} x_{i}e^{\imath i\frac{2\pi S}{M}}e^{-\imath i\frac{2\pi I}{M}}.
 * \f]
 * Therefore, the factors are \f$e^{\imath i\frac{2\pi S}{M}}\f$.
 */
void ppfft::create_M_shift_factors( int M, int shift, fftw_complex * out, double sigma )
{
   std::complex< double > W( 0.0, 2.0 * chirp_z::PI * shift / M + sigma );

   auto aux_ptr = reinterpret_cast< std::complex< double >* >( out );
   for ( int i = 0; i < M; ++i )
   {
      double i_d = i;
      aux_ptr[ i ] = std::exp( W * i_d );
   }
}

/**
 * \brief Set certain elements of an array to zero.
 *
 * \param n Number of columns.
 * \param start_line First line for zero-setting.
 * \param end_line One past the last line for zero-setting.
 * \param array Array where to apply the operation.
 * \param n_threads Number of threads of execution.
 *
 * This function sets all the elements of lines \p start_line, \p start_line + 1, ..., \p end_line - 1 to 0. This is done by calling \p n_threads instances of line_set_zero_worker().
 */
void ppfft::line_set_zero( int n, int start_line, int end_line, fftw_complex * array, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( ppfft::line_set_zero_worker, n, start_line, end_line, array, i, n_threads ) );
   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

/**
 * \brief Set certain elements of an array to zero.
 *
 * \param n Number of columns.
 * \param start_line First line for zero-setting.
 * \param end_line One past the last line for zero-setting.
 * \param array Array where to apply the operation.
 * \param thread_id Number of current thread of execution (should be a number in 0, 1, ..., \p n_threads - 1).
 * \param n_threads Number of threads of execution.
 *
 * This function sets all the elements of select lines in \p start_line, \p start_line + 1, ..., \p end_line - 1 to 0. All components of such lines will be zeroed only once if this function is run with every \p thread_id ranging from 0, 1, ..., \p n_threads - 1.
 */
void ppfft::line_set_zero_worker( int n, int start_line, int end_line, fftw_complex * array, int thread_id, int n_threads )
{
   auto aux_ptr = reinterpret_cast< std::complex< double > * >( array );
   for ( int i = start_line + thread_id; i < end_line; i += n_threads )
      for ( int j = 0; j < n; ++j )
         aux_ptr[ j + i * n ] = 0.0;
}

/**
 * \brief Copy input image to fft input array performing alternating signal shift (BH part).
 *
 * \tparam data_t Type of input data array element, can be either \p double or \p fftw_complex.
 * \param m Number of lines in the image.
 * \param n Number of columns in the image.
 * \param M Number of samples on each pseudo-polar ray.
 * \param dest Destination array.
 * \param src Source array.
 * \param factors Shifting factors.
 * \param n_threads Number of threads of execution.
 *
 * This function copies the content of each line of the input array \p src to the corresponding line of the destination array \p dest while multiplying each element by a shifting factor in order to shift the result of the FFTs that will be applied to the destination array. This is done by calling \p n_threads instances of copy_shifted_data_bh_worker().
 */
template< class data_t >
void ppfft::copy_shifted_data_bh( int m, int n, int M, fftw_complex * dest, data_t const * src, fftw_complex const * factors, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( ppfft::copy_shifted_data_bh_worker< data_t >, m, n, M, dest, src, factors, i, n_threads ) );
   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

/**
 * \brief Copy select lines of input image to fft input array performing alternating signal shift (BH part).
 *
 * \tparam data_t Type of input data array element, can be either \p double or \p fftw_complex.
 * \param m Number of lines in the image.
 * \param n Number of columns in the image.
 * \param M Number of samples on each pseudo-polar ray.
 * \param dest Destination array.
 * \param src Source array.
 * \param factors Shifting factors.
 * \param thread_id Number of current thread of execution (should be a number in 0, 1, ..., \p n_threads - 1).
 * \param n_threads Number of threads of execution.
 *
 * This function copies the content of select lines of the input array \p src to the corresponding line of the destination array \p dest while multiplying each element by a shifting factor in order to shift the result of the FFTs that will be applied to the destination array. All components of such lines will be copied only once if this function is run with every \p thread_id ranging from 0, 1, ..., \p n_threads - 1.
 */
template< class data_t >
void ppfft::copy_shifted_data_bh_worker( int m, int n, int M, fftw_complex * dest, data_t const * src, fftw_complex const * factors, int thread_id, int n_threads )
{
   auto aux_ptr_in = reinterpret_cast< typename ppfft::type_provider< data_t >::const_pointer_t >( src );
   auto aux_ptr_out = reinterpret_cast< std::complex< double > * >( dest );
   auto aux_ptr_factors = reinterpret_cast< std::complex< double > const * >( factors );
   for ( int i = thread_id; i < m; i += n_threads )
   {
      // Copy images rows into rows for FFT input:
      for ( int j = 0; j < n; ++j )
         aux_ptr_out[ j + i * M ] = aux_ptr_in[ j + i * n ] * aux_ptr_factors[ j ];
      // Zero pad elements that may have been overwritten:
      for ( int j = n; j < M; ++j )
         aux_ptr_out[ j + i * M ] = 0.0;
   }
}

/**
 * \brief Copy input image to fft input array performing alternating signal shift (BV part).
 *
 * \tparam data_t Type of input data array element, can be either \p double or \p fftw_complex.
 * \param m Number of lines in the image.
 * \param n Number of columns in the image.
 * \param M Number of samples on each pseudo-polar ray.
 * \param dest Destination array.
 * \param src Source array.
 * \param factors Shifting factors.
 * \param n_threads Number of threads of execution.
 *
 * This function copies the content of each line of the input array \p src to the corresponding column of the destination array \p dest while multiplying each element by a shifting factor in order to shift the result of the FFTs that will be applied to the destination array. This is done by calling \p n_threads instances of copy_shifted_data_bv_worker().
 */
template< class data_t >
void ppfft::copy_shifted_data_bv( int m, int n, int M, fftw_complex * dest, data_t const * src, fftw_complex const * factors, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( ppfft::copy_shifted_data_bv_worker< data_t >, m, n, M, dest, src, factors, i, n_threads ) );
   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

/**
 * \brief Copy select lines of input image to fft input array performing alternating signal shift (BV part).
 *
 * \tparam data_t Type of input data array element, can be either \p double or \p fftw_complex.
 * \param m Number of lines in the image.
 * \param n Number of columns in the image.
 * \param M Number of samples on each pseudo-polar ray.
 * \param dest Destination array.
 * \param src Source array.
 * \param factors Shifting factors.
 * \param thread_id Number of current thread of execution (should be a number in 0, 1, ..., \p n_threads - 1).
 * \param n_threads Number of threads of execution.
 *
 * This function copies the content of select lines of the input array \p src to the corresponding column of the destination array \p dest while multiplying each element by a shifting factor in order to shift the result of the FFTs that will be applied to the destination array. All components of such lines will be copied only once if this function is run with every \p thread_id ranging from 0, 1, ..., \p n_threads - 1.
 */
template< class data_t >
void ppfft::copy_shifted_data_bv_worker( int m, int n, int M, fftw_complex * dest, data_t const * src, fftw_complex const * factors, int thread_id, int n_threads )
{
   auto aux_ptr_in = reinterpret_cast< typename ppfft::type_provider< data_t >::const_pointer_t >( src );
   auto aux_ptr_out = reinterpret_cast< std::complex< double > * >( dest );
   auto aux_ptr_factors = reinterpret_cast< std::complex< double > const * >( factors );
   for ( int i = thread_id; i < n; i += n_threads )
   {
      // Copy columns of image into rows for FFT input:
      for ( int j = 0; j < m; ++j )
         aux_ptr_out[ j + i * M ] = aux_ptr_in[ i + j * n ] * aux_ptr_factors[ j ];
      // Zero pad elements that may have been overwritten:
      for ( int j = m; j < M; ++j )
         aux_ptr_out[ j + i * M ] = 0.0;
   }
}

/**
 * \brief Constructor
 *
 * \param m Number of lines in the image.
 * \param n Number of columns in the image.
 * \param M Number of samples on each pseudo-polar ray. Must be an even number and must be larger than or equal the maximum between \p m and \p n.
 * \param N Number of pseudo-polar rays. Must be an even number and must be larger than or equal 2 times the maximum between \p m and \p n.
 * \param S Number of extra rays. Must be divisible by 4.
 * \param fftw_n_threads Number of FFTW execution threads.
 * \param flags Flags for FFTW3's planning routines.
 * \param n_threads Number of execution threads.
 *
 * Sets upt the ppfft object. It plans the FFTs, allocates memory, zero-pads FFTs input arrays, create shifting factors and export FFTW3 wisdom. The fields \a m_, \a n_, \a M_, and \a N_ are initialized with \p m, \p n, \p M, and \p N, respectively.
 **/
ppfft::ppfft( int m, int n, int M, int N, int S, unsigned fftw_n_threads, unsigned flags, int n_threads, double sigma_h, double sigma_v )
   : n_threads_( ppfft::thread_num( n_threads ) ),
     fftw_n_threads_( ppfft::fftw_thread_num( fftw_n_threads ) ),
     m_( m ), n_( n ), M_( M ), N_( N ), S_( S ),
     fftw_in_( fftw_alloc_complex( M * ( N + S ) ) ),
     fftw_out_( fftw_alloc_complex( M * ( N + S ) ) ),
     chirp_z_bh_obj_( N / 2, S / 2, M, M / 2, ( N + S ) % 4 ? ( N + S ) / 4 : ( N + S ) / 4 - 1,
                      fftw_out_, fftw_in_,
                      fftw_alloc_complex( M * ( N + S ) ), fftw_alloc_complex( M * ( N + S ) ),
                      n_threads_, flags, -sigma_h
                    ),
     chirp_z_bv_obj_( N / 2, S / 2, M, ( M % 2 ) ? M / 2 : M / 2 - 1, ( N + S ) / 4,
                      fftw_out_, fftw_in_,
                      chirp_z_bh_obj_.buffer_, chirp_z_bh_obj_.out_,
                      n_threads_, flags, sigma_v
                    ),
     bh_M_shift_( fftw_alloc_complex( M ) ),
     bv_M_shift_( fftw_alloc_complex( M ) )
{
   // Plan for the FFTs:
   direct_plan_bv_ = fftw_plan_many_dft( 1, // Rank
                                         &M, // Dimensions
                                         n, // How many
                                         fftw_in_, // Input data
                                         0, // Inembed
                                         1, // Istride
                                         M, // Idist
                                         fftw_out_, // Output data
                                         0, // Onembed
                                         1, // Ostride
                                         M, // Odist
                                         FFTW_FORWARD,
                                         flags
                                       );
   direct_plan_bh_ = fftw_plan_many_dft( 1, // Rank
                                         &M, // Dimensions
                                         m, // How many
                                         fftw_in_, // Input data
                                         0, // Inembed
                                         1, // Istride
                                         M, // Idist
                                         fftw_out_, // Output data
                                         0, // Onembed
                                         1, // Ostride
                                         M, // Odist
                                         FFTW_FORWARD,
                                         flags
                                       );
   // Plan for the adjoint FFTs:
   adjoint_plan_bv_ = fftw_plan_many_dft( 1, // Rank
                                          &M, // Dimensions
                                          n, // How many
                                          chirp_z_bv_obj_.out_, // Input data
                                          0, // Inembed
                                          N + S, // Istride
                                          1, // Idist
                                          fftw_out_, // Output data
                                          0, // Onembed
                                          1, // Ostride
                                          M, // Odist
                                          FFTW_BACKWARD,
                                          flags
                                        );
   adjoint_plan_bh_ = fftw_plan_many_dft( 1, // Rank
                                          &M, // Dimensions
                                          m, // How many
                                          chirp_z_bh_obj_.out_, // Input data
                                          0, // Inembed
                                          N + S, // Istride
                                          1, // Idist
                                          fftw_out_, // Output data
                                          0, // Onembed
                                          1, // Ostride
                                          M, // Odist
                                          FFTW_BACKWARD,
                                          flags
                                        );

   // Fill in necessary zeros in the array.
   for( int i = 0; i < N + S; ++i )
      for( int j = 0; j < M; ++j )
         fftw_in_[ j + i * M ][ 0 ] = fftw_in_[ j + i * M ][ 1 ] = 0.0;
   for( int i = 0; i < N + S; ++i )
      for( int j = 0; j < M; ++j )
         fftw_out_[ j + i * M ][ 0 ] = fftw_out_[ j + i * M ][ 1 ] = 0.0;

   // Shifting factors:
   create_M_shift_factors( M, M / 2, bh_M_shift_, -sigma_h );
   create_M_shift_factors( M, ( M % 2 ) ? M / 2 : M / 2 - 1, bv_M_shift_, sigma_v );

   // Save wisdom:
   fftw_export_wisdom_to_filename( "ppfft_wisdom.txt" );
}

/**
 * \brief Destructor.
 *
 * Releases all resources.
 */
ppfft::~ppfft()
{
   // Clean up memory resources:
   fftw_free( bv_M_shift_ );
   fftw_free( bh_M_shift_ );
   fftw_free( chirp_z_bh_obj_.out_ );
   fftw_free( chirp_z_bh_obj_.buffer_ );
   fftw_free( fftw_out_ );
   fftw_free( fftw_in_ );

   // Clean up plan data:
   fftw_destroy_plan( adjoint_plan_bh_ );
   fftw_destroy_plan( adjoint_plan_bv_ );
   fftw_destroy_plan( direct_plan_bh_ );
   fftw_destroy_plan( direct_plan_bv_ );

#ifndef PFFT_NOTHREADS
   // Clean up FFTW's resources:
   fftw_cleanup_threads();
#endif
}

/**
 * \brief Copy result to output array (BV part).
 *
 * \param M Number of samples on each pseudo-polar ray.
 * \param N Number of pseudo-polar rays.
 * \param dest Destination array.
 * \param res Source array.
 * \param n_threads Number of threads of execution.
 *
 * This function copies the content of each column of array \p res resulting from the chirp-z transform to the correct column of the destination array \p dest. This is done by calling \p n_threads instances of copy_bv_result_worker().
 */
void ppfft::copy_bv_result( int M, int N, fftw_complex * dest, fftw_complex const * res, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( ppfft::copy_bv_result_worker, M, N, dest, res, i, n_threads ) );
   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

/**
 * \brief Copy select lines of result to output array (BV part).
 *
 * \param M Number of samples on each pseudo-polar ray.
 * \param N Number of pseudo-polar rays.
 * \param dest Destination array.
 * \param res Source array.
 * \param thread_id Number of current thread of execution (should be a number in 0, 1, ..., \p n_threads - 1).
 * \param n_threads Number of threads of execution.
 *
 * This function copies the content of select lines of each column of the array \p res resulting from the chirp-z transform to the correct column of the destination array \p dest. All components will be copied only once if this function is run with every \p thread_id ranging from 0, 1, ..., \p n_threads - 1..
 */
void ppfft::copy_bv_result_worker( int M, int N, fftw_complex * dest, fftw_complex const * res, int thread_id, int n_threads )
{
   int HN = N / 2;
   auto aux_ptr_result = reinterpret_cast< std::complex< double > const * >( res );
   auto aux_ptr_out = reinterpret_cast< std::complex< double > * >( dest );
   for ( int i = thread_id; i < M; i += n_threads )
      for ( int j = 0; j < HN; ++j )
         aux_ptr_out[ j + i * N ] = aux_ptr_result[ ( HN - 1 - j ) + ( M - 1 - i ) * N ];
}

/**
 * \brief Copy result to output array (BH part).
 *
 * \param M Number of samples on each pseudo-polar ray.
 * \param N Number of pseudo-polar rays.
 * \param dest Destination array.
 * \param res Source array.
 * \param n_threads Number of threads of execution.
 *
 * This function copies the content of each column of array \p res resulting from the chirp-z transform to the correct column of the destination array \p dest. This is done by calling \p n_threads instances of copy_bh_result_worker().
 */
void ppfft::copy_bh_result( int M, int N, fftw_complex * dest, fftw_complex const * res, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( ppfft::copy_bh_result_worker, M, N, dest, res, i, n_threads ) );
   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

/**
 * \brief Copy select lines of result to output array (BH part).
 *
 * \param M Number of samples on each pseudo-polar ray.
 * \param N Number of pseudo-polar rays.
 * \param dest Destination array.
 * \param res Source array.
 * \param thread_id Number of current thread of execution (should be a number in 0, 1, ..., \p n_threads - 1).
 * \param n_threads Number of threads of execution.
 *
 * This function copies the content of select lines of each column of the array \p res resulting from the chirp-z transform to the correct column of the destination array \p dest. All components will be copied only once if this function is run with every \p thread_id ranging from 0, 1, ..., \p n_threads - 1.
 */
void ppfft::copy_bh_result_worker( int M, int N, fftw_complex * dest, fftw_complex const * res, int thread_id, int n_threads )
{
   int HN = N / 2;
   auto aux_ptr_result = reinterpret_cast< std::complex< double > const * >( res );
   auto aux_ptr_out = reinterpret_cast< std::complex< double > * >( dest );
   for ( int i = thread_id; i < M; i += n_threads )
      for ( int j = 0; j < HN; ++j )
         aux_ptr_out[ ( j + HN ) + i * N ] = aux_ptr_result[ j + i * N ];
}

/**
 * \brief Half ppfft
 *
 * \param plan FFTW3 plan.
 * \param chirp_z_obj Chirp-z object.
 *
 * Execute plan, multiply by shifting factors and perform chirp-z transform. Depending on what is stored at ppfft::fftw_in_ and on which plan and factors are passed, performs BH or BV part of the transform. The final result will be stored at member chirp_z::out_ of ppfft::chirp_z_obj_.
 */
void ppfft::half_direct( fftw_plan * plan, chirp_z * chirp_z_obj )
{
   pm_prof prof( "ppfft::half_direct()" );

   // Compute FFTs:
   prof.start( "First FFT" );
   fftw_execute( *plan );
   prof.stop();

   // Compute chirp Z transform:
   prof.start( "Chirp Z direct" );
   chirp_z_obj->direct();
   prof.stop();
}

/**
 * \brief Compute pfft transform.
 *
 * \tparam data_t Type of input data array element, can be either \p double or \p fftw_complex.
 * \param in Input array containing an image of \f$m\f$ lines and \f$n\f$ columns.
 * \param out Output array. Must have enough space for \f$M\f$ lines and \f$N + S\f$ columns.
 *
 * This function computes, in an efficient way, the Discrete Fourier Transform
 * \f[
 *    X( \xi, \upsilon ) := \sum_{i = 0}^{m - 1}\sum_{j = 0}^{n - 1}x_{i, j}e^{-\imath( j\xi + i\upsilon )}
 * \f]
 * at the pseudo-polar grid
 * \f[
 *    \left\{ \xi^H_{I, J} = \frac{2\pi I}{M}, \quad -\frac M2 \leq I < \frac M2\quad\mbox{and}\quad \upsilon^H_{I, J} = \frac{4J}N\xi^H_{I, J}, \quad -\frac {N + S}4 < J \leq \frac {N + S}4 \right\},
 * \f]
 * \f[
 *    \left\{ \upsilon^V_{I, J} = \frac{2\pi I}{M}, \quad \frac M2 \geq I > -\frac M2\quad\mbox{and}\quad \xi^V_{I, J} = \frac{4J}N\upsilon^V_{I, J}, \quad \frac {N + S}4 > J \geq -\frac {N + S}4 \right\},
 * \f]
 * where \f$m = {}\f$ ppfft::m_, \f$n = {}\f$ ppfft::n_, \f$M = {}\f$ ppfft::M_, \f$N = {}\f$ ppfft::N_, and \f$S = {}\f$ ppfft::S_. The values of these members are determined, respectively, by the parameters \p m, \p n, \p M, \p N, and \p S of ppfft::ppfft().
 *
 * The first \f$( N + S ) / 2\f$ columns of the output contain the \f$( \xi^V_{I, J}, \upsilon^V_{I, J} )\f$ samples, while the final \f$( N + S ) / 2\f$ columns of the output contain the \f$( \xi^H_{I, J}, \upsilon^H_{I, J} )\f$ samples.
 */
template< class data_t >
void ppfft::direct( data_t const * in, fftw_complex * out )
{
   pm_prof prof( "ppfft::direct()" );

   // Copy input matrix to FFTW's input array:
   prof.start( "ppfft::copy_shifted_data_bv()" );
   ppfft::copy_shifted_data_bv( m_, n_, M_, fftw_in_, in, bv_M_shift_, n_threads_ );
   prof.stop();

   // Compute BV part of transform:
   prof.start( "half_direct( bv_N_shift_ )" );
   half_direct( &direct_plan_bv_, &chirp_z_bv_obj_ );
   prof.stop();

   // Zero pad if necessary:
   prof.start( "Zero-padding fftw_out_" );
   line_set_zero( M_, n_, m_, fftw_out_, n_threads_ );
   prof.stop();

   // Copy result to output:
   prof.start( "Copying bv" );
   // This copying worsens significantly using parallelization
   // probably because it is made backwards:
   ppfft::copy_bv_result( M_, N_ + S_, out, chirp_z_bv_obj_.out_, 1 );
   prof.stop();

   // Copy input matrix to FFTW's input array:
   prof.start( "ppfft::copy_shifted_data_bh()" );
   ppfft::copy_shifted_data_bh( m_, n_, M_, fftw_in_, in, bh_M_shift_, n_threads_ );
   prof.stop();

   // Zero pad if necessary:
   prof.start( "Zero-padding fftw_out_" );
   line_set_zero( M_, m_, n_, fftw_out_, n_threads_ );
   prof.stop();

   // Compute BH part of transform:
   prof.start( "half_direct( bh_N_shift_ )" );
   half_direct( &direct_plan_bh_, &chirp_z_bh_obj_ );
   prof.stop();

   // Copy result to output:
   prof.start( "Copying bh" );
   ppfft::copy_bh_result( M_, N_ + S_, out, chirp_z_bh_obj_.out_, n_threads_ );
   prof.stop();
}

/**
 * \brief Copy and shift to adjoint output array (BH part).
 *
 * \param m Number of lines in the image.
 * \param n Number of columns in the image.
 * \param M Number of samples on each pseudo-polar ray.
 * \param dest Destination array.
 * \param src Source array.
 * \param factors Shifting factors.
 * \param n_threads Number of threads of execution.
 *
 * This function copies, while alternating signs of the elements, the content of each line of array \p src to the corresponding line of the destination array \p dest. It performs the adjoint operation of copy_shifted_data_bh(). This is done by calling \p n_threads instances of copy_shifted_result_bh_adjoint_worker().
 */
void ppfft::copy_shifted_result_bh_adjoint( int m, int n, int M, fftw_complex * dest, fftw_complex const * src, fftw_complex const * factors, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( ppfft::copy_shifted_result_bh_adjoint_worker, m, n, M, dest, src, factors, i, n_threads ) );
   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

/**
 * \brief Copy and shift select lines to adjoint output array (BH part).
 *
 * \param m Number of lines in the image.
 * \param n Number of columns in the image.
 * \param M Number of samples on each pseudo-polar ray.
 * \param dest Destination array.
 * \param src Source array.
 * \param factors Shifting factors.
 * \param thread_id Number of current thread of execution (should be a number in 0, 1, ..., \p n_threads - 1).
 * \param n_threads Number of threads of execution.
 *
 * This function copies, while alternating signs of the elements, the content of select lines of array \p src to the corresponding lines of the destination array \p dest. It performs the adjoint operation of copy_shifted_data_bh_worker(). All components will be copied only once if this function is run with every \p thread_id ranging from 0, 1, ..., \p n_threads - 1.
 */
void ppfft::copy_shifted_result_bh_adjoint_worker( int m, int n, int M, fftw_complex * dest, fftw_complex const * src, fftw_complex const * factors, int thread_id, int n_threads )
{
   auto aux_ptr_in = reinterpret_cast< std::complex< double > const * >( src );
   auto aux_ptr_out = reinterpret_cast< std::complex< double > * >( dest );
   auto aux_ptr_factors = reinterpret_cast< std::complex< double > const * >( factors );
   for ( int i = thread_id; i < m; i += n_threads )
      for ( int j = 0; j < n; ++j )
         aux_ptr_out[ j + i * n ] = aux_ptr_in[ j + i * M ] * std::conj( aux_ptr_factors[ j ] );
}

/**
 * \brief Shift and sum to adjoint output array (BV part).
 *
 * \param m Number of lines in the image.
 * \param n Number of columns in the image.
 * \param M Number of samples on each pseudo-polar ray.
 * \param dest Destination array.
 * \param src Source array.
 * \param factors Shifting factors.
 * \param n_threads Number of threads of execution.
 *
 * This function sums, after alternating signs of the elements, the content of each line of array \p src to the corresponding column of the destination array \p dest. It performs the adjoint operation of copy_shifted_data_bv(). This is done by calling \p n_threads instances of copy_shifted_result_bv_adjoint_worker().
 */
void ppfft::sum_shifted_result_bv_adjoint( int m, int n, int M, fftw_complex * dest, fftw_complex const * src, fftw_complex const * factors, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( ppfft::sum_shifted_result_bv_adjoint_worker, m, n, M, dest, src, factors, i, n_threads ) );
   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

/**
 * \brief Shift and sum select lines to adjoint output array (BV part).
 *
 * \param m Number of lines in the image.
 * \param n Number of columns in the image.
 * \param M Number of samples on each pseudo-polar ray.
 * \param dest Destination array.
 * \param src Source array.
 * \param factors Shifting factors.
 * \param thread_id Number of current thread of execution (should be a number in 0, 1, ..., \p n_threads - 1).
 * \param n_threads Number of threads of execution.
 *
 * This function sums, after alternating signs of the elements, the content of select lines of array \p src to the corresponding columns of the destination array \p dest. It performs the adjoint operation of copy_shifted_data_bv_worker(). All components will be summed only once if this function is run with every \p thread_id ranging from 0, 1, ..., \p n_threads - 1.
 */
void ppfft::sum_shifted_result_bv_adjoint_worker( int m, int n, int M, fftw_complex * dest, fftw_complex const * src, fftw_complex const * factors, int thread_id, int n_threads )
{
   auto aux_ptr_in = reinterpret_cast< std::complex< double > const * >( src );
   auto aux_ptr_out = reinterpret_cast< std::complex< double > * >( dest );
   auto aux_ptr_factors = reinterpret_cast< std::complex< double > const * >( factors );
   for ( int i = thread_id; i < m; i += n_threads )
      for ( int j = 0; j < n; ++j )
         aux_ptr_out[ j + i * n ] += aux_ptr_in[ i + j * M ] * std::conj( aux_ptr_factors[ i ] );
}

/**
 * \brief Copy BV part of input image to fft input array while inverting column ordering.
 *
 * \tparam data_t Type of input data array element, can be either \p double or \p fftw_complex.
 * \param M Number of samples on each pseudo-polar ray.
 * \param N Number of pseudo-polar rays.
 * \param dest Destination array.
 * \param in Input array.
 * \param n_threads Number of threads of execution.
 *
 * This function copies the content of each BV column of input array \p in to the correct column of the destination array \p dest, which will be input to the adjoint chirp-z transform. It performs the adjoint operation of copy_bv_result(). This is done by calling \p n_threads instances of copy_data_adjoint_bv_worker().
 */
template< class data_t >
void ppfft::copy_data_adjoint_bv( int M, int N, fftw_complex * dest, data_t const * in, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( ppfft::copy_data_adjoint_bv_worker< data_t >, M, N, dest, in, i, n_threads ) );
   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

/**
 * \brief Copy select lines of BV part of input image to fft input array while inverting column ordering.
 *
 * \tparam data_t Type of input data array element, can be either \p double or \p fftw_complex.
 * \param M Number of samples on each pseudo-polar ray.
 * \param N Number of pseudo-polar rays.
 * \param dest Destination array.
 * \param in Input array.
 * \param thread_id Number of current thread of execution (should be a number in 0, 1, ..., \p n_threads - 1).
 * \param n_threads Number of threads of execution.
 *
 * This function copies the content of select lines of the BV columns of input array \p in to the correct column of the destination array \p dest, which will be input to the adjoint chirp-z transform. It performs the adjoint operation of copy_bv_result_worker(). All components of BV part will be copied only once if this function is run with every \p thread_id ranging from 0, 1, ..., \p n_threads - 1.
 */
template< class data_t >
void ppfft::copy_data_adjoint_bv_worker( int M, int N, fftw_complex * dest, data_t const * in, int thread_id, int n_threads )
{
   int HN = N / 2;
   auto aux_ptr_in = reinterpret_cast< typename ppfft::type_provider< data_t >::const_pointer_t >( in );
   auto aux_ptr_out = reinterpret_cast< std::complex< double > * >( dest );
   for ( int i = thread_id; i < M; i += n_threads )
   {
      for ( int j = 0; j < HN; ++j )
         aux_ptr_out[ j + i * N ] = aux_ptr_in[ ( HN - 1 - j ) + ( M - 1 - i ) * N ];
      for ( int j = HN; j < N; ++j )
         aux_ptr_out[ j + i * N ] = 0.0;
   }
}

/**
 * \brief Copy BH part of input image to fft input array.
 *
 * \tparam data_t Type of input data array element, can be either \p double or \p fftw_complex.
 * \param M Number of samples on each pseudo-polar ray.
 * \param N Number of pseudo-polar rays.
 * \param dest Destination array.
 * \param in Input array.
 * \param n_threads Number of threads of execution.
 *
 * This function copies the content of each BH column of input array \p in to the correct column of the destination array \p dest, which will be input to the adjoint chirp-z transform. It performs the adjoint operation of copy_bh_result(). This is done by calling \p n_threads instances of copy_data_adjoint_bh_worker().
 */
template< class data_t >
void ppfft::copy_data_adjoint_bh( int M, int N, fftw_complex * dest, data_t const * in, int n_threads )
{
   std::vector< std::thread > v_threads;
   v_threads.reserve( n_threads );
   for ( int i = 0; i < n_threads; ++i )
      v_threads.push_back( std::thread( ppfft::copy_data_adjoint_bh_worker< data_t >, M, N, dest, in, i, n_threads ) );
   for ( int i = 0; i < n_threads; ++i )
      v_threads[ i ].join();
}

/**
 * \brief Copy select lines of BH part of input image to fft input array.
 *
 * \tparam data_t Type of input data array element, can be either \p double or \p fftw_complex.
 * \param M Number of samples on each pseudo-polar ray.
 * \param N Number of pseudo-polar rays.
 * \param dest Destination array.
 * \param in Input array.
 * \param thread_id Number of current thread of execution (should be a number in 0, 1, ..., \p n_threads - 1).
 * \param n_threads Number of threads of execution.
 *
 * This function copies the content of select lines of the BH columns of input array \p in to the correct column of the destination array \p dest, which will be input to the adjoint chirp-z transform. It performs the adjoint operation of copy_bh_result_worker(). All components of BH part will be copied only once if this function is run with every \p thread_id ranging from 0, 1, ..., \p n_threads - 1.
 */
template< class data_t >
void ppfft::copy_data_adjoint_bh_worker( int M, int N, fftw_complex * dest, data_t const * in, int thread_id, int n_threads )
{
   int HN = N / 2;
   auto aux_ptr_in = reinterpret_cast< typename ppfft::type_provider< data_t >::const_pointer_t >( in );
   auto aux_ptr_out = reinterpret_cast< std::complex< double > * >( dest );
   for ( int i = thread_id; i < M; i += n_threads )
   {
      for ( int j = 0; j < HN; ++j )
         aux_ptr_out[ j + i * N ] = aux_ptr_in[ ( j + HN ) + i * N ];
      for ( int j = HN; j < N; ++j )
         aux_ptr_out[ j + i * N ] = 0.0;
   }
}

/**
 * \brief Half adjoint ppfft
 *
 * \param plan FFTW3 plan.
 * \param chirp_z_obj Chirp-z object.
 *
 * Perform chirp-z transform, multiply by conjugate of shifting factors and execute plan. Depending on what is stored at ppfft::fftw_in_ and on which plan and factors are passed, performs BH or BV part of the transform. The final result will be stored at member chirp_z::out_ of ppfft::chirp_z_obj_.
 */
void ppfft::half_adjoint( fftw_plan * plan, chirp_z * chirp_z_obj )
{
   pm_prof prof( "ppfft::half_adjoint()" );

   // Compute adjoint chirp Z transform:
   prof.start( "Chirp Z adjoint" );
   chirp_z_obj->adjoint();
   prof.stop();

   // Compute IFFT:
   prof.start( "Final FFT" );
   fftw_execute( *plan );
   prof.stop();
}

/**
 * \brief Compute adjoint pfft transform.
 *
 * \tparam data_t Type of input data array element, can be either \p double or \p fftw_complex.
 * \param in Input array containing an image of \a m_ lines and \a n_ columns.
 * \param out Output array. Must have enough space for \a M_ lines and \a N_ columns.
 *
 * This function computes, in an efficient way, the adjoint of the operation computed by direct().
 */
template< class data_t >
void ppfft::adjoint( data_t const * in, fftw_complex * out )
{
   pm_prof prof( "ppfft::adjoint()" );

   // Copy BH part of input matrix to chirp Z's adjoint input array:
   prof.start( "ppfft::copy_data_adjoint_bh()" );
   ppfft::copy_data_adjoint_bh( M_, N_ + S_, fftw_in_, in, n_threads_ );
   prof.stop();

   // Compute BH part of adjoint:
   prof.start( "half_adjoint( transpose_bh_N_shift_ )" );
   half_adjoint( &adjoint_plan_bh_, &chirp_z_bh_obj_ );
   prof.stop();

   // Copy result to output:
   prof.start( "Copying adjoint bh" );
   ppfft::copy_shifted_result_bh_adjoint( m_, n_, M_, out, fftw_out_, bh_M_shift_, n_threads_ );
   prof.stop();

   // Copy BV part of input matrix to chirp Z's adjoint input array:
   prof.start( "ppfft::copy_data_adjoint_bv()" );
   ppfft::copy_data_adjoint_bv( M_, N_ + S_, fftw_in_, in, n_threads_ );
   prof.stop();

   // Compute BV part of adjoint:
   prof.start( "half_adjoint( transpose_bv_N_shift_ )" );
   half_adjoint( &adjoint_plan_bv_, &chirp_z_bv_obj_ );
   prof.stop();

   // Sum result to output:
   prof.start( "Summing bv" );
   ppfft::sum_shifted_result_bv_adjoint( m_, n_, M_, out, fftw_out_, bv_M_shift_, n_threads_ );
   prof.stop();

   // Zero pad to have correct input for chirp_z in direct transform:
   prof.start( "Zero-padding fftw_out_" );
   line_set_zero( M_, n_, m_, fftw_out_, n_threads_ );
   prof.stop();
}

#endif // #ifndef PPFFT_H
