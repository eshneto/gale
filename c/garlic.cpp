//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE. If not, see <https://www.gnu.org/licenses/>.

#define GPPFFT_NO_DEFAULT
#include "gppfft.h"
#include "garlic.h" // Header file for routines defined here.
#include "dft.h"    // Provides golden-angle generating functions

garlic_plan garlic_create( int m, int n, int M, int N_g, int N, int S )
{
   std::vector< double > angles = dft::golden_angles( N_g, dft::PI / 2.0 );

   garlic_plan retval = new gppfft<>( m, n, &( angles[ 0 ] ), N_g, M, S, 0, N, 4 * ( S + 1 ) );

   return retval;
}

garlic_plan garlic_create_general( int m, int n, int M, int N_g, int N, int S, double * angles )
{
   garlic_plan retval = new gppfft<>( m, n, angles, N_g, M, S, 0, N, 4 * ( S + 1 ) );

   return retval;
}

void garlic_execute( garlic_plan plan, std::complex< double > const * in, std::complex< double >* out )
{
   plan->direct( in, out );
}

void garlic_execute( garlic_plan plan, double const * in, std::complex< double > * out )
{
   plan->direct( in, out );
}

void garlic_execute_dft( garlic_plan plan, std::complex< double > const * in, std::complex< double > * out )
{
   std::vector< double > angles = garlic_angles( plan );
   double * xi = new double[ plan->ppfft_object_.M_ * plan->N_ ];
   double * upsilon = new double[ plan->ppfft_object_.M_ * plan->N_ ];
   dft::any_angle_pp_grid( plan->ppfft_object_.M_, angles, xi, upsilon );

   dft::dft( plan->ppfft_object_.m_, plan->ppfft_object_.n_, in, plan->ppfft_object_.M_, plan->N_, xi, upsilon, out );

   delete[] upsilon;
   delete[] xi;
}

void garlic_execute_dft( garlic_plan plan, double const * in, std::complex< double > * out )
{
   std::vector< double > angles = garlic_angles( plan );
   double * xi = new double[ plan->ppfft_object_.M_ * plan->N_ ];
   double * upsilon = new double[ plan->ppfft_object_.M_ * plan->N_ ];
   dft::any_angle_pp_grid( plan->ppfft_object_.M_, angles, xi, upsilon );

   dft::dft( plan->ppfft_object_.m_, plan->ppfft_object_.n_, in, plan->ppfft_object_.M_, plan->N_, xi, upsilon, out );

   delete[] upsilon;
   delete[] xi;
}

void garlic_execute_adjoint( garlic_plan plan, std::complex< double > const * in, std::complex< double > * out )
{
   plan->adjoint( in, out );
}

std::vector< double > garlic_angles( garlic_plan plan )
{
   return dft::golden_angles( plan->N_ );
}

void garlic_destroy( garlic_plan plan )
{
   delete plan;
}
