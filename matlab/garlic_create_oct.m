%     Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
%
%     This file is part of GALE.
%
%     GALE is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     GALE is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with GALE.  If not, see <https://www.gnu.org/licenses/>.
%
function PLAN = garlic_create_oct( M, N, N_G, M_G, N_P, S )

   if length( N_G ) == 1
      PLAN = garlic_mex( 1, M, N, N_G, M_G, N_P, S );
   else
      PLAN = garlic_mex( 6, M, N, N_G, M_G, N_P, S );
   end

end
