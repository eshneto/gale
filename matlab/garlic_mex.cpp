//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE.  If not, see <https://www.gnu.org/licenses/>.

#include "../c/garlic.cpp"
#include "mex.h"
#include "matrix.h"
#include "fort.c"
#include <cmath>
#include <set>
#include <cstdint>
#include <complex>

std::set< garlic_plan > plans;

void garlic_create_mex(
                        int nlhs, mxArray * plhs [],
                        int nrhs, const mxArray * prhs []
                      )
{
   if ( nrhs < 6 )
      return;

   int m   = std::round( *mxGetPr( prhs[ 0 ] ) );
   int n   = std::round( *mxGetPr( prhs[ 1 ] ) );
   int N_g = std::round( *mxGetPr( prhs[ 2 ] ) );
   int M   = std::round( *mxGetPr( prhs[ 3 ] ) );
   int N   = std::round( *mxGetPr( prhs[ 4 ] ) );
   int S   = std::round( *mxGetPr( prhs[ 5 ] ) );

   garlic_plan plan = garlic_create( m, n, M, N_g, N, S );
   plans.insert( plan );

   plhs[0] = mxCreateNumericMatrix( 1, 1, mxINT64_CLASS, mxREAL );
   *static_cast< uint64_t * >( mxGetData( plhs[ 0 ] ) ) = reinterpret_cast< uint64_t >( plan );
}

void garlic_create_general_mex(
   int nlhs, mxArray * plhs [],
   int nrhs, const mxArray * prhs []
)
{
   if ( nrhs < 6 )
      return;

   int m   = std::round( *mxGetPr( prhs[ 0 ] ) );
   int n   = std::round( *mxGetPr( prhs[ 1 ] ) );
   double * angles = reinterpret_cast< double * >( mxGetPr( prhs[ 2 ] ) );
   if ( mxGetN( prhs[ 2 ] ) != 1 )
      return;
   int M   = std::round( *mxGetPr( prhs[ 3 ] ) );
   int N   = std::round( *mxGetPr( prhs[ 4 ] ) );
   int S   = std::round( *mxGetPr( prhs[ 5 ] ) );

   int N_g = mxGetM( prhs[ 2 ] );

   garlic_plan plan = garlic_create_general( m, n, M, N_g, N, S, angles );
   plans.insert( plan );

   plhs[ 0 ] = mxCreateNumericMatrix( 1, 1, mxINT64_CLASS, mxREAL );
   *static_cast< uint64_t * >( mxGetData( plhs[ 0 ] ) ) = reinterpret_cast< uint64_t >( plan );
}

void garlic_destroy_mex(
                         int nlhs, mxArray * plhs [],
                         int nrhs, const mxArray * prhs []
                       )
{
   if ( nrhs < 1 )
      return;

   garlic_plan plan = reinterpret_cast< garlic_plan >( *static_cast< uint64_t * >( mxGetData( prhs[ 0 ] ) ) );

   if ( plans.find( plan ) != plans.end() )
   {
      garlic_destroy( plan );
      plans.erase( plan );
   }
   else
   {
      mexPrintf( "Invalid plan. Please make sure you are passing a plan created using garlic_create() that has not yet been destroyed.\n" );
   }
}

void garlic_destroy_all_mex()
{
   auto it = plans.begin();
   while ( it != plans.end() )
   {
      garlic_destroy( *it );
      ++it;
   }

   plans.clear();
}

void garlic_execute_mex(
                         int nlhs, mxArray * plhs [],
                         int nrhs, const mxArray * prhs []
                       )
{
   if ( nrhs < 2 )
      return;

   garlic_plan plan = reinterpret_cast< garlic_plan >( *static_cast< uint64_t * >( mxGetData( prhs[ 0 ] ) ) );

   if ( plans.find( plan ) == plans.end() )
   {
      mexPrintf( "Invalid plan. Please make sure you are passing a plan created using garlic_create() that has not yet been destroyed.\n" );
      return;
   }

   if ( ( mxGetM( prhs[ 1 ] ) != plan->ppfft_object_.n_ ) || ( mxGetN( prhs[ 1 ] ) != plan->ppfft_object_.m_ ) )
   {
      mexPrintf( "Input matrix does not match plan's dimensions. Plan takes %i by %i matrices as input.\n", plan->ppfft_object_.m_, plan->ppfft_object_.n_ );
      return;
   }

   std::complex< double > * in = reinterpret_cast< std::complex< double > * >( mat2fort( prhs[ 1 ], mxGetM( prhs[ 1 ] ), mxGetN( prhs[ 1 ] ) ) );
   std::complex< double > * out = new std::complex< double >[ plan->ppfft_object_.M_ * plan->N_ ];

   garlic_execute( plan, in, out );

   plhs[ 0 ] = fort2mat( reinterpret_cast< double * >( out ), plan->N_, plan->N_, plan->ppfft_object_.M_ );

   delete[] out;
   mxFree( in );
}

void garlic_adjoint_mex(
                         int nlhs, mxArray * plhs [],
                         int nrhs, const mxArray * prhs []
                       )
{
   if ( nrhs < 2 )
      return;

   garlic_plan plan = reinterpret_cast< garlic_plan >( *static_cast< uint64_t * >( mxGetData( prhs[ 0 ] ) ) );

   if ( plans.find( plan ) == plans.end() )
   {
      mexPrintf( "Invalid plan. Please make sure you are passing a plan created using garlic_create() that has not yet been destroyed.\n" );
      return;
   }

   if ( ( mxGetM( prhs[ 1 ] ) != plan->N_ ) || ( mxGetN( prhs[ 1 ] ) != plan->ppfft_object_.M_ ) )
   {
      mexPrintf( "Input matrix does not match plan's dimensions. Plan's adjoint takes %i by %i matrices as input.\n", plan->ppfft_object_.M_, plan->N_ );
      return;
   }

   std::complex< double > * in = reinterpret_cast< std::complex< double > * >( mat2fort( prhs[ 1 ], mxGetM( prhs[ 1 ] ), mxGetN( prhs[ 1 ] ) ) );
   std::complex< double > * out = new std::complex< double >[ plan->ppfft_object_.m_ * plan->ppfft_object_.n_ ];

   garlic_execute_adjoint( plan, in, out );

   plhs[ 0 ] = fort2mat( reinterpret_cast< double * >( out ), plan->ppfft_object_.n_, plan->ppfft_object_.n_, plan->ppfft_object_.m_ );

   delete[] out;
   mxFree( in );
}

void mexFunction(
                  int nlhs, mxArray * plhs [],
                  int nrhs, const mxArray * prhs []
                )
{
   if ( nrhs < 1 )
      return;

   int opt = *mxGetPr( prhs[ 0 ] );

   switch( opt )
   {
      case 1:
         garlic_create_mex( nlhs, plhs, nrhs - 1, prhs + 1 );
         break;
      case 2:
         garlic_destroy_mex( nlhs, plhs, nrhs - 1, prhs + 1 );
         break;
      case 3:
         garlic_destroy_all_mex();
         break;
      case 4:
         garlic_execute_mex( nlhs, plhs, nrhs - 1, prhs + 1 );
         break;
      case 5:
         garlic_adjoint_mex( nlhs, plhs, nrhs - 1, prhs + 1 );
         break;
      case 6:
         garlic_create_general_mex( nlhs, plhs, nrhs - 1, prhs + 1 );
         break;
   }
}
