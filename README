This is GALE. GALE is a numeric software intended to perform
accurate and efficient approximation of the DFT over the
generalized linogram domain. GALE is licensed under the GPL
v3 or later. For alternative licensing schemes, please contact
the author.

GALE is implemented in C++ and uses the FFTW3 and Boost
libraries for FFT and special functions computations,
respectively.

MATLAB and Python bindings are available. Octave support is
currently incomplete, but further development is planned for
this platform.

We highly discourage the use of MATLAB because of its closed
and proprietary nature and because of it's poorly designed
programming language. Octave is a free open source replacement,
but it inherits MATLAB's programming language.

We suggest users starting to make use of our code to try either
the c++ library or to use Python if a higher level interface is
desired.

For more information, contact the author:
Elias S. Helou <elias@icmc.usp.br>.
