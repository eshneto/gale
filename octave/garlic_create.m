%     Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
%
%     This file is part of GALE.
%
%     GALE is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     GALE is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with GALE.  If not, see <https://www.gnu.org/licenses/>.
%
function plan = garlic_create( M, N_G, N, S, M_G, N_P )
% Create a plan for the golden angle fast DFT
%
%  PLAN = GARLIC_CREATE( M )
%  PLAN = GARLIC_CREATE( M, N_G )
%  PLAN = GARLIC_CREATE( M, N_G, N )
%  PLAN = GARLIC_CREATE( M, N_G, N, S )
%  PLAN = GARLIC_CREATE( M, N_G, N, S, M_G )
%  PLAN = GARLIC_CREATE( M, N_G, N, S, M_G, N_P )
%
%  M is the number of lines in input images.
%  N_G is the number of angles in output DFT. If not given will be the same as M.
%  N is the number of columns in input images. If not given, will be the same as M.
%  S is half of the number of parcels in truncated series. If not given, 6 is used.
%  M_G is the number of samples per angle in output DFT. If not provided, M_G = max( [ M, N ] ) is used.
%  N_P is the number of angles in intermediate DFT. Unless you know precisely why to provide this value, there is no need for it. If not given, it will be such that N_P + 4 * S will be the smallest power of two such that N_P is larger than or equal 2 * max( [ M, N ] ). This value must be multiple of four and must be larger than 2 * max( [ M, N ] ).

   if nargin < 1
      disp( "Insufficient number of arguments" )
   end

   if nargin < 2
      N_G = M;
   end

   if nargin < 3
      N = M;
   end

   if nargin < 4
      S = 6;
   end

   if nargin < 5
      M_G = max( [ M, N ] );
   else
      if M_G < max( [ M, N ] );
         disp( 'Error, parameter M_G can not be smaller than max( [ M, N ]. Type help garlic_create for more info.' );
         return
      end
   end

   if nargin < 6
      N_T = 2 * max( [ M, N ] ) + 4 * S;
      E = nextpow2( N_T );
      if ( 2 ^ E ) == N_T
         E = E + 1;
      end
      N_P = 2 ^ E - 4 * S;
   end

   plan = garlic_create_oct( M, N, N_G, M_G, N_P, S );

end
