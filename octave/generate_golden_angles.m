%     Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
%
%     This file is part of GALE.
%
%     GALE is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     GALE is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with GALE.  If not, see <https://www.gnu.org/licenses/>.
%
function A = generate_golden_angles( N, start_angle )

   golden_angle = 1.9416110387254665773;

   if nargin < 2
      start_angle = pi / 2.0;
   end

   A = zeros( [ N, 1 ] );
   A( 1 ) = start_angle;
   for i = 2 : N
      A( i ) = A( i - 1 ) + golden_angle;
   end

   A = mod( A - pi / 4, pi );
   A( A <= 0 ) = A( A <= 0 ) + pi;
   A = A + pi / 4;

   A = sort( A );

end
