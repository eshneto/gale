%     Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
%
%     This file is part of GALE.
%
%     GALE is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     GALE is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with GALE.  If not, see <https://www.gnu.org/licenses/>.
%
function ADJDFT = garlic_adjoint( PLAN, IMAGE )
% execute adjoint plan for the golden angle fast DFT
%
%  GARLIC_ADJOINT( PLAN, IMAGE )
%
%  Executes adjoint PLAN on IMAGE. IMAGE must have dimesions equal to M_G lines and N_G columns as passed for garlic_create() during creation of PLAN, otherwise the function causes an error.

   ADJDFT = garlic_adjoint_oct( PLAN, IMAGE.' );
   ADJDFT = ADJDFT.';

end
