//    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
//
//    This file is part of GALE.
//
//    GALE is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    GALE is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with GALE.  If not, see <https://www.gnu.org/licenses/>.

#include "../garlic.cpp"
#include <octave/oct.h>
#include <octave/uint64NDArray.h>
#include <octave/CMatrix.h>
#include <cstdint>
#include <set>
#include <cmath>
#include <complex>

std::set< garlic_plan > plans;

DEFUN_DLD( garlic_create_oct, args, nargout, "" )
{
   if ( args.length() < 6 )
      return octave_value_list();

   int m   = std::round( args( 0 ).array_value()( 0 ) );
   int n   = std::round( args( 1 ).array_value()( 0 ) );
   int N_g = std::round( args( 2 ).array_value()( 0 ) );
   int M   = std::round( args( 3 ).array_value()( 0 ) );
   int N   = std::round( args( 4 ).array_value()( 0 ) );
   int S   = std::round( args( 5 ).array_value()( 0 ) );

   garlic_plan plan = garlic_create( m, n, M, N_g, N, S );
   plans.insert( plan );

   dim_vector dv( 1, 1 );
   uint64NDArray retval( dv );
   retval( 0, 0 ) = reinterpret_cast< uint64_t >( plan );

   return octave_value( retval );
}

DEFUN_DLD( garlic_destroy_oct, args, nargout,"" )
{
   if ( args.length() < 1 )
      return octave_value_list();

   garlic_plan plan = reinterpret_cast< garlic_plan >( static_cast< uint64_t >( args( 0 ).uint64_array_value()( 0 ) ) );

   if ( plans.find( plan ) != plans.end() )
   {
      garlic_destroy( plan );
      plans.erase( plan );
   }
   else
   {
      octave_stdout << "Invalid plan. Please make sure you are passing a plan created using garlic_create() that has not yet been destroyed.\n";
   }

   return octave_value_list();
}

DEFUN_DLD( garlic_destroy_all_oct, args, nargout, "" )
{
   auto it = plans.begin();
   while ( it != plans.end() )
   {
      garlic_destroy( *it );
      ++it;
   }

   plans.clear();

   return octave_value_list();
}

DEFUN_DLD( garlic_execute_oct, args, nargout, "" )
{
   if ( args.length() < 2 )
      return octave_value_list();

   garlic_plan plan = reinterpret_cast< garlic_plan >( static_cast< uint64_t >( args( 0 ).uint64_array_value()( 0 ) ) );

   if ( plans.find( plan ) == plans.end() )
   {
      octave_stdout << "Invalid plan. Please make sure you are passing a plan created using garlic_create() that has not yet been destroyed.\n";
   }

   ComplexMatrix inMat = args( 1 ).complex_matrix_value();
   dim_vector dims = inMat.dims();

   if ( ( dims( 0 ) != plan->ppfft_object_.n_ ) || ( dims( 1 ) != plan->ppfft_object_.m_ ) )
   {
      octave_stdout << "Input matrix does not match plan's dimensions. Plan takes " << plan->ppfft_object_.m_
                    << " by " << plan->ppfft_object_.n_ << " matrices as input.\n";
      return octave_value_list();
   }

   ComplexMatrix outMat( plan->N_, plan->ppfft_object_.M_ );

   garlic_execute( plan, inMat.fortran_vec(), outMat.fortran_vec() );

   return octave_value( outMat );
}

DEFUN_DLD( garlic_adjoint_oct, args, nargout, "" )
{
   if ( args.length() < 2 )
      return octave_value_list();

   garlic_plan plan = reinterpret_cast< garlic_plan >( static_cast< uint64_t >( args( 0 ).uint64_array_value()( 0 ) ) );

   if ( plans.find( plan ) == plans.end() )
   {
      octave_stdout << "Invalid plan. Please make sure you are passing a plan created using garlic_create() that has not yet been destroyed.\n";
   }

   ComplexMatrix inMat = args( 1 ).complex_matrix_value();
   dim_vector dims = inMat.dims();

   if ( ( dims( 0 ) != plan->N_ ) || ( dims( 1 ) != plan->ppfft_object_.M_ ) )
   {

      octave_stdout << "Input matrix does not match plan's dimensions. Plan's adjoint takes " << plan->ppfft_object_.M_
                    << " by " << plan->N_ << " matrices as input.\n";
      return octave_value_list();
   }

   ComplexMatrix outMat( plan->ppfft_object_.n_, plan->ppfft_object_.m_ );

   garlic_execute_adjoint( plan, inMat.fortran_vec(), outMat.fortran_vec() );

   return octave_value( outMat );
}
