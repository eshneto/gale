%     Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
%
%     This file is part of GALE.
%
%     GALE is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
%
%     GALE is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
%
%     You should have received a copy of the GNU General Public License
%     along with GALE.  If not, see <https://www.gnu.org/licenses/>.
%
clc; clear all;

garlic_compile

m = 35;
n = 40;
M = 100;
N = 90;
S = 6;

plan = garlic_create( m, N, n, S, M );

x = ones( [ m n ] ) + 0.5i * ones( [ m n ] );
Ax = garlic_execute( plan, x );

y = ones( [ M N ] );
Aty = garlic_adjoint( plan, y );

x(:)'*Aty(:)
Ax(:)'*y(:)

aAx = abs( Ax );
rel_thres = 1.0;
mx = max( max( aAx ) )
aAx( aAx > rel_thres * mx ) = rel_thres * mx;

imagesc( aAx )

garlic_destroy()
