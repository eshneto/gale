#    Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
#
#    This file is part of GALE.
#
#    GALE is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    GALE is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with GALE.  If not, see <https://www.gnu.org/licenses/>.
#

# Define paths:
ifeq ($(origin SRCDIR), undefined)
	SRCDIR := $(shell pwd)
endif
ifeq ($(origin TMPDIR), undefined)
	TMPDIR := $(shell pwd)/tmp
endif
ifeq ($(origin BLDDIR), undefined)
	BLDDIR := $(shell pwd)/build
endif

$(TMPDIR)/chirp_z_test/chirp_z_test : $(SRCDIR)/c/test_code/chirp_z_test.cpp $(SRCDIR)/c/chirp_z.h $(SRCDIR)/c/pm_prof.h
	mkdir -p $(@D)
	cp $(SRCDIR)/c/test_code/chirp_z_test.cpp $(@D)
	cp $(SRCDIR)/c/chirp_z.h $(@D)
	cp $(SRCDIR)/c/pm_prof.h $(@D)
	cd $(@D) && g++ -O3 -Wall -Werror -o chirp_z_test chirp_z_test.cpp -lfftw3 -lfftw3_threads -lpthread -lm
.PHONY : chirp_z_test
chirp_z_test : $(TMPDIR)/chirp_z_test/chirp_z_test
	cd $(TMPDIR)/$(@) && ./$(@)

$(TMPDIR)/ppfft_test/ppfft_test : $(SRCDIR)/c/test_code/ppfft_test_random.cpp $(SRCDIR)/c/ppfft.h $(SRCDIR)/c/chirp_z.h $(SRCDIR)/c/pm_prof.h
	mkdir -p $(@D)
	cp $(SRCDIR)/c/test_code/ppfft_test_random.cpp $(@D)
	cp $(SRCDIR)/c/ppfft.h $(@D)
	cp $(SRCDIR)/c/chirp_z.h $(@D)
	cp $(SRCDIR)/c/dft.h $(@D)
	cp $(SRCDIR)/c/pm_prof.h $(@D)
	cd $(@D) && g++ -O3 -Wall -Werror -o ppfft_test ppfft_test_random.cpp -lfftw3 -lfftw3_threads -lpthread -lm
.PHONY : ppfft_test
ppfft_test : $(TMPDIR)/ppfft_test/ppfft_test
	cd $(TMPDIR)/$(@) && ./$(@)

$(TMPDIR)/gappfft_test/gappfft_test : $(SRCDIR)/c/test_code/gappfft_test_random.cpp $(SRCDIR)/c/gppfft.h $(SRCDIR)/c/ppfft.h $(SRCDIR)/c/chirp_z.h $(SRCDIR)/c/pm_prof.h
	mkdir -p $(@D)
	cp $(SRCDIR)/c/test_code/gappfft_test_random.cpp $(@D)
	cp $(SRCDIR)/c/gppfft.h $(@D)
	cp $(SRCDIR)/c/ppfft.h $(@D)
	cp $(SRCDIR)/c/chirp_z.h $(@D)
	cp $(SRCDIR)/c/kaiser_bessel.h $(@D)
	cp $(SRCDIR)/c/test_code/test_utils.h $(@D)
	cp $(SRCDIR)/c/dft.h $(@D)
	cp $(SRCDIR)/c/pm_prof.h $(@D)
	cd $(@D) && g++ -O3 -Wall -Werror -o gappfft_test gappfft_test_random.cpp -lnfft3 -lfftw3 -lfftw3_threads -lpthread -lm
.PHONY : gappfft_test
gappfft_test : $(TMPDIR)/gappfft_test/gappfft_test
	cd $(TMPDIR)/$(@) && ./$(@)

$(TMPDIR)/shared/garlic_c.so : $(SRCDIR)/c/garlic_c.h $(SRCDIR)/c/garlic_c.cpp $(SRCDIR)/c/garlic.h $(SRCDIR)/c/garlic.cpp
	mkdir -p $(@D)
	cd $(@D) && g++ -O3 -Wall -Werror -fPIC -c $(SRCDIR)/c/garlic_c.cpp
	gcc $(@D)/garlic_c.o -shared -o $(@)

.PHONY : test_load
test_load : $(TMPDIR)/shared/garlic_c.so $(SRCDIR)/python/gppfft.py
	mkdir -p $(TMPDIR)/test
	cp $(TMPDIR)/shared/garlic_c.so $(SRCDIR)/python/gppfft.py $(TMPDIR)/test
	cd $(TMPDIR)/test && python -B gppfft.py

.PHONY : clean
clean :
	rm -rf $(TMPDIR)
	rm -rf $(BLDDIR)
