#   Copyright 2018 Elias S. Helou <elias@icmc.usp.br>
#
#   This file is part of GALE.
#
#   GALE is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   GALE is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with GALE.  If not, see <https://www.gnu.org/licenses/>.

import ctypes
import ctypes.util
import math
import numpy

libstdcpp  = ctypes.CDLL( ctypes.util.find_library( "stdc++" ), mode=ctypes.RTLD_GLOBAL )
libfftw3   = ctypes.CDLL( ctypes.util.find_library( "fftw3" ), mode=ctypes.RTLD_GLOBAL )
libfftw3_t = ctypes.CDLL( ctypes.util.find_library( "fftw3_threads" ), mode=ctypes.RTLD_GLOBAL )
libgarlic  = ctypes.CDLL( "./garlic_c.so" )

_c_double_p  = ctypes.POINTER( ctypes.c_double )
_c_complex_p = ctypes.POINTER( 2 * ctypes.c_double )
_c_plan_p = ctypes.c_void_p

libgarlic.C_garlic_create.argtypes = [ ctypes.c_int,
                                       ctypes.c_int,
                                       ctypes.c_int,
                                       ctypes.c_int,
                                       ctypes.c_int,
                                       ctypes.c_int
                                     ]
libgarlic.C_garlic_create.restype = _c_plan_p

libgarlic.C_garlic_create_general.argtypes = [ ctypes.c_int,
                                               ctypes.c_int,
                                               ctypes.c_int,
                                               ctypes.c_int,
                                               ctypes.c_int,
                                               ctypes.c_int,
                                               _c_double_p
                                             ]
libgarlic.C_garlic_create_general.restype = _c_plan_p

libgarlic.C_garlic_execute_cplx.argtypes = [ _c_plan_p,
                                             _c_complex_p,
                                             _c_complex_p
                                           ]
libgarlic.C_garlic_execute_cplx.restype = None

libgarlic.C_garlic_execute.argtypes = [ _c_plan_p,
                                        _c_double_p,
                                        _c_complex_p
                                      ]
libgarlic.C_garlic_execute.restype = None

libgarlic.C_garlic_execute_adjoint.argtypes = [ _c_plan_p,
                                                _c_complex_p,
                                                _c_complex_p
                                              ]
libgarlic.C_garlic_execute_adjoint.restype = None

libgarlic.C_garlic_destroy.argtypes = [ _c_plan_p ]
libgarlic.C_garlic_destroy.restype = None

def generate_golden_angles( N, first_angle = None ):

   if first_angle is None:
      first_angle = math.pi / 2.0

   # Golden ratio:
   ratio = 2.0 * math.pi / ( 1.0 + ( 5.0 ** 0.5 ) )

   # Generate angles:
   angles = []
   for J in range( N ):
      angles.append( ratio * J + first_angle )

   # Constrain to ( pi/4, 5pi / 4]:
   for J in range( N ):
      curr_angle = angles[ J ] - ( math.pi / 4.0 )
      mul = int( curr_angle / math.pi )
      curr_angle -= mul * math.pi
      if curr_angle <= 0.0:
         curr_angle += math.pi
      angles[ J ] = curr_angle + ( math.pi / 4.0 )

   angles.sort()

   return angles

def generate_samples( M, angles, sigma = None, return_complex = True ):

   if isinstance( angles, ( int, long ) ):
      angles = generate_golden_angles( angles )

   if sigma is None:
      sigma = math.pi / M

   delta = 2.0 * math.pi / M

   xi = numpy.zeros( ( M, len( angles ) ) )
   upsilon = numpy.zeros( ( M, len( angles ) ) )

   for j in range( len( angles ) ):
      if ( abs( math.sin( angles[ j ] ) ) > abs( math.cos( angles[ j ] ) ) ) and \
         ( angles[ j ] <= 3.0 * math.pi / 4.0 ):
         # "Vertical" angles:
         ct = math.cos( angles[ j ] ) / math.sin( angles[ j ] )
         for i in range( M ):
            upsilon[ i, j ] = math.pi - delta * i - sigma;
            xi[ i, j ] = upsilon[ i, j ] * ct;

      else:
         # "Horizontal" angles:
         tg = math.tan( angles[ j ] )
         for i in range( M ):
            xi[ i, j ] = -math.pi + delta * i + sigma;
            upsilon[ i, j ] = xi[ i, j ] * tg

   if return_complex:
      return ( upsilon + 1j * xi ) / ( 2.0 * math.pi );
   else:
      return ( xi, upsilon )

class gen_lino( object ):

   # P is the chirp_z length!
   def __init__( self, m, N_a = None, n = None, M = None, N = None, S = 6, angles = None, P = None ):

      if N_a is None:
         if angles is None:
            N_a = m
         else:
            if len( angles.shape ) != 1:
               raise ValueError( "angles must be an one-dimensional array." )
            N_a = len( angles )
      else:
         if not ( angles is None ):
            raise ValueError( "It is not allowed to provide both angles and N." )

      if n is None:
         n = m

      if M is None:
         M = m

      if ( not ( N is None ) ) and ( not ( P is None ) ):
         raise ValueError( "It is not allowed to provide both P and N." )

      if not ( P is None ):
         N = 2 * ( P - 2 * ( S + 1 ) )
      if N is None:
         N = 2 * max( [ m, n ] )

      self.m = m
      self.n = n
      self.M = M
      self.N_a = N_a
      self.N = N
      self.S = S
      if angles is None:
         self.angles = numpy.array( generate_golden_angles( N_a ), dtype = numpy.float64 )
      else:
         self.angles = numpy.array( angles, dtype = numpy.float64 )

      if angles is None:
         self.plan = libgarlic.C_garlic_create( m, n, M, N_a, N, S )
      else:
         self.plan = libgarlic.C_garlic_create( m, n, M, N_a, N, S,
                                                self.angles.ctypes.data
                                              )

   #def dcf( self ):
      #return abs( generate_samples( self.M, self.angles ) )

   def shift_matrix( self ):
      k = generate_samples( self.M, self.angles )
      return numpy.exp( 1j * ( math.pi * self.M ) * ( numpy.real( k ) + numpy.imag( k ) ) )

   def dcf( self, shift = True ):
      k = generate_samples( self.M, self.angles )
      if shift:
         return numpy.abs( k ) * numpy.conj( self.shift_matrix() )
      else:
         return numpy.abs( k )

   def forward( self, x ):

      if ( x.shape[ 0 ] != self.m ) or \
         ( x.shape[ 1 ] != self.n ):

         raise ValueError( "Invalid argument shape! Required is ( %d, %d )." % ( self.m, self.n ) )

      if not ( x.dtype in ( numpy.complex64, numpy.complex128, numpy.float32, numpy.float64 ) ):
         raise ValueError( "Invalid argument data type! Required single or double precision real or complex." )

      if x.dtype in ( numpy.complex64, numpy.complex128 ):
         x = numpy.array( x, dtype = numpy.complex128, order = 'C' )
      else:
         x = numpy.array( x, dtype = numpy.float64, order = 'C' )

      retval = numpy.zeros( ( self.M, self.N_a ), dtype = numpy.complex128, order = "C" )

      if x.dtype == numpy.complex128:
         libgarlic.C_garlic_execute_cplx( self.plan,
                                          ctypes.cast( x.ctypes.data, _c_complex_p ),
                                          ctypes.cast( retval.ctypes.data, _c_complex_p )
                                        )
      else:
         libgarlic.C_garlic_execute( self.plan,
                                     ctypes.cast( x.ctypes.data, _c_double_p ),
                                     ctypes.cast( retval.ctypes.data, _c_complex_p )
                                   )

      return retval

   def backward( self, x ):

      if ( x.shape[ 0 ] != self.M ) or \
         ( x.shape[ 1 ] != self.N_a ):

         raise ValueError( "Invalid argument shape! Required is ( %d, %d )." % ( self.M, self.N_a ) )

      if not ( x.dtype in ( numpy.complex64, numpy.complex128 ) ):
         raise ValueError( "Invalid argument data type! Required single or double precision complex." )

      if x.dtype in ( numpy.complex64, numpy.complex128 ):
         x = numpy.array( x, dtype = numpy.complex128, order = "C" )

      retval = numpy.zeros( ( self.m, self.n ), dtype = numpy.complex128, order = "C" )

      libgarlic.C_garlic_execute_adjoint( self.plan,
                                          ctypes.cast( x.ctypes.data, _c_complex_p ),
                                          ctypes.cast( retval.ctypes.data, _c_complex_p )
                                        )

      return retval

   def psf( self ):
       y = numpy.ones( ( self.M, self.N_a ), dtype = numpy.float64, order = "C" )
       return self.backward( y * self.dcf() )

   def __del__( self ):
      # If libgarlic has already gone out of scope we are about to be cleaned up anyway
      if not ( libgarlic is None ):
         libgarlic.C_garlic_destroy( self.plan )

if __name__ == "__main__":
   import matplotlib.pyplot as pp

   res = 512
   nspokes = 400
   P = 640

   print "Initializing GALE..."
   obj = gen_lino( res, nspokes, P = P )
   print "Done!"

   x = obj.psf()
   pp.imshow( numpy.log( numpy.abs( x ) ), cmap = 'gray' )
   #pp.imshow( numpy.abs( x ), cmap = 'gray' )
   pp.colorbar()
   pp.show()

   #pp.plot( numpy.log( abs( x[ 256, : ] ) ) )
   #pp.show()

